// Scripts for firebase and firebase messaging
importScripts('https://www.gstatic.com/firebasejs/8.2.0/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/8.2.0/firebase-messaging.js');

// Initialize the Firebase app in the service worker by passing the generated config
const firebaseConfig = {
    apiKey: "AIzaSyCnzG-rCwK6nQ3kBGPtLyYWKTeqm7NaZds",
    authDomain: "sarga-gelsin.firebaseapp.com",
    projectId: "sarga-gelsin",
    storageBucket: "sarga-gelsin.appspot.com",
    messagingSenderId: "581465811376",
    appId: "1:581465811376:web:8c061c94749c8c56c98e75",
    measurementId: "G-G4V185MHSV"
};


firebase.initializeApp(firebaseConfig);

// Retrieve firebase messaging
const messaging = firebase.messaging();

messaging.onBackgroundMessage(function(payload) {
    console.log('Received background message', payload);

    const notificationTitle = payload.notification.title;
    const notificationOptions = {
        body: payload.notification.body,
    };

    self.registration.showNotification(notificationTitle, notificationOptions);
});