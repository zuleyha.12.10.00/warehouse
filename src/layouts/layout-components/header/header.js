import React from 'react';
import { connect } from 'react-redux';
import {
	Nav,
	NavItem,
	NavLink,
	Navbar,
	NavbarBrand,
	Collapse
} from 'reactstrap';
import { authenticationService } from '../../../request/_services/auth';

/*--------------------------------------------------------------------------------*/
/* Import images which are need for the HEADER                                    */
/*--------------------------------------------------------------------------------*/
// import logodarkicon from '../../../assets/images/logo-icon.png';

const mapStateToProps = state => ({
	...state
});

class Header extends React.Component {
	constructor(props) {
		super(props);
		this.toggle = this.toggle.bind(this);
		this.showMobilemenu = this.showMobilemenu.bind(this);
		this.sidebarHandler = this.sidebarHandler.bind(this);
		this.state = {
			isOpen: false
		};
	}
	/*--------------------------------------------------------------------------------*/
	/*To open NAVBAR in MOBILE VIEW                                                   */
	/*--------------------------------------------------------------------------------*/
	toggle() {
		this.setState({
			isOpen: !this.state.isOpen
		});
	}
	/*--------------------------------------------------------------------------------*/
	/*To open SIDEBAR-MENU in MOBILE VIEW                                             */
	/*--------------------------------------------------------------------------------*/
	showMobilemenu() {
		document.getElementById('main-wrapper').classList.toggle('show-sidebar');
	}
	sidebarHandler = () => {
		let element = document.getElementById('main-wrapper');
		switch (this.props.settings.activeSidebarType) {
			case 'full':
			case 'iconbar':
				element.classList.toggle('mini-sidebar');
				if (element.classList.contains('mini-sidebar')) {
					element.setAttribute('data-sidebartype', 'mini-sidebar');
				} else {
					element.setAttribute(
						'data-sidebartype',
						this.props.settings.activeSidebarType
					);
				}
				break;

			case 'overlay':
			case 'mini-sidebar':
				element.classList.toggle('full');
				if (element.classList.contains('full')) {
					element.setAttribute('data-sidebartype', 'full');
				} else {
					element.setAttribute(
						'data-sidebartype',
						this.props.settings.activeSidebarType
					);
				}
				break;
			default:
		}
	};

	render() {
		return (
			<header className="topbar navbarbg" >
			
				<Navbar className={"top-navbar " + (this.props.settings.activeNavbarBg === "skin6" ? 'navbar-light' : 'navbar-dark')} expand="md">
					<div className="navbar-header" id="logobg" data-logobg={this.props.settings.activeLogoBg}>
						{/*--------------------------------------------------------------------------------*/}
						{/* Mobile View Toggler  [visible only after 768px screen]                         */}
						{/*--------------------------------------------------------------------------------*/}
						<span className="nav-toggler d-block d-md-none" onClick={this.showMobilemenu}>
							<img width="20" src="/images/menu.png"/>
						</span>
						{/*--------------------------------------------------------------------------------*/}
						{/* Logos Or Icon will be goes here for Light Layout && Dark Layout                */}
						{/*--------------------------------------------------------------------------------*/}
						<NavbarBrand href="/">
							<b className="logo-icon">
								{/* <img style={{width: "100%", maxWidth: 200}} src={logodarkicon} alt="homepage" className="dark-logo" /> */}
							</b>
						</NavbarBrand>
						{/*--------------------------------------------------------------------------------*/}
						{/* Mobile View Toggler  [visible only after 768px screen]                         */}
						{/*--------------------------------------------------------------------------------*/}
						<span className="topbartoggler d-block d-md-none">
							{/* <img width="20" src="/images/menu.png"/> */}
						</span>
					</div>
					<Collapse className="navbarbg" isOpen={this.state.isOpen} navbar data-navbarbg={this.props.settings.activeNavbarBg} >
						<Nav className="float-left" navbar>
							<NavItem>
								<NavLink href="#" className="d-none d-md-block" onClick={this.sidebarHandler}>
									<span><i class="fas fa-chevron-left"></i></span>
								</NavLink>
							</NavItem>
							<NavItem>
								<form className='form-inline'>
									<div className='search-field-module'>
										<div className='search-field-wrap'>
											<button
												className='btn btn-secondary btn-search swipe-to-top'
												style={{transform : 'translate(-2px,0.5px)'}}
												data-toggle='tooltip'
												data-placement='bottom'
												title='Search Products'
												//onClick={() => history.push(`/search/${searchText.replace(/\./g, "").replace(/\,/g, "").replace(/\//g, "").replace(/\?/g, "").replace(/\!/g, "").replace(/\;/g, "")}`)}
												>
												<i className='fas fa-search' />
											</button>
											<input
											type='search'
											placeholder='Поиск'
											data-toggle='tooltip'
											data-placement='bottom'
											title='search item'
											//   onChange={(e) => setSearchText(e.target.value)}
											//   value={searchText}
											/>
											<button
											className='btn btn-secondary swipe-to-top'
											style={{transform : 'translate(2px,0.5px)'}}
											data-toggle='tooltip'
											data-placement='bottom'
											title='Search Products'
											//onClick={() => history.push(`/search/${searchText.replace(/\./g, "").replace(/\,/g, "").replace(/\//g, "").replace(/\?/g, "").replace(/\!/g, "").replace(/\;/g, "")}`)}
											>
											<i class="fas fa-times"></i>
											</button>
										</div>
									</div>
								</form>
							</NavItem>
						</Nav>
						<Nav navbar>
							<NavItem>
								<span style={{background: "#00ACD1", borderRadius: "4px", height: '44px', width: '44px', padding: '10px'}}>
									<img src={'/images/calendar.svg'}/>
								</span>
							</NavItem>
							<NavItem>
								<span style={{background: "#00ACD1", borderRadius: "4px", height: '44px', width: '44px', padding: '10px'}}>
									<img src={'/images/sms.svg'}/>
								</span>
							</NavItem>
							<NavItem>
								<span style={{background: "#00ACD1", borderRadius: "4px", height: '44px', width: '44px', padding: '10px'}}>
									<img src={'/images/notification.svg'}/>
								</span>
							</NavItem>
							
							<NavItem>				
								<a href="/" style={{float: "right", color: "#525252", verticalAlign: "middle", marginRight: '30px', marginTop:'5px'}}>
									<div className='container' style={{minWidth:'300px'}}>
										<div className='row'>
											<span className='col-10'>
												<h4 className='font-inter' style={{ fontSize: "16px", textAlign:'end'}}>Durdyyew Aman</h4>
												<h6 style={{fontSize: '14px', color: '#03ADD2', fontWeight:'400', textAlign:'end'}}>Baslyk</h6>
											</span>
											<span className='col-2' style={{background: '#03ADD2', borderRadius:'50%', maxWidth:'40px', textAlign:'center', paddingTop:'8px', height:'40px', color:'#fff'}}>
												A
											</span>
										</div>
									</div>
								</a>
							</NavItem>
						</Nav>
						</Collapse>
					
				</Navbar>
			</header>
		);
	}
}
export default connect(mapStateToProps)(Header);
