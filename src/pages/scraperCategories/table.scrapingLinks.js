import React from "react";
import {
    Button,
    ButtonGroup,
    Modal,
    ModalHeader,
    ModalBody,
    ModalFooter,
    Input,
    FormGroup,
    Label,
    Card,
    CardBody
} from 'reactstrap';

import BootstrapTable from 'react-bootstrap-table-next';
import 'react-bootstrap-table-next/dist/react-bootstrap-table2.min.css';
import cellEditFactory, { Type } from 'react-bootstrap-table2-editor';
import filterFactory, { selectFilter, textFilter } from 'react-bootstrap-table2-filter';

import languages from "../../languages";


class Table extends React.Component {

    state = {
        genderModal: null, 
        selectedGenders: []
    }

    openGenderModal(row){
        this.setState({genderModal: row, selectedGenders: row.gender ? JSON.parse(row.gender): []})
    }

    onChangeGenders(value, id){
        if (value){
            this.state.selectedGenders.push(id)
        }else{
            this.state.selectedGenders = this.state.selectedGenders
                .filter((e)=> e !== id )
        }
        this.setState(this.state)
    }

    onClickDelete(id){
        if (window.confirm("Are you sure you want to delete this category")){
            this.props.onDelete(id)
        }
    }

    onSaveGenderChange(){
        let body = {gender: JSON.stringify(this.state.selectedGenders)}
        console.log(this.state.genderModal, body)
        this.props.onUpdate(this.state.genderModal.id, body)
        this.setState({genderModal: null, selectedGenders: []})
    }

    onUpdate(oldValue, newValue, row, column, done) {
        if (window.confirm('Do you want to accept this change?')) {
            done(true);
            let body = {}
            body[column.dataField] = newValue
            this.props.onUpdate(row.id, body)
        } else {
            done(false);
        }
        return { async: true };
    }

    

    onDelete(id){
        if (!window.confirm("Are you sure you want to delete the links"))
            return
        this.props.onDelete(id)
    }

    renderTable(columns, data, parent) {
        let rows = data.filter((e) => e.parentId == parent)
        if (rows && rows.length)
            return <BootstrapTable
                bootstrap4
                keyField='id'
                data={rows || []}
                columns={columns}
                filter={ filterFactory() }
                cellEdit={cellEditFactory({
                    mode: 'dbclick',
                    beforeSaveCell: this.onUpdate.bind(this),
                    // afterSaveCell: (oldValue, newValue, row, column) => { console.log('After Saving Cell!!'); }
                })}
            />
        return (
            <div style={{ textAlign: "center" }}>
                No data have found!
            </div>
        );
    }

    render() {
        console.log(this.props.data)
        return (
            <div>
                {this.renderTable(this.columns, this.props.data, null)}
                <Modal
                    isOpen={this.state.genderModal}
                    toggle={()=>this.setState({genderModal: null})}
                    className={this.props.className}
                    fade={false}
                >
                    <ModalHeader toggle={()=>this.setState({genderModal: null})}>
                        Gender
                    </ModalHeader>
                    <ModalBody>
                        <FormGroup>
                            <Card className="border">
                                <CardBody>
                                    { this.renderGenders() }
                                </CardBody>
                            </Card>
                        </FormGroup>
                    </ModalBody>
                    <ModalFooter>
                        <Button color="primary" onClick={()=>this.onSaveGenderChange()}>
                            Save
                        </Button>
                        <Button color="secondary" onClick={()=>this.setState({genderModal: null})}>
                            Cancel
                        </Button>
                    </ModalFooter>
                </Modal>
            </div>
        )
    }

    renderGenders(){
        return Object.keys(genders).map((e, ind)=> (
            <FormGroup check style={{margin: 5}} key={ind}>
                <Label check>
                    <Input 
                        type="checkbox" 
                        checked={this.state.selectedGenders.indexOf(e.id)>-1}
                        onChange={(el)=>this.onChangeGenders(el.target.checked, e.id)}
                    />{' '}
                    {e.id} - {languages.map((elem, i)=> e[`title_${elem.code}`] ).join(" / ")}
                </Label>
            </FormGroup>
        ))
    }

    getCategoryTitleById(id, recurse){
        if (!id)
            return ""
        
        let cat = this.props.categories.find((e)=> String(e.id) === String(id) )
        if (!cat)
            return ""
        return (this.getCategoryTitleById(cat.parentId, true) + cat[`title_${languages[0].code}`] + (recurse ? " > " : "" ) )
    }

    columns = [
        {
            dataField: 'id',
            text: 'ID',
            sort: true,
            filter: textFilter()
        },
        {
            dataField: "categoryId",
            text: "Category",
            sort: true,
            filter: textFilter(),
            editable: false,
            formatter: (cell) => cell.map(e => this.getCategoryTitleById(e)).join(" / ")
        },
        {
            dataField: "links",
            text: "Links",
            sort: true,
            filter: textFilter(),
            formatter: (cell, row) => {
                return <div style={{maxHeight: 100, overflow: 'hidden'}}>{cell}</div>
            }
        },
        {
            dataField: "gender",
            text: "Gender",
            filter: selectFilter({
                options: genders
            }),
            editor: {
                type: Type.SELECT,
                getOptions: (setOptions, { row, column }) => (
                    Object.keys(genders).map( e => ({ value: e, label: genders[e] }) )
                )
            },            
            formatter: (cell)=>genders[cell]
        },
        {
            sort: true,
            dataField: "priority",
            text: "Priority",
            filter: textFilter()
        },
        {
            dataField: "weight",
            text: "Weight",
            filter: textFilter()
        },
        {
            sort: true,
            dataField: "count",
            text: "Count",
            filter: textFilter()
        },
        {
            text: "Action",
            dataField: "action",
            editable: false,
            formatter: (cellContent, row) => (
                <ButtonGroup>
                    <Button color="danger" onClick={() => this.onDelete(row.id)}>Delete</Button>
                </ButtonGroup>
            ),
            style: { textAlign: "center", verticalAlign: "middle" }
        }
    ];
}

export const genders = {
    1: "Erkek",
    2: "Ayal",
    3: "Oglan çaga",
    4: "Gyz çaga"
}

export default Table;








// var http = require('http');
// var https = require('https');
// http.createServer(onRequest).listen(3000);
// function onRequest(client_req, client_res) {
//   console.log('serve: ' + client_req.url);
//    console.log(client_req.url.split("?url=")[1]);
// https.get(client_req.url.split("?url=")[1], res => {
//   let data = [];
//   const headerDate = res.headers && res.headers.date ? res.headers.date : 'no response date';
//   console.log('Status Code:', res.statusCode);
//   console.log('Date in Response header:', headerDate);
//   res.on('data', chunk => {
//     data.push(chunk);
//   });
//   res.on('end', () => {
//     console.log('Response ended: ');
//     const res = Buffer.concat(data);
//             client_res.writeHead(200);
//         client_res.end(res);
//   });
// }).on('error', err => {
//   console.log('Error: ', err.message);
//         client_res.writeHead(400);
//         client_res.end(err.message);
// });       
         
// }  





// var http = require('http');
// var https = require('https');
// http.createServer(onRequest).listen(3000);
// function onRequest(client_req, client_res) {
//   console.log('serve: ' + client_req.url);
//    console.log(client_req.url.split("?url=")[1]);
// https.get(client_req.url.split("?url=")[1], res => {
//   let data = [];
//   const headerDate = res.headers && res.headers.date ? res.headers.date : 'no response date';
//   console.log('Status Code:', res.statusCode);
//   console.log('Date in Response header:', headerDate);
//   res.on('data', chunk => {
//     data.push(chunk);
//   });
//   res.on('end', () => {
//     console.log('Response ended: ');
//     const res = Buffer.concat(data);
//             client_res.writeHead(200);
//         client_res.end(res);
//   });
// }).on('error', err => {
//   console.log('Error: ', err.message);
//         client_res.writeHead(400);
//         client_res.end(err.message);
// });       
         
// }  