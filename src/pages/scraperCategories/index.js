import React from "react";
import {
    Card,
    CardBody,
    CardHeader,
    Button, 
    ButtonGroup
} from 'reactstrap';
import Table from './table.scrapingLinks'
import CreateLinks from './createLinks'

import { connect } from 'react-redux';
import { 
    getCategories,
    confCategoriesWithLinks
} from '../../redux/categories/action';

import { 
    getScrapingLinks,
    createScrapingLink,
    deleteScrapingLink,
    updateScrapingLink,
    resetScrapingLinks,
} from '../../redux/scrapingLinks/action';
import languages from "../../languages";


const mapStateToProps = state => ({
    categories: state.categories,
    filters: state.filterValues
});
const mapDispatchToProps = dispatch => ({
    getCategories: () => getCategories(dispatch),  
    confCategoriesWithLinks: (data) => confCategoriesWithLinks(dispatch, data),  
    
    getScrapingLinks: (scraperId, callback) => getScrapingLinks(dispatch, scraperId, callback), 
    createScrapingLink: (data, callback) => createScrapingLink(dispatch, data, callback),
    deleteScrapingLink: (id, callback) => deleteScrapingLink(dispatch, id, callback),
    resetScrapingLinks: (id, callback) => resetScrapingLinks(dispatch, id, callback),
    updateScrapingLink: (id, data, callback) => updateScrapingLink(dispatch, id, data, callback) 
});


class Categories extends React.Component {
    state = {
        data: [],
        create: false
    }

    toggleCreate(){
        this.setState({create: !this.state.create})
    }

    scrapingLinks = []
    
    componentDidMount(){
        if (!this.props.categories.data.length)
            this.props.getCategories();
        this.props.getScrapingLinks(this.props.location.state.id, (err, res)=>{
            if (err)
                return console.error(err)

            this.scrapingLinks = res
        })
    }

    confCategories(){
        let categories = this.props.categories.data.map(e => ({...e, links: this.getLink(this.scrapingLinks, e.id)}))
        this.props.confCategoriesWithLinks(categories)
    }

    getLink(arr, categoryId){
        for(let i = 0; i < (arr||[]).length; i++){
            console.log(categoryId, arr[i].categoryId)
            if (categoryId === arr[i].categoryId){
                console.log(arr[i])
                return(arr[i].links)
            }
        }
        return null
    }

    getLinkObject(arr, categoryId){
        for(let i = 0; i < arr.length; i++){
            if (categoryId === arr[i].categoryId){
                console.log(arr[i])
                return(arr[i])
            }
        }
        return null
    }

    onDelete(categoryId){
        this.props.deleteScrapingLink(categoryId, (err, res) => {
            if (err){
                return console.error(err)
            }
            this.scrapingLinks = this.scrapingLinks.filter(e => e.id !== categoryId)
            this.forceUpdate()
        })
    }

    onUpdate(categoryId, body){
        this.props.updateScrapingLink(categoryId, body, (err, data)=>{
            if (err){
                alert(err)
            }
            this.scrapingLinks = this.scrapingLinks.map(e => e.id === categoryId ? {...e, ...body} : e)
            this.forceUpdate()
        })
    }

    getModifiedCategories(){
        this.props.categories.data.forEach( e => {
            e.links = this.getLink(e.id)
        })
        return this.props.categories.data
    }

    getCategory(id){
        for (const category of this.props.categories.data) {
            if (category.id == id)
                return category[`title_${languages[0].code}`];
        }

        return null;
    }

    resetCount(){
        this.props.resetScrapingLinks(this.props.location.state.id, (err, res) => {
            if (err){
                return console.error(err)
            }
            this.scrapingLinks = this.scrapingLinks.map(e => ({...e, count: 0}))
            this.forceUpdate()
        })
    }

    onCreate(obj){
        this.scrapingLinks.push(obj)
        this.forceUpdate()
    }

    render() {
        // let categories = this.getModifiedCategories()
        return (
            <div>
                <Card>
                    <CardHeader>
                        <h3 style={{ display: "inline-block" }}>Scraper: {this.props.location && this.props.location.state.title}</h3>
                        <ButtonGroup style={{float: "right", position: "fixed", top: 15, right: 200, zIndex: 999999999}}>
                            <Button 
                                color="info" 
                                onClick={()=>this.resetCount()}
                            >
                                Reset Count
                            </Button>
                            <Button 
                                color="primary" 
                                onClick={()=>this.toggleCreate()}
                            >
                                Create Category Link
                            </Button>
                        </ButtonGroup>
                    </CardHeader>
                    <CardBody>
                        
                        <Table
                            data = {this.scrapingLinks}
                            onUpdate = {(categoryId, body) => this.onUpdate(categoryId, body)}
                            getLink = {(categoryId) => this.getLink(categoryId)}
                            onDelete = {(catId) => this.onDelete(catId)}
                            getCategory = {(id) => this.getCategory(id)}
                            categories = {this.props.categories.data}
                        />
                            
                    </CardBody>
                </Card>
                
                <CreateLinks
                    created = {(body)=>this.onCreate(body)}
                    scraperId = {this.props.location && this.props.location.state.id}
                    isOpen={this.state.create}
                    toggle={()=>this.toggleCreate()}
                />
            </div>
        );
    }

}


export default connect(mapStateToProps, mapDispatchToProps)(Categories);
