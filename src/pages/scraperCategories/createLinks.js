import React from "react";
import {
    Modal,
    ModalHeader,
    ModalBody,
    ModalFooter,
    Button,
    Input,
    FormGroup,
    Label,
    Card,
    CardBody,
    Collapse
} from 'reactstrap';

import {genders} from "./table.scrapingLinks"

import { connect } from 'react-redux';
import { getCategories } from '../../redux/categories/action';
import { 
    createScrapingLink
} from '../../redux/scrapingLinks/action';

import languages from '../../languages';

const mapStateToProps = state => ({
    categories: state.categories
});

const mapDispatchToProps = dispatch => ({
    createScrapingLink: (data, callback) => createScrapingLink(dispatch, data, callback),
    getCategories: () => getCategories(dispatch)
});

class CreateKey extends React.Component {
    state = {
        selectedCategories: [],
        collapsedCategories: [],
        selectedGender: 1,
    }

    onChangeCategory(value, id){
        if (value === -1){
            if (this.state.collapsedCategories.includes(id)){
                this.state.collapsedCategories = this.state.collapsedCategories.filter( e => e !== id)
            } else {
                this.state.collapsedCategories.push(id)
            }

            this.setState(this.state)
            return
        }

        if (value){
            this.setState({selectedCategories: [...this.state.selectedCategories, id]})
        }else{
            this.setState({selectedCategories: []})
        }
    }

    onChangeGender(value){
        this.setState({selectedGender: value})
    }

    onClickCreate(){
        if (!this.state.selectedCategories.length){
            alert("Categories can't be emty")
            return
        }

        let body = {
            categoryId: JSON.stringify(this.state.selectedCategories),
            gender: this.state.selectedGender,
            priority: this.priority.value,
            weight: this.weight.value,
            links: this.refs.links.value,
            scraperId: this.props.scraperId
        }

        this.props.createScrapingLink(body, (err, res) => {
            if (err){
                return console.error(err);
            }
            body.id = res.insertId
            body.categoryId = JSON.parse(body.categoryId)
            this.props.created(body)
            return;
        });

        this.onClose()
    }

    onClose(){
        this.state.collapsedCategories = []
        this.state.selectedCategories = []
        this.setState(this.state)
        this.props.toggle()
    }

    collapseCategory(key){
        if (this.state.collapsedCategories.indexOf(key)<0){
            this.setState({collapsedCategories: [...this.state.collapsedCategories, key]})
        } else {
            this.setState({collapsedCategories: this.state.collapsedCategories.filter( e => e!==key) })
        }
    }

    componentWillMount(){
        if(!this.props.categories.data.length){
            this.props.getCategories()
        }
    }

    render() {
        return (
            <Modal
                isOpen={this.props.isOpen}
                toggle={()=>this.onClose()}
                className={this.props.className}
                fade={false}
            >
                <ModalHeader toggle={()=>this.onClose()}>
                    Create Category Link
                </ModalHeader>
                <ModalBody>
                    <div style={{textAlign: "right"}}>
                        <Button color="primary" onClick={()=>this.onClickCreate()}>
                            Save
                        </Button>
                        <Button color="secondary" onClick={()=>this.onClose()}>
                            Cancel
                        </Button>
                    </div>
                    <FormGroup>
                        <Label>Categories</Label><br/>
                        <Button
                            color={"#fff"} 
                            onClick={()=>this.collapseCategory(-1)}
                        >
                            Select category: <i>[{
                                this.state.selectedCategories.map( e => this.getCategoryTitleById(e) ).join(", ")}]</i>
                        </Button>
                        <Collapse isOpen={ this.state.collapsedCategories.indexOf(-1) > -1 }>
                            <Card className="border">
                                <CardBody>
                                    { this.renderCategories(null, 0) }
                                </CardBody>
                            </Card>
                        </Collapse>
                    </FormGroup>
                    <FormGroup>
                        <Label>Gender</Label><br/>
                        { this.renderGenders() }
                    </FormGroup>
                    <FormGroup>
                        <Label for="links">Links</Label>
                        <div>
                            <textarea style={{width: "100%"}} ref="links" type="text" name="links"/>
                        </div>
                    </FormGroup>
                    <FormGroup>
                        <Label for="priority">Priority</Label>
                        <Input innerRef={(node) => this.priority = node} type="number" name="priority" defaultValue="0"/>
                    </FormGroup>
                    <FormGroup>
                        <Label for="weight">Weight (kg)</Label>
                        <Input innerRef={(node) => this.weight = node} type="number" name="weight" defaultValue="1"/>
                    </FormGroup>
                </ModalBody>
                <ModalFooter>
                    <Button color="primary" onClick={()=>this.onClickCreate()}>
                        Save
                    </Button>
                    <Button color="secondary" onClick={()=>this.onClose()}>
                        Cancel
                    </Button>
                </ModalFooter>
            </Modal>
        );
    }

    renderCategories(parent, ind){
        let children = []
        this.props.categories.data.filter((e)=> e.parentId == parent )
            .forEach((e)=>{
                children.push(
                    this.hasChildren(e.id)
                    ?
                    <FormGroup check style={{margin: 5, marginLeft: ind*30}} key={e.id}>
                        <Label>
                            <b onClick={()=>this.onChangeCategory(-1, e.id)}>{e.id} - {languages.map((elem, i)=> e[`title_${elem.code}`] ).join(" / ")}</b>
                        </Label>
                    </FormGroup>
                    :
                    <FormGroup check style={{margin: 5, marginLeft: ind*30}} key={e.id}>
                        <Label check>
                            <Input 
                                type="checkbox" 
                                checked={this.state.selectedCategories.includes(e.id)}
                                onChange={(el)=>this.onChangeCategory(el.target.checked, e.id)}
                            />{' '}
                            {e.id} - {languages.map((elem, i)=> e[`title_${elem.code}`] ).join(" / ")}
                        </Label>
                    </FormGroup>,
                    <Collapse  key={e.id+"_col"} isOpen={ this.state.collapsedCategories.indexOf(e.id) > -1 }>
                        {this.renderCategories(e.id, ind+1)}
                    </Collapse>
                )
            })
        return children   
    }

    renderGenders(){
        return(
            <select value={this.state.selectedGender} onChange={(el)=>this.onChangeGender(el.target.value)}>
                {
                    Object.keys(genders).map((e)=> (
                        <option key={e} value={e}>{genders[e]}</option>
                    ))
                }
            </select>
        )
    }

    hasChildren(id){
        return this.props.categories.data.filter((e)=> e.parentId === id ).length
    }

    getCategoryTitleById(id){
        if (!id)
            return ""
        
        let cat = this.props.categories.data.find((e)=> e.id === id )
        if (!cat)
            return ""
        return  cat[`title_${languages[0].code}`]
    }

}

export default connect(mapStateToProps, mapDispatchToProps)(CreateKey);
