import React from "react";
import {
    Card,
    CardBody,
    CardHeader,
    Collapse,
    FormGroup,
    Label,
    Input,
    Row,
    Col,
    Button
} from 'reactstrap';


import { connect } from 'react-redux';
import { getCategories } from '../../redux/categories/action';
import { createProduct } from '../../redux/products/action';

import languages from '../../languages';

import FiltersForm from './FiltersForm';
import MultiLangInput from '../../views/components/MultiLangInput'
import UnselectableFilters from "./UnselectableFilters";

const mapStateToProps = state => ({
    categories: state.categories
});
const mapDispatchToProps = dispatch => ({
    getCategories: () => getCategories(dispatch), 

    createProduct: (body, callback) => createProduct(dispatch, body, callback),   
});


class Products extends React.Component {
    state = {
        selectedCategories: []
    }

    componentWillMount(){
        if(!this.props.categories.data.length){
            this.props.getCategories()
        }
    }

    onChangeCategories(value, id){
        if (value)
            this.state.selectedCategories.push(id)
        else
            this.state.selectedCategories = this.state.selectedCategories
                .filter((e)=> e !== id )
        
        this.setState(this.state)
    }

    onClickSubmit(){
        let body = {
            title: JSON.stringify(this.refs.title.value), //obj of translation. ex: {tm: "ru", ru: "py"}
            subtitle: JSON.stringify(this.refs.subtitle.value), //obj of translation. ex: {tm: "ru", ru: "py"}
            categories: JSON.stringify(this.state.selectedCategories), // array of category id's. ex: [1, 2, 3]
            filters: JSON.stringify(this.refs.filters.value),
            filterSetQuantities: JSON.stringify(this.refs.filters.quantitiesByFilters),
            description: JSON.stringify(this.refs.description.value), //obj of translation. ex: {tm: "ru", ru: "py"}
            extra: JSON.stringify(this.refs.extra.value), //obj of translation. ex: {tm: "ru", ru: "py"}
            "image[]": this.refs.filters.images,
            priority: this.refs.priority.value || 0, // integer value
            price: this.refs.price.value || 0, // double value
            salePrice: this.refs.salePrice.value === "" ? undefined : this.refs.salePrice.value, // if in sale double value, if not undefined
        }
        
        this.props.createProduct(body, (err, succ)=>{
            if (err)
                return alert(err)
            if (window.confirm("Product created SUCCESSFULLY! Click 'OK' if you want to quit creating product."))
                this.props.history.goBack()
        });
    }

    onCancel(){
        if (window.confirm("Cahnges will be discarded!"))
            this.props.history.goBack()
    }

    render() {
        return (
            <div>
                <Card>
                    <CardHeader>
                        <h3 style={{ display: "inline-block" }}>Create a new Product</h3>
                    </CardHeader>
                    <CardBody>
                        {/******************************************/}
                        {/***   FORM STARTS HERE   *****************/}
                        {/******************************************/}
                        <div>
                            <Row form>
                                <Col md={6}>
                                    <MultiLangInput ref="title" name="title" />
                                </Col>
                                <Col md={6}>
                                    <MultiLangInput ref="subtitle" name="subtitle" />
                                </Col>
                            </Row>
                            <Row form>
                                <Col md={4}>
                                    <FormGroup>
                                        <Label>Price</Label>
                                        <Input 
                                            type="number" 
                                            placeholder="Ex: 120.50" 
                                            innerRef={(node)=> this.refs.price = node }
                                        />
                                    </FormGroup>
                                </Col>
                                <Col md={4}>
                                    <FormGroup>
                                        <Label>Sale price</Label>
                                        <Input 
                                            type="number" 
                                            placeholder="Ex: 120.50" 
                                            innerRef={(node)=> this.refs.salePrice = node }
                                        />
                                    </FormGroup>
                                </Col>
                                <Col md={4}>
                                    <FormGroup>
                                        <Label>Priority</Label>
                                        <Input 
                                            type="number" 
                                            defaultValue="0"
                                            innerRef={(node)=> this.refs.priority = node }
                                        />
                                    </FormGroup>
                                </Col>
                            </Row>
                            <FormGroup>
                                <Label>Product categories</Label><br/>
                                <Button
                                    color={"#fff"} 
                                    onClick={()=>this.setState({categoryCollapse: !this.state.categoryCollapse})}
                                >
                                    Select categories: {this.state.selectedCategories.length}
                                </Button>
                                <Collapse isOpen={this.state.categoryCollapse}>
                                    <Card className="border">
                                        <CardBody>
                                            { this.renderCategories(null, 0) }
                                        </CardBody>
                                    </Card>
                                </Collapse>
                            </FormGroup>

                            <UnselectableFilters 
                                innerRef={(node)=> this.refs.unselectableFilters = node }
                            />

                            <FiltersForm 
                                innerRef={(node)=> this.refs.filters = node }
                            />

                            <FormGroup>
                                <MultiLangInput 
                                    type = "textarea" 
                                    name = "Product description" 
                                    ref = "description"/>
                            </FormGroup>

                            <FormGroup>
                                <MultiLangInput 
                                    type = "textarea" 
                                    name = "Extra" 
                                    ref = "extra"/>
                            </FormGroup>
                            
                        
                            <Button 
                                color="success"
                                onClick={()=>this.onClickSubmit()}
                            >
                                Save
                            </Button> {" "}
                            <Button color="secondary" onClick={()=>this.onCancel()}>Cancel</Button>
                        </div>
                        {/******************************************/}
                        {/***   FORM  ENDS  HERE   *****************/}
                        {/******************************************/}

                    </CardBody>
                </Card>
            </div>
        );
    }

    renderCategories(parent, ind){
        let children = []
        this.props.categories.data.filter((e)=> e.parentId == parent )
            .forEach((e)=>{
                children.push(
                    <FormGroup check style={{margin: 5, marginLeft: ind*30}} key={e.id}>
                        <Label check>
                            <Input 
                                type="checkbox" 
                                defaultChecked={e.id in this.state.selectedCategories}
                                onChange={(el)=>this.onChangeCategories(el.target.checked, e.id)}
                            />{' '}
                            {languages.map((elem, i)=> e[`title_${elem.code}`] ).join(" / ")}
                        </Label>
                    </FormGroup>,
                    ...this.renderCategories(e.id, ind+1)
                )
            })
        return children
        
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Products);
