import React from "react";
import {
    FormGroup,
    Label,
    Input,
    Row,
    Col,
    Button
} from 'reactstrap';

import ImageUploadInput from '../../views/components/ImageUploadInput'

import { connect } from 'react-redux';
import { getFilterKeys } from '../../redux/filterKeys/action';
import { getFilterValues } from '../../redux/filterValues/action';

import languages from "../../languages";

const mapStateToProps = state => ({
    filterKeys: state.filterKeys,
    filterValues: state.filterValues
});
const mapDispatchToProps = dispatch => ({
    getFilterKeys: () => getFilterKeys(dispatch),   
    getFilterValues: () => getFilterValues(dispatch),
});


class FilterForm extends React.Component {

    state = {
        filterKeys: [],
        imagesByFilter: "null",
        images: [],
        quantity: 1,
    }

    value = []
    images = {}
    quantitiesByFilters = []

    onChangeQuantitiesByFilters(ind, val){
        this.quantitiesByFilters[ind].quantity = val
        this.forceUpdate()
    }

    onSelectFilter(ind, keyId){
        if (ind==="null" && keyId !== "null"){
            this.state.filterKeys.push({id: keyId, values: []})
        }
        else if (keyId==="null"){
            if (this.state.imagesByFilter === ind){
                this.state.imagesByFilter = "null";
                this.state.images = []
            }
            this.state.filterKeys = this.state.filterKeys.filter((e, i) => i!==ind )
        }
        else{
            this.state.filterKeys[ind] = {id: keyId, values: []}
        }
        this.setState(this.state)
    }
    
    onSelectFilterValue(keyInd, newValue, valueInd){
        if(valueInd === "null" && newValue !== "null"){
            let obj = {additionalPrice: 0, id: newValue}
            if (this.state.imagesByFilter === keyInd)
                obj.images = []
            this.state.filterKeys[keyInd].values.push(obj)
        }else if (newValue==="null"){
            this.state.filterKeys[keyInd].values = this.state.filterKeys[keyInd].values.filter((e, i) => i!==valueInd )
        }
        else{
            this.state.filterKeys[keyInd].values[valueInd].id = newValue
        }
        this.setState(this.state)
    }

    onChangeFilterValue(keyInd, valueInd, attr, value){
        this.state.filterKeys[keyInd].values[valueInd][attr] = value;
        this.setState(this.state)
    }

    renameFile(file, newName) {
        newName += "_"+Date.now()+"."+file.name.split('.').pop();
        return new File([file], newName, {
            type: file.type,
            lastModified: file.lastModified,
        });
    }

    onSwitchSpecImage(keyInd, valueInd, isChecked){
        if (!isChecked){
            delete this.state.filterKeys[keyInd].values[valueInd].specImage
        }
        this.state.filterKeys[keyInd].values[valueInd].hasSpecImage = isChecked
        this.setState(this.state)
    }

    onChangeSpecImage(keyInd, valueInd, image){
        
        let title = `${this.state.filterKeys[keyInd].values[valueInd].id}`
        image = this.renameFile(image, title)
        this.state.filterKeys[keyInd].values[valueInd].specImage = image

        this.setState(this.state)
    }


    onCloseSpecImage(keyInd, valueInd){
        delete this.state.filterKeys[keyInd].values[valueInd].specImage
        this.onSwitchSpecImage(keyInd, valueInd, false)
    }

    onChangeDefProductImages(keyInd, isChecked){
        if (isChecked){
            if (this.state.imagesByFilter === "null"){
                delete this.state.images;
            } else {
                this.state.filterKeys[this.state.imagesByFilter].values.forEach( elem => {
                    delete elem.images;
                });
            }

            this.state.imagesByFilter = keyInd;
            if (keyInd === "null"){
                this.state.images = []
            } else {
                this.state.filterKeys[keyInd].values.forEach(elem => {
                    elem.images = []
                });
            }
            this.setState(this.state)
        } else {
            this.onChangeDefProductImages("null", true)
        }
    }

    onChangeProductImages(keyInd, valueInd, imageInd, newImg){
        this.changeProductImage(keyInd, valueInd, imageInd, newImg)
        this.setState(this.state)
    }

    onChangeProductImagePriority(keyInd, valueInd, imageInd, newVal){
        let images = this.getProductImages(keyInd, valueInd)
        images[imageInd] = newVal;
        this.setState(this.state)
    }

    onRemoveProductImages(keyInd, valueInd, imageInd){
        let images = this.getProductImages(keyInd, valueInd)
        images = images.filter((e, i) => i !== imageInd )
        this.setProductImages(keyInd, valueInd, images)
        this.setState(this.state)
    }

    getProductImages(keyInd, valueInd){
        if (keyInd === "null"){
            return this.state.images;
        }  
        return this.state.filterKeys[keyInd].values[valueInd].images;
    }

    setProductImages(keyInd, valueInd, images){
        if (keyInd === "null"){
            this.state.images = images;
            return
        }
        this.state.filterKeys[keyInd].values[valueInd].images = images;
    }

    changeProductImage(keyInd, valueInd, imageInd, newImg){
        let images = this.getProductImages(keyInd, valueInd)
        let id = "";
        if (keyInd!=="null"){
            id = this.state.filterKeys[keyInd].values[valueInd].id
        }

        let title = `${id}`
        newImg = this.renameFile(newImg, title)
        images[imageInd].image = newImg
    }

    onAddProductImage(keyInd, valueInd){
        if (keyInd==="null"){
            this.state.images.push({priority: 0})
        } else {
            this.state.filterKeys[keyInd].values[valueInd].images.push({priority: 0})
        }
        this.setState(this.state)
    }

    componentWillMount(){
        if(!this.props.filterKeys.data.length){
            this.props.getFilterKeys()
            this.props.getFilterValues()
        }
    }

    componentDidMount(){
        this.props.innerRef(this)
    }

    saveState(){
        this.value = []
        this.images = []
        this.state.filterKeys.forEach((e, i)=>{
            this.value.push(...e.values.map((value, ind) => {
                let obj = {
                    id: value.id,
                    additionalPrice: value.additionalPrice
                }
                if (i===this.state.imagesByFilter && value.images){
                    let images = [];
                    value.images.forEach((img) => {
                        if (img.image){
                            this.images.push(img.image)
                            images.push({image: img.image.name, priority: img.priority})
                        }
                    })
                    obj.images = images
                }
                if (value.hasSpecImage && value.specImage){
                    this.images.push(value.specImage)
                    obj.specImage = value.specImage.name
                }
                return obj
            }))
        })
        if (this.state.imagesByFilter==="null"){
            let images = []
            this.state.images.forEach(e => {
                if(e.image){
                    this.images.push(e.image)
                    images.push({image: e.image.name, priority: e.priority})
                }
            })
            let obj = {id: null, images};
            this.value.push(obj)
        }
        if (!this.value.length){
            this.quantitiesByFilters = [{quantity: this.state.quantity, set: [null]}]
        }
    }
   
    render() {
        this.saveState()
        return (
            <FormGroup>
                <FormGroup>
                    <Label>Not Selectable Filter</Label>
                    {
                        this.state.filterKeys.filter(e => !e.mustChoose).map((selected, i) => (
                            this.filterKeyRenderer(selected, i)
                        ))
                    }
                    { this.filterKeyRenderer({id: "null"}, "null") }
                    <Row>
                        {
                            this.value.length && this.value[0].id !== null
                            ?
                                <Col xs={12}> 
                                    <FormGroup>
                                        <Label>Quantities by filters</Label>
                                        { this.renderQuantities() }
                                    </FormGroup>
                                </Col>
                            :
                                <Col xs={3}> 
                                    <FormGroup>
                                        <Label>Quantity</Label>
                                        <Input 
                                            type="number" 
                                            value={this.state.quantity}
                                            onChange={(e)=> this.setState({quantity: e.target.value}) }
                                        />
                                    </FormGroup>
                                </Col>
                        }
                    </Row>

                </FormGroup>
            </FormGroup>
        );
    }

    filterKeyRenderer = (selected, i) => (
        <FormGroup key={i}>
            <Row>
                <Col sm={5}>
                    <FormGroup>
                        <Input 
                            key={i}
                            type="select" 
                            value={selected.id}
                            onChange={(e)=>this.onSelectFilter(i, e.target.value)}
                        >
                            <option value="null">...</option>
                            {
                                this.props.filterKeys.data.map((key)=>
                                    <option key={key.id} value={key.id}>
                                        {languages.map((lang)=>key[`title_${lang.code}`]).join(" / ")}
                                    </option>
                                )
                            }
                        </Input>
                    </FormGroup>
                </Col>
            </Row>
            {
                selected.id !== "null" 
                ?
                    <Row>
                        <Col xs={{offset: 1, size: 11}}>
                            { 
                                selected.values.map((sv, ind) => (
                                    this.filterValueRenderer(selected.id, i, sv, ind)
                                ))
                            }
                            { this.filterValueRenderer(selected.id, i, {id: "null"}, "null") }
                        </Col>
                    </Row>
                :
                    null
            }

        </FormGroup>
    )

    filterValueRenderer = (keyId, keyInd, valueObj, valueInd) => (
        <Row key={keyInd+"_"+valueInd}>
            <Col md={3}>
                <FormGroup>
                    <Label>Value</Label>
                    <Input 
                        type = "select" 
                        value = {valueObj.id}
                        onChange = {(e)=>this.onSelectFilterValue(keyInd, e.target.value, valueInd)}
                    >
                        <option value="null">...</option>
                        {
                            (this.props.filterValues.data[keyId]||[]).map((val, ind) => (
                                <option value={val.id} key={val.id}>
                                    {languages.map((lang)=>val[`title_${lang.code}`]).join(" / ")}
                                </option>
                            ))
                        }
                    </Input>
                </FormGroup>
            </Col>
        </Row>
    )
}

export default connect(mapStateToProps, mapDispatchToProps)(FilterForm);
