import React from "react";
import {
    FormGroup,
    Label,
    Input,
    Row,
    Col,
    Button
} from 'reactstrap';

import ImageUploadInput from '../../views/components/ImageUploadInput'

import { connect } from 'react-redux';
import { getFilterKeys } from '../../redux/filterKeys/action';
import { getFilterValues } from '../../redux/filterValues/action';

import languages from "../../languages";

const mapStateToProps = state => ({
    filterKeys: state.filterKeys,
    filterValues: state.filterValues
});
const mapDispatchToProps = dispatch => ({
    getFilterKeys: () => getFilterKeys(dispatch),   
    getFilterValues: () => getFilterValues(dispatch),
});


class FilterForm extends React.Component {

    state = {
        filterKeys: [],
        imagesByFilter: "null",
        images: [],
        quantity: 1,
    }

    value = []
    images = {}
    quantitiesByFilters = []

    onChangeQuantitiesByFilters(ind, val){
        this.quantitiesByFilters[ind].quantity = val
        this.forceUpdate()
    }

    onSelectFilter(ind, keyId){
        if (ind==="null" && keyId !== "null"){
            this.state.filterKeys.push({id: keyId, values: []})
        }
        else if (keyId==="null"){
            if (this.state.imagesByFilter === ind){
                this.state.imagesByFilter = "null";
                this.state.images = []
            }
            this.state.filterKeys = this.state.filterKeys.filter((e, i) => i!==ind )
        }
        else{
            this.state.filterKeys[ind] = {id: keyId, values: []}
        }
        this.setState(this.state)
    }
    
    onSelectFilterValue(keyInd, newValue, valueInd){
        if(valueInd === "null" && newValue !== "null"){
            let obj = {additionalPrice: 0, id: newValue}
            if (this.state.imagesByFilter === keyInd)
                obj.images = []
            this.state.filterKeys[keyInd].values.push(obj)
        }else if (newValue==="null"){
            this.state.filterKeys[keyInd].values = this.state.filterKeys[keyInd].values.filter((e, i) => i!==valueInd )
        }
        else{
            this.state.filterKeys[keyInd].values[valueInd].id = newValue
        }
        this.setState(this.state)
    }

    onChangeFilterValue(keyInd, valueInd, attr, value){
        this.state.filterKeys[keyInd].values[valueInd][attr] = value;
        this.setState(this.state)
    }

    renameFile(file, newName) {
        newName += "_"+Date.now()+"."+file.name.split('.').pop();
        return new File([file], newName, {
            type: file.type,
            lastModified: file.lastModified,
        });
    }

    onSwitchSpecImage(keyInd, valueInd, isChecked){
        if (!isChecked){
            delete this.state.filterKeys[keyInd].values[valueInd].specImage
        }
        this.state.filterKeys[keyInd].values[valueInd].hasSpecImage = isChecked
        this.setState(this.state)
    }

    onChangeSpecImage(keyInd, valueInd, image){
        
        let title = `${this.state.filterKeys[keyInd].values[valueInd].id}`
        image = this.renameFile(image, title)
        this.state.filterKeys[keyInd].values[valueInd].specImage = image

        this.setState(this.state)
    }


    onCloseSpecImage(keyInd, valueInd){
        delete this.state.filterKeys[keyInd].values[valueInd].specImage
        this.onSwitchSpecImage(keyInd, valueInd, false)
    }

    onChangeDefProductImages(keyInd, isChecked){
        if (isChecked){
            if (this.state.imagesByFilter === "null"){
                delete this.state.images;
            } else {
                this.state.filterKeys[this.state.imagesByFilter].values.forEach( elem => {
                    delete elem.images;
                });
            }

            this.state.imagesByFilter = keyInd;
            if (keyInd === "null"){
                this.state.images = []
            } else {
                this.state.filterKeys[keyInd].values.forEach(elem => {
                    elem.images = []
                });
            }
            this.setState(this.state)
        } else {
            this.onChangeDefProductImages("null", true)
        }
    }

    onChangeProductImages(keyInd, valueInd, imageInd, newImg){
        this.changeProductImage(keyInd, valueInd, imageInd, newImg)
        this.setState(this.state)
    }

    onChangeProductImagePriority(keyInd, valueInd, imageInd, newVal){
        let images = this.getProductImages(keyInd, valueInd)
        images[imageInd] = newVal;
        this.setState(this.state)
    }

    onRemoveProductImages(keyInd, valueInd, imageInd){
        let images = this.getProductImages(keyInd, valueInd)
        images = images.filter((e, i) => i !== imageInd )
        this.setProductImages(keyInd, valueInd, images)
        this.setState(this.state)
    }

    getProductImages(keyInd, valueInd){
        if (keyInd === "null"){
            return this.state.images;
        }  
        return this.state.filterKeys[keyInd].values[valueInd].images;
    }

    setProductImages(keyInd, valueInd, images){
        if (keyInd === "null"){
            this.state.images = images;
            return
        }
        this.state.filterKeys[keyInd].values[valueInd].images = images;
    }

    changeProductImage(keyInd, valueInd, imageInd, newImg){
        let images = this.getProductImages(keyInd, valueInd)
        let id = "";
        if (keyInd!=="null"){
            id = this.state.filterKeys[keyInd].values[valueInd].id
        }

        let title = `${id}`
        newImg = this.renameFile(newImg, title)
        images[imageInd].image = newImg
    }

    onAddProductImage(keyInd, valueInd){
        if (keyInd==="null"){
            this.state.images.push({priority: 0})
        } else {
            this.state.filterKeys[keyInd].values[valueInd].images.push({priority: 0})
        }
        this.setState(this.state)
    }

    componentWillMount(){
        if(!this.props.filterKeys.data.length){
            this.props.getFilterKeys()
            this.props.getFilterValues()
        }
    }

    componentDidMount(){
        this.props.innerRef(this)
    }

    saveState(){
        this.value = []
        this.images = []
        this.state.filterKeys.forEach((e, i)=>{
            this.value.push(...e.values.map((value, ind) => {
                let obj = {
                    id: value.id,
                    additionalPrice: value.additionalPrice
                }
                if (i===this.state.imagesByFilter && value.images){
                    let images = [];
                    value.images.forEach((img) => {
                        if (img.image){
                            this.images.push(img.image)
                            images.push({image: img.image.name, priority: img.priority})
                        }
                    })
                    obj.images = images
                }
                if (value.hasSpecImage && value.specImage){
                    this.images.push(value.specImage)
                    obj.specImage = value.specImage.name
                }
                return obj
            }))
        })
        if (this.state.imagesByFilter==="null"){
            let images = []
            this.state.images.forEach(e => {
                if(e.image){
                    this.images.push(e.image)
                    images.push({image: e.image.name, priority: e.priority})
                }
            })
            let obj = {id: null, images};
            this.value.push(obj)
        }
        if (!this.value.length){
            this.quantitiesByFilters = [{quantity: this.state.quantity, set: [null]}]
        }
    }
   
    render() {
        this.saveState()
        return (
            <FormGroup>
                <FormGroup>
                    <Label>Filter</Label>
                    {
                        this.state.filterKeys.map((selected, i) => (
                            this.filterKeyRenderer(selected, i)
                        ))
                    }
                    { this.filterKeyRenderer({id: "null"}, "null") }
                    <Row>
                        {
                            this.value.length && this.value[0].id !== null
                            ?
                                <Col xs={12}> 
                                    <FormGroup>
                                        <Label>Quantities by filters</Label>
                                        { this.renderQuantities() }
                                    </FormGroup>
                                </Col>
                            :
                                <Col xs={3}> 
                                    <FormGroup>
                                        <Label>Quantity</Label>
                                        <Input 
                                            type="number" 
                                            value={this.state.quantity}
                                            onChange={(e)=> this.setState({quantity: e.target.value}) }
                                        />
                                    </FormGroup>
                                </Col>
                        }
                    </Row>

                </FormGroup>
            </FormGroup>
        );
    }

    filterKeyRenderer = (selected, i) => (
        <FormGroup key={i}>
            <Row>
                <Col sm={5}>
                    <FormGroup>
                        <Input 
                            key={i}
                            type="select" 
                            value={selected.id}
                            onChange={(e)=>this.onSelectFilter(i, e.target.value)}
                        >
                            <option value="null">...</option>
                            {
                                this.props.filterKeys.data.map((key)=>
                                    <option key={key.id} value={key.id}>
                                        {languages.map((lang)=>key[`title_${lang.code}`]).join(" / ")}
                                    </option>
                                )
                            }
                        </Input>
                    </FormGroup>
                </Col>
                <Col sm={5}>
                    <FormGroup check>
                        <Label check>
                            <Input 
                                type="checkbox" 
                                onChange = {(e)=>this.onChangeDefProductImages(i, e.target.checked)}
                                checked = {i===this.state.imagesByFilter}
                            />
                            Define product IMAGE by this filter
                        </Label>
                    </FormGroup>
                </Col>
            </Row>
            {
                selected.id !== "null" 
                ?
                    <Row>
                        <Col xs={{offset: 1, size: 11}}>
                            { 
                                selected.values.map((sv, ind) => (
                                    this.filterValueRenderer(selected.id, i, sv, ind)
                                ))
                            }
                            { this.filterValueRenderer(selected.id, i, {id: "null"}, "null") }
                        </Col>
                    </Row>
                :
                    null
            }
            {
                selected.id === "null" && this.state.imagesByFilter === "null"
                ?
                    <Col xs={{offset: 1, size: 11}} sm={{offset: 1, size: 6}}> 
                        <FormGroup>
                            <Label>Product Images</Label>
                            <div>
                                { this.imagesRenderer(this.state.images, "null", null) }
                                <Button className="mdi mdi-plus" onClick={()=>this.onAddProductImage("null", null)}></Button>
                            </div>
                        </FormGroup>
                    </Col>
                :
                    null
            }

        </FormGroup>
    )

    filterValueRenderer = (keyId, keyInd, valueObj, valueInd) => (
        <Row key={keyInd+"_"+valueInd}>
            <Col md={3}>
                <FormGroup>
                    <Label>Value</Label>
                    <Input 
                        type = "select" 
                        value = {valueObj.id}
                        onChange = {(e)=>this.onSelectFilterValue(keyInd, e.target.value, valueInd)}
                    >
                        <option value="null">...</option>
                        {
                            (this.props.filterValues.data[keyId]||[]).map((val, ind) => (
                                <option value={val.id} key={val.id}>
                                    {languages.map((lang)=>val[`title_${lang.code}`]).join(" / ")}
                                </option>
                            ))
                        }
                    </Input>
                </FormGroup>
            </Col>
            {
                valueInd!=="null"
                ?
                    <Col md={2}>
                        <FormGroup>
                            <Label>Additional price</Label>
                            <Input 
                                type="number" 
                                value={valueObj.additionalPrice}
                                onChange={(e)=> this.onChangeFilterValue(keyInd, valueInd, "additionalPrice", e.target.value) }
                            />
                        </FormGroup>
                    </Col>
                : 
                    null
            }
            {
                valueInd!=="null"
                ?
                    <Col md={3}>
                        <FormGroup check>
                            <Label check style={{marginTop: 25}}>
                                <Input 
                                    type="checkbox" 
                                    onChange = {(e)=>this.onSwitchSpecImage(keyInd, valueInd, e.target.checked)}
                                    checked = {valueObj.hasSpecImage||false}
                                />
                                Specific image view
                            </Label>
                        </FormGroup>
                    </Col>
                : 
                    null
            }
            
            {
                valueObj.hasSpecImage
                ?
                    <Col xs={{offset: 1, size: 11}} sm={{offset:1, size: 3}}> 
                        <FormGroup>
                            <Label>Special image for this value</Label>
                            <div>
                                <ImageUploadInput 
                                    id="special_img" 
                                    style={{height: 50}}
                                    fileValue = {valueObj.specImage}
                                    onClose={()=>this.onCloseSpecImage(keyInd, valueInd)}
                                    onChange={(img)=>this.onChangeSpecImage(keyInd, valueInd, img)}
                                    />
                            </div>
                        </FormGroup>
                    </Col>
                :
                    null
            }

            {
                valueObj.images
                ?
                    <Col xs={{offset: 1, size: 11}} sm={{offset: 1, size: 6}}> 
                        <FormGroup>
                            <Label>Product Images for this value</Label>
                            <div>
                                { this.imagesRenderer(valueObj.images, keyInd, valueInd) }
                                <Button className="mdi mdi-plus" onClick={()=>this.onAddProductImage(keyInd, valueInd)}></Button>
                            </div>
                        </FormGroup>
                    </Col>
                :
                    null
            }
        </Row>
    )

    imagesRenderer = (images, keyInd, valueInd) => (
        images.map((img, ind)=>(
            <div style={{display: "inline-block"}}>
                <ImageUploadInput 
                    key={`${keyInd}_${valueInd}_${ind}`}
                    id={`product_img_${keyInd}_${valueInd}_${ind}`} 
                    style={{height: 70, margin: "0 5px"}}
                    fileValue = {img.image}
                    onClose = {()=>this.onRemoveProductImages(keyInd, valueInd, ind)}
                    onChange = {(e)=>this.onChangeProductImages(keyInd, valueInd, ind, e)}
                />
                <Input 
                    type="number" 
                    style={{width: 75}} 
                    value={img.priority}
                    onChange = {(e)=>this.onChangeProductImagePriority(keyInd, valueInd, ind, e.target.value)}        
                    />
            </div>
        ))
    )


    getOldSetIfExists(set){
        let exist = false;
        for (let k = 0; k < this.quantitiesByFilters.length; k++){
            let e = this.quantitiesByFilters[k]
            if (e.set.length === set.length){
                let matches = true;
                for (let i = 0; i < e.set.length; i++)
                    if (set.indexOf(e.set[i]) === -1)
                        matches = false;
                if (matches)
                    return e;
            }
        }
        return {set, quantity: 0}
    }

    getSetsWithQuantity(sets){
        return sets.map( e => (
            this.getOldSetIfExists(e)
        ))
    }

    renderQuantities = () => { 
        let filterSets = this.getSelectedFilterValues(0);
        this.quantitiesByFilters = this.getSetsWithQuantity(filterSets);
        return this.quantitiesByFilters.map( (elem, ind) => {
            return (
                <div key={ind}>
                    <Input 
                        style={{width: 100, display: "inline-block", marginRight: 5}}
                        type="number" 
                        value={this.quantitiesByFilters[ind].quantity}
                        onChange={ (e)=>this.onChangeQuantitiesByFilters(ind, e.target.value) }
                    />
                    {
                        this.props.filterValues.array.filter( e => elem.set.indexOf(String(e.id)) > -1)
                            .map( (val) => (
                                <span key={val.id} style={{margin: "0 10px"}}>
                                    {languages.map((lang)=>val[`title_${lang.code}`]).join(" / ")}
                                </span>
                            ))
                    }
                </div>
            )
        })
    }

    getSelectedFilterValues(keyInd){
        let keys = this.state.filterKeys
        if (keys.length <= keyInd)
            return [[]]

        let next = this.getSelectedFilterValues(keyInd + 1)
        
        let temp = [] 
        keys[keyInd].values.forEach( e => {
            temp.push(
                ...next.map( nxt => (
                    [e.id, ...nxt]
                ))
            )
        })
        if (!temp.length)
            return [[]]
        return temp;
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(FilterForm);
