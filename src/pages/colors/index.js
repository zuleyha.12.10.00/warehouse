import React from "react";
import {
    Card,
    CardBody,
    CardHeader,
    Modal,
    ModalHeader,
    ModalBody,
    ModalFooter,
    Button,
    Input,
    FormGroup,
    Label,
    ButtonGroup
} from 'reactstrap';
import MultiLangInput from '../../views/components/MultiLangInput'
import Table from './table.category'

import { connect } from 'react-redux';
import { 
    getColor, 
    createColor, 
    deleteColor, 
    updateColor
} from '../../redux/colors/action';


const mapStateToProps = state => ({
    colors: state.colors
});
const mapDispatchToProps = dispatch => ({
    getColor: () => getColor(dispatch),  
    createColor: (body) => createColor(dispatch, body),
    deleteColor: (id) => deleteColor(dispatch, id),
    updateColor: (id, body) => updateColor(dispatch, id, body)
});

const colorsOptions = {
    1: "Operator",
    2: "Türk",
    3: "Warehouse",
    4: "Curier",
    5: "Kassir"
}

class Color extends React.Component {
    state = {
        data: [],
        create: false
    }
    

    onClickCreate(row){
        this.setState({create: true})
    }

    onCancelCreate(){
        this.setState({create: false})
    }

    createColor(){
        if (!this.password.value.length || !this.username.value.length){
            alert("Username and password can't be emty")
            return
        }

        let body = {
            name: this.name.value,
            username: this.username.value,
            password: this.password.value,
            phonenumber: this.phonenumber.value,
            status: this.status.value
        }

        this.props.createColor(body)
        this.onCancelCreate()
    }

    componentWillMount(){
        if (!this.props.colors.data.length)
            this.props.getColor();
    }

    render() {
        return (
            <div>
                <Card>
                    <CardHeader>
                        <h3 style={{ display: "inline-block" }}>Color</h3>
                        <ButtonGroup style={{float: "right"}}>
                            <Button 
                                color="primary" 
                                onClick={()=>this.onClickCreate()}
                            >
                                Create new
                            </Button>
                        </ButtonGroup>
                    </CardHeader>
                    <CardBody>
                        <Table
                            data = {this.props.colors.data}
                            onClickCreate = {(row)=>this.onClickCreate(row)}
                            onDelete = {this.props.deleteColor}
                            onUpdate = {this.props.updateColor}
                            colorsOptions = {colorsOptions}
                        />
                    </CardBody>
                </Card>
              
                <Modal
                    isOpen={this.state.create}
                    toggle={()=>this.onCancelCreate()}
                    className={this.props.className}
                    fade={false}
                >
                    <ModalHeader toggle={()=>this.onCancelCreate()}>
                        Create colors 
                    </ModalHeader>
                    <ModalBody>
                        <FormGroup>
                            <Label for="name">Name*</Label>
                            <Input innerRef={(node) => this.name = node} name="name"/>
                        </FormGroup>
                        <FormGroup>
                            <Label for="user">Username*</Label>
                            <Input innerRef={(node) => this.username = node} name="user"/>
                        </FormGroup>
                        <FormGroup>
                            <Label for="password">Password*</Label>
                            <Input innerRef={(node) => this.password = node} name="password"/>
                        </FormGroup>
                        <FormGroup>
                            <Label for="phone">Phone number (optional)</Label>
                            <Input innerRef={(node) => this.phonenumber = node} name="phone"/>
                        </FormGroup>
                        <FormGroup>
                            <Label for="Price">Status*</Label>
                            <Input type="select" innerRef={(node) => this.status = node}>
                                {
                                    Object.keys(colorsOptions).map( key => (
                                        <option key={key} value={key}>{colorsOptions[key]}</option>
                                    ))
                                }
                            </Input>
                        </FormGroup>
                    </ModalBody>
                    <ModalFooter>
                        <Button color="primary" onClick={()=>this.createColor()}>
                            Save
                        </Button>
                        <Button color="secondary" onClick={()=>this.onCancelCreate()}>
                            Cancel
                        </Button>
                    </ModalFooter>
                </Modal>
            </div>
        );
    }

}


export default connect(mapStateToProps, mapDispatchToProps)(Color);
