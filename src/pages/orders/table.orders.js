import React from "react";
import {
	Button, Input
} from 'reactstrap';

import BootstrapTable from 'react-bootstrap-table-next';
import 'react-bootstrap-table-next/dist/react-bootstrap-table2.min.css';
import filterFactory, { textFilter, dateFilter } from 'react-bootstrap-table2-filter';
import cellEditFactory, { Type } from 'react-bootstrap-table2-editor';

import paginationFactory from 'react-bootstrap-table2-paginator';

import images from "../../constants/images";
import OrderProductTable from "./table.orderProducts";
import formatPrice from "../../constants/formatPrice";
import { status as userStatus } from "../users/table.users";

const statusObj = {
    "-1": "Klient otmen etdi",
    0: "Otmen edildi",
    1: "Zakaz etmeli",
    2: "Zakaz edildi",
    3: "TM ugradyldy",
    4: "Kliende gowşuryldy"  
}


class Table extends React.Component {

    onUpdate(oldValue, newValue, row, column, done) {
        if (column.dataField === "status") {
            this.props.updateOrderProduct(row.id, { status: newValue })
            done(true);
        } 
        else if (column.dataField === "originOrderId") {
            this.props.updateOrder(row.id, { originOrderId: newValue })
            done(true);
        } else {
            done(false);
        }
        return { async: true };
    }

    getOrderData(){
        // if(this.props.userId){
        //     return this.props.orders.data.filter( e => e.userId === this.props.userId )
        // }
        return this.props.orders || []
    }

    onTableChange(type, {filters}){
        clearTimeout(this.change)
        this.change = setTimeout(()=>{
            let obj = {}
            for (const dataField in filters) {
                obj[dataField] = filters[dataField].filterVal
            }
            console.log(obj)
            this.props.updateFilter(obj)
        }, 2000)
    }

    render() {
        return (
            <div>
                <BootstrapTable
                    bootstrap4
                    keyField='id'
                    data={this.getOrderData()}
                    columns={this.getColumns()}
                    expandRow={this.expandRow}
                    filter={filterFactory()}
                    noDataIndication={'No results found'}
                    remote={{filter: true}}
                    onTableChange = {(type, newState)=>this.onTableChange(type, newState)}
                    selectRow = {{
                        mode: 'checkbox',
                        clickToSelect: false,
                        onSelect: (row, isSelect) => this.props.selectRow(isSelect, row),
                        onSelectAll: (isSelect, rows) => this.props.selectAllRow(isSelect, rows)
                    }}
                    defaultSorted = {[{
                        dataField: 'createdAt',
                        order: 'desc'
                      }]}
                    cellEdit={ cellEditFactory({ 
                        mode: 'click', 
                        blurToSave: true,
                        beforeSaveCell: this.onUpdate.bind(this)
                    }) }
                    rowStyle = {()=>({fontSize: 13})}
                />
            </div>
        )
    }

    expandRow = {
        showExpandColumn: true,
        expandByColumnOnly: true,
        renderer: row => (
            <OrderProductTable {...this.props} data={row.products.map(p => ({...p, id: "id_"+p.orderProductId, discount: row.discount}))}/>
        ),
        expandHeaderColumnRenderer: ({ isAnyExpands }) => {
            if (isAnyExpands) {
                return <span>-</span>;
            }
            return <span>+</span>;
        },
        expandColumnRenderer: ({ expanded }) => {
            if (expanded) {
                return (
                    <span>-</span>
                );
            }
            return (
                <span>+</span>
            );
        }
    };

    formateDateTime(time){
        console.log(time)
        var date = new Date(new Date(time).valueOf() - 1000 * 60 * 60 * 5);
        var year = date.getFullYear();
        var month = (date.getMonth() +1);
        var day = date.getDate();
        
        var hour = date.getHours();
        var minute = date.getMinutes();
        
        return this.formateTime(year, month, day, hour, minute);
      }
      
      formateTime(year, month, day, hour, minute){
        return this.makeDoubleDigit(year) + "/" + 
               this.makeDoubleDigit(month) + "/" + 
               this.makeDoubleDigit(day) + " " + 
               this.makeDoubleDigit(hour) + ":" + 
               this.makeDoubleDigit(minute);
      }
      
      makeDoubleDigit(x){
        return (x < 10) ? "0" + x : x;
      }

    getColumns() {
        return [
            {
                headerStyle: (colum, colIndex) => ({ width: '100px', textAlign: 'center' }),
                dataField: 'id',
                text: 'ID',
                filter: textFilter()
            },
            {
                headerStyle: (colum, colIndex) => ({ width: '150px', textAlign: 'center' }),
                dataField: 'originOrderId',
                text: 'Origin ID',
                filter: textFilter()
            },
            {
                headerStyle: (colum, colIndex) => ({ width: '200px', textAlign: 'center' }),
                filter: textFilter(),
                dataField: "user",
                text: "Buyer",
                editable: false,
                formatter: (cell, row) => (<div>
                    {row.name} {row.userStatus ? `[${userStatus[row.userStatus]}]` : null} 
                    , {row.phoneNumber} 
                    
                    {row.note && <hr/>}
                    {row.note} 
                </div>)
            },
            {
                headerStyle: (colum, colIndex) => ({ width: '110px', textAlign: 'center' }),
                dataField: "createdAt",
                text: "Date",
                editable: false,
                formatter: (cell, row) => (this.formateDateTime(row.createdAt))
            },
            {
                headerStyle: (colum, colIndex) => ({ width: '250px', textAlign: 'center' }),
                dataField: "total",
                text: "Total",
                editable: false,
                formatter: (cell, row) => {
                    let totalTL = row.products.reduce((total, product) => total + product.originalPrice * product.quantity, 0)
                    let totalTMT = row.products.reduce((total, product) => total + product.price * product.quantity, 0)
                    let totalDiscount = totalTMT * (row.discount/100 || 0)
                    return (
                        <div>
                            <b>{row.products.length}</b> product(s) 
                            <span style={{margin: "0px 10px", color: "#000", fontWeight: 500}}>{totalTL} TL</span>
                            <br/>
                            {formatPrice(totalTMT)} TMT
                            <span style={{color: "red"}}> - {formatPrice(totalDiscount)}({row.discount}%) TMT</span> = {formatPrice(totalTMT - totalDiscount)} TMT
                        </div>
                    )
                }
            },
            {
                headerStyle: (colum, colIndex) => ({ textAlign: 'center' }),
                dataField: "address",
                text: "Address",
                editable: false,
                formatter: (cell, row) => (<div>
                    {row.deliveryPhoneNumber} 
                    {row.deliveryPhoneNumber && <br/>}
                    
                    <u>{row.city}</u>, {" "+row.address}
                </div>)
            },
            {
                text: "Action",
                dataField: "createChild",
                editable: false,
                headerStyle: (colum, colIndex) => ({ width: '100px', textAlign: 'center' }),
                formatter: (cellContent, row) => (
                    <Button disabled={!row.products.filter(p => p.status == 4).length} onClick={()=>this.printCheck(row)} color="primary">🖨</Button>

                ),
                style: { textAlign: "center", verticalAlign: "middle" }
            }
        ];
    }


    getStatus(cell){
        switch (Number(cell)) {
            case -1:
                return <div>Klient otmen etdi</div>
            case 0:
                return <div>Otmen edildi</div>
            case 1:
                return <div className='text-danger'>Zakaz etmeli</div>
            case 2:
                return <div className='text-success'>Zakaz edildi</div>
            case 3:
                return <div className='text-info'>TM ugradyldy</div>
            case 4:
                return <div  style={{color: "#e9c308"}}>Kliende gowşuryldy</div>
            default:
                return cell
        }
    }

    printCheck(row){
        var mywindow = window.open('', 'PRINT', 'height=400,width=600');
        
        let totalTMT = row.products.reduce((total, product) => total + product.price * product.quantity, 0)
        let totalDiscount = totalTMT * (row.discount/100 || 0)
        
        let printview = `
            <!DOCTYPE html>
            <html lang="en">
                <head>
                    <meta charset="UTF-8">
                    <meta http-equiv="X-UA-Compatible" content="IE=edge">
                    <meta name="viewport" content="width=device-width, initial-scale=1.0">
                    <title>Recept</title>
                
                    <style>
                        body {
                            font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif
                        }
                
                        .cont {
                            padding: 15px;
                            padding-left: 30px;
                        }
                
                        .order-cont {
                            background-color: #161616;
                            color: white;
                            position: relative;
                        }
                        .lenta {
                            width: 0;
                            height: 0;
                            border: 24.5px solid transparent;
                            border-right: 0;
                            border-left: 22px solid #333;
                            position: absolute;
                            right: -22px;
                            top: -1;
                            bottom: 0;
                        }
                        .info {
                            padding: 30px;
                            padding-left: 30px;
                        }
                
                
                        table.orders {
                            width: 100%;
                        }
                        table.orders td {
                            padding: 10px;
                            font-size: 14px;
                        }
                        
                        table.orders thead th {
                            text-align: start;
                            width: 15%;
                            padding: 10px;
                        }
                        table.orders thead th:first-child {
                            width: 55%;
                        }
                
                        table {
                            border-collapse:collapse;
                        }
                        tr {
                            border:none;
                        }
                        th, td {
                            /* border-collapse:collapse; */
                            border-bottom: 1px solid #999;
                            padding-top:0;
                            padding-bottom:0;
                        }
                        .verticalSplit {
                            border-top:none;
                            border-bottom:none;
                        }
                        .verticalSplit:first-of-type {
                            border-left:none;
                        }
                        .verticalSplit:last-of-type {
                            border-right:none;
                        }
                
                        .result {
                            width: 40%;
                            float: right;
                            font-size: 14px;
                        }
                        .result span {
                            padding: 10px 0;
                            display: inline-block;
                        }
                
                        .result span:first-child {
                            width: 100px;
                        }
                        .result .main {
                            font-weight: bold;
                            font-size: 16px;
                        }
                        div.footer {
                            text-align: center;
                            position: fixed;
                            bottom: 30px;
                            left: 0;
                            right: 0;
                        }
                    </style>
                </head>
                <body>
                    <div>
                        <div style="display: inline-block; width: 50%;">
                            <h1 class="cont"><img src="https://sargagelsin.com/images/logo.png" height="40px"/></h1>
                            <div class="order-cont cont">
                                Zakaz:  <b>#${row.id}</b>
                                <!-- <div class="lenta"></div> -->
                            </div>
                            <div class="info cont">
                                <b>${row.city}</b>
                                <br/>
                                <br/>
                                ${row.address}
                            </div>
                        </div>
                        <div style="display: inline-block; width: 39%; margin-left: 10%;">
                            <h1 class="cont">Faktura</h1>
                            <div class="cont">
                                Sene:  <b>${this.formateDateTime(row.createdAt)}</b>
                            </div>
                            <div class="info cont">
                                <b>${row.name}</b>
                                <br/>
                                <br/>
                                ${row.phoneNumber}
                            </div>
                        </div>
                    </div>
                    <hr/>
                    <div class="cont detail">
                        <table class="orders">
                            <thead>
                                <th>Haryt</th>
                                <th>Bahasy</th>
                                <th>Sany</th>
                                <th>Jemi</th>
                            </thead>
                            <tbody>
                                ${
                                    row.products.filter(p => p.status == 4).map( p => (
                                        `
                                            <tr>
                                                <td>${p.brand} - ${p.productTitle}</td>
                                                <td>${formatPrice(p.price)} TMT</td>
                                                <td>${p.quantity}</td>
                                                <td>${formatPrice(p.price, p.quantity)} TMT</td>
                                            </tr>
                                        `
                                    )).join("\n")
                                }
                            </tbody>
                        </table>
                        <div class="result">
                            <div>
                                <span>Jemi: </span>
                                <span>${formatPrice(totalTMT)} TMT</span>
                            </div>
                            <div>
                                <span>Arzanladyş:</span>
                                <span>-${formatPrice(totalDiscount)}(${row.discount}%) TMT</span>
                            </div>
                            <div class="main">
                                <span>Netije: </span>
                                <span>${formatPrice(totalTMT-totalDiscount)} TMT</span>
                            </div>
                        </div>
                    </div>
                    <div class="footer">Söwdaňyz üçin sag boluň!</div>
                </body>
            </html>
        `   
        

        mywindow.document.body.innerHTML = printview
        // console.log(view, mywindow.document.body);

        mywindow.document.close(); // necessary for IE >= 10
        mywindow.focus(); // necessary for IE >= 10*/

        mywindow.print();
        setTimeout(mywindow.close, 100)
    }
}

export default (Table);
