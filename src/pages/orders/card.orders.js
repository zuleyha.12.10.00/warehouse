import React from "react";


import BootstrapTable from 'react-bootstrap-table-next';
import 'react-bootstrap-table-next/dist/react-bootstrap-table2.min.css';
import filterFactory, { textFilter, selectFilter } from 'react-bootstrap-table2-filter';
import cellEditFactory, { Type } from 'react-bootstrap-table2-editor';
import paginationFactory from 'react-bootstrap-table2-paginator';

import languages from '../../languages';
import images from "../../constants/images";
import Card from "./card"
import { Col, Row } from "reactstrap";

const statusObj = {
    "-1": "Klient otmen etdi",
    0: "Otmen edildi",
    1: "Zakaz etmeli",
    2: "Zakaz edildi",
    3: "TM ugradyldy",
    4: "Kliende gowşuryldy"  
}

class Table extends React.Component {

    onUpdateStatus(id, newValue) {
        this.props.updateOrderStatus(id, { status: newValue })
    }

    getFilterValuesTitle(id) {
        let filterValues = this.props.filterValues.array
        for (let i = 0; i < filterValues.length; i++) {
            if (String(filterValues[i].id) === String(id)) {
                return filterValues[i][`title_${languages[0].code}`]
            }
        }
    }

    render() {
        return (
            <Row>
                {
                    this.props.orders.map( (o, i) => (
                        <Col xs={12} sm={6} key={i}>
                            <Card 
                                {...o} 
                                getFilterValuesTitle={(filterId)=>this.getFilterValuesTitle(filterId)}
                                onChangeStatus={(id, status) => this.onUpdateStatus(id, status)}
                                />
                        </Col>
                    ))
                }
            </Row>
        )
    }

    formateDateTime(time){
        var date = new Date(time);
        
        var year = date.getFullYear();
        var month = (date.getMonth() +1);
        var day = date.getDate();
        
        var hour = date.getHours();
        var minute = date.getMinutes();
        
        return this.formateTime(year, month, day, hour, minute);
      }
      
      formateTime(year, month, day, hour, minute){
        return this.makeDoubleDigit(year) + "/" + 
               this.makeDoubleDigit(month) + "/" + 
               this.makeDoubleDigit(day) + " " + 
               this.makeDoubleDigit(hour) + ":" + 
               this.makeDoubleDigit(minute);
      }
      
      makeDoubleDigit(x){
        return (x < 10) ? "0" + x : x;
      }

    getColumns() {
        return [
            {
                dataField: 'id',
                text: 'ID',
                sort: true,
                filter: textFilter(),
                hidden: true
            },
            {
                dataField: 'createdAt',
                text: 'Date of order',
                sort: true,
                filter: textFilter(),
                editable: false,
                formatter: (cell) => this.formateDateTime(cell)
            },
            {
                dataField: "title",
                text: "Title",
                sort: true,
                editable: false,
                filter: textFilter(),
                formatter: (cell, row) => <span> {row.productId} <hr/> {cell || <span style={{color: "red"}}>[no longer exists]</span>}</span>
            },
            {
                dataField: "image",
                text: "Image",
                editable: false,
                formatter: (cell) => {
                    if (cell)
                        return <img width="80%" src={images.getImagePath(cell)}></img>
                    return cell
                },
                style: {
                    textAlign: "center"
                }
            },
            {
                dataField: "color",
                text: "Filters",
                editable: false,
                formatter: (cell, row) => (
                    (cell && row.size) ? (cell + " / " + row.size) : (cell || row.size)
                )
            },
            {
                dataField: "price",
                text: "Price",
                editable: false,
                formatter: (cell, row) => (
                    <div>
                        <strong>{row.quantity}</strong> x {cell} TMT
                        {
                            row.originalPrice ?
                            <div>
                                <br/>
                                <strong>{row.quantity}</strong> x {row.originalPrice} TL
                            </div>
                            : null
                        }
                    </div>
                )
            },
            {
                dataField: "originalLink",
                text: "Link",
                editable: false,
                formatter: (cell, row)=>(
                    cell || row.productOriginalLink
                    ?
                        <a target="_blank" href={cell || row.productOriginalLink} style={{maxHeight: 83, overflowY: "hidden"}}>
                            {(cell || row.productOriginalLink).substring(0,40) + ((cell || row.productOriginalLink).length>100 ? "..." : "")}
                        </a>
                    :
                        row.productDBId
                        ?
                            "В наличии"
                        :
                            null
                )
            },
            {
                dataField: "phoneNumber",
                text: "Buyer",
                filter: textFilter(),
                editable: false,
                formatter: (cell, row) => (<div>
                    {row.firstName} 
                    <br/>
                    {row.phoneNumber} 
                    <br/>
                    <br/>
                    {row.city}, 
                    {" "+row.address}
                </div>)
            },
            {
                dataField: "status",
                text: "Status",
                sort: true,
                editor: {
                    type: Type.SELECT,
                    getOptions: (setOptions, { row, column }) => (
                        Object.keys(statusObj).map( e => ({ value: e, label: statusObj[e] }) )
                    )
                },
                formatter: (cell) => {
                    switch (Number(cell)) {
                        case -1:
                            return <div>Klient otmen etdi</div>
                        case 0:
                            return <div>Otmen edildi</div>
                        case 1:
                            return <div className='text-danger'>Zakaz etmeli</div>
                        case 2:
                            return <div className='text-success'>Zakaz edildi</div>
                        case 3:
                            return <div className='text-info'>TM ugradyldy</div>
                        case 4:
                            return <div  style={{color: "#e9c308"}}>Kliende gowşuryldy</div>
                        default:
                            return cell
                    }
                },
                filter: selectFilter({
                    options: statusObj
                })
            },
            // {
            //     text: "Action",
            //     dataField: "createChild",
            //     formatter: (cellContent, row) => (
            //         <ButtonGroup>
            //             <Link to={"/products/"+row.id}>
            //                 <Button 
            //                     color="primary" 
            //                     className="mdi mdi-eye" 
            //                     />
            //                 </Link>
            //             <Button color="danger" className="mdi mdi-delete" onClick={() => this.onClickDelete(row.id)}></Button>
            //         </ButtonGroup>
            //     ),
            //     style: { textAlign: "center", verticalAlign: "middle" }
            // }
        ];
    }
}

export default (Table);
