import React from "react";
import {
	Card,
	CardBody,
    CardHeader, Button,
    Input,
    Dropdown,
    Nav,
    NavItem,
    NavLink,
    TabContent,
    TabPane,
    Row,
    Col,
    CardTitle,
    CardText
} from 'reactstrap';
import { getToken, onMessageListener, messaging } from '../../constants/firbase';

import Table from './table.orders';
import CardRow from './card.orders';

import { connect } from 'react-redux';
import { getOrders, updateOrderProduct, updateOrder, deleteOrder, updateOrderFilter } from '../../redux/orders/action';

import { registerAdmin } from "../../redux/orders/action";
import { statusObj } from "./table.orderProducts";

const mapStateToProps = state => ({
    orders: state.orders
});
const mapDispatchToProps = dispatch => ({
    // getOrders: (page, params, callback) => getOrders(dispatch, page, params, callback),
    // updateOrderFilter: (params, callback) => updateOrderFilter(dispatch, params, callback),
    // updateOrderProduct: (id, body) => updateOrderProduct(id, body, dispatch),
    // updateOrder: (id, body) => updateOrder(id, body, dispatch),
    // deleteOrder: (body) => deleteOrder(body, dispatch)
});

const categories = [
    {
        'name' : 'filtr 1 mikron',
        'total' : '235',
        'unit' : 'san',
        'coefficient' : '1',
        'add-unit' : 'san',
        'add-weight' : '235',
        'available' : 'yaramly'},
    {
        'name' : 'filtr 5 mikron',
        'total' : '235',
        'unit' : 'san',
        'coefficient' : '1',
        'add-unit' : 'san',
        'add-weight' : '235',
        'available' : 'yaramly'},
    {
        'name' : 'plasmas gara',
        'total' : '235',
        'unit' : 'san',
        'coefficient' : '1',
        'add-unit' : 'san',
        'add-weight' : '235',
        'available' : 'yaramly'}
]
    

class Products extends React.Component {
    constructor(props) {
        super(props);
        this.toggle = this.toggle.bind(this);
    this.state = {
        isTokenFound: false,
        activeTab: '1'
    }
}

toggle(tab) {
    if (this.state.activeTab !== tab) {
      this.setState({
        activeTab: tab
      });
    }
  }
    

    selected = []

    isLoading = false

    componentWillMount() {
        if (!this.props.orders.data.length) {
            //this.getOrders()
        }
    }

    // getOrders(){
    //     this.props.getOrders(this.props.orders.page, {dateFrom: this.props.orders.filter.dateFrom, dateTo: this.props.orders.filter.dateTo})
    // }

    // filterByNewFilter(){
    //     this.props.updateOrderFilter({status: this.status.value, dateFrom: this.dateFrom.value, dateTo: this.dateTo.value})
    // }

    // updateFilter(filter){
    //     this.props.updateOrderFilter({status: this.status.value, dateFrom: this.dateFrom.value, dateTo: this.dateTo.value, ...filter})
    // }

    componentDidMount(){
        let _this = this;
        
        if (!messaging)
            return;

        if (this.props.currentUser.status !== 0 && this.props.currentUser.status !== 1)
            return

        messaging.requestPermission()
        .then(function() {
            console.log('Notification permission granted.');
            
            // getToken((code)=> {
            //     if (code){
            //         registerAdmin(code)
            //     }
            // });

            onMessageListener().then(payload => {
                // setNotification({title: payload.notification.title, body: payload.notification.body})
                alert(payload);
            }).catch(err => console.log('failed: ', err));
        })
        .catch(function(err) {
            console.log('Unable to get permission to notify. ', err);
        });
    }

    // loadMore(ev) {
    //     if (!this.props.orders.isEnd){
    //         this.isLoading = true
    //         this.getOrders()
    //     }
    // };

    // deleteSelected(){
    //     if (!this.selectedRows || !this.selectedRows.length) {
    //         this.props.deleteOrder({ids: JSON.stringify(this.selected)})
    //         this.selectedRows = []
    //     }
    // }

    // selectRow(isSelect, row){
    //     if ( isSelect )
    //         this.selected.push(row.id)
    //     else
    //         this.selected = this.selected.filter( e => e !== row.id )
    //     return true;
    // }

    // selectAllRow(isSelect, rows){
    //     if (isSelect)
    //         this.selected = rows.map( e => e.id );
    //     else
    //         this.selected = []
    //     return this.selected
    // }

	render() {
        console.log(this.props.orders.filter.status)
		return (
            <div>
                
                    <div>
        <Nav tabs>
          <NavItem>
            <NavLink
              className={this.state.activeTab === '1' ? 'active' : ''}
              onClick={() => { this.toggle('1'); }}
            >
              beylekiler
            </NavLink>
          </NavItem>
          <NavItem>
            <NavLink
              className={this.state.activeTab === '2' ? 'active' : ''}
              onClick={() => { this.toggle('2'); }}
            >
              suwun gosundylary
            </NavLink>
          </NavItem>
        </Nav>
        <Card>
                    <CardBody>
        <TabContent activeTab={this.state.activeTab}>
          <TabPane tabId="1">
            <table className="table table-striped table-borderless">
                <tbody>
                {
                    categories.map((desc,ind) => (
                    <tr key={ind}>
                        {/*<th scope="row">{t('Brand Name.1')}:</th>*/}
                        <td style={{'paddingLeft' : '10px'}} scope="row">{desc.name}</td>
                    </tr>
                    ))
                }
                </tbody>
            </table>
          </TabPane>
          <TabPane tabId="2">
            <Row>
              <Col sm="6">
                <Card body>
                  <CardTitle>Special Title Treatment</CardTitle>
                  <CardText>With supporting text below as a natural lead-in to additional content.</CardText>
                  <Button>Go somewhere</Button>
                </Card>
              </Col>
              <Col sm="6">
                <Card body>
                  <CardTitle>Special Title Treatment</CardTitle>
                  <CardText>With supporting text below as a natural lead-in to additional content.</CardText>
                  <Button>Go somewhere</Button>
                </Card>
              </Col>
            </Row>
          </TabPane>
        </TabContent>
        </CardBody>
                </Card>
      </div>
                    

               </div>
		);
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(Products);