import React from "react";

import {
    Row,
    Card, 
    CardBody,
    Col
} from 'reactstrap'

import helper from '../../constants/images'

const statusColor = {
    "-1": "#cccccc80",
    0: "#cccccc80",
    1: "#ff00001f",
    2: "#00800030",
    3: "#0000ff26",
    4: "#ffff0029"  
}

const statusObj = {
    "-1": "Klient otmen etdi",
    0: "Otmen edildi",
    1: "Zakaz etmeli",
    2: "Zakaz edildi",
    3: "TM ugradyldy",
    4: "Kliende gowşuryldy"  
}

const statusUser = {
    "-1": "📵",
    0: "🌤",
    1: "☀️",
    2: "🧿"
}


export default (props) => {
    const onChange = (e) => {
        props.onChangeStatus(props.id, e.target.value)
    }

    const formateDateTime = (time) => {
        var date = new Date(time);
        
        var year = date.getFullYear();
        var month = (date.getMonth() +1);
        var day = date.getDate();
        
        var hour = date.getHours();
        var minute = date.getMinutes();
        
        return formateTime(year, month, day, hour, minute);
      }
      
    const formateTime = (year, month, day, hour, minute) => {
        return makeDoubleDigit(year) + "/" + 
               makeDoubleDigit(month) + "/" + 
               makeDoubleDigit(day) + " " + 
               makeDoubleDigit(hour) + ":" + 
               makeDoubleDigit(minute);
    }

    const makeDoubleDigit = (x) => {
        return (x < 10) ? "0" + x : x;
    }

    return(
        <Card>
            <CardBody style={{background: statusColor[props.status]}}>
                <Row>
                    <Col xs={12} style={{color: props.productDBId ? "#777" : "red", fontSize: 13}}>{props.title || <span style={{color: "red"}}>[no longer exists]</span>}</Col>
                    <Col xs={5} hidden={!props.productDBId}>
                        <img src={helper.getThumbPath(props.image)} width="100%"/>
                    </Col>
                    <Col xs={props.productDBId ? 7 : 12}>
                        <Row style={{fontSize: 13}}>{formateDateTime(props.createdAt)}</Row>
                        <Row> <span style={{paddingRight: 5}}>{props.quantity} x</span> <span style={{color: '#aaa', marginRight: 10}}>{props.price} TMT</span> {props.originalPrice ? props.originalPrice+"TL" : ""}</Row>
                        <Row>{
                            (props.color && props.size) ? (props.color + " / " + props.size) : (props.color || props.size)
                        }</Row>
                        <Row>{statusUser[props.userStatus] + " " +props.firstName}</Row>
                        <Row><a href={`tel:${props.phoneNumber}`}>{props.phoneNumber}</a></Row>
                        <Row style={{fontSize: 13}}>{props.city},</Row>
                        <Row style={{fontSize: 13}}>{props.address}</Row>
                    </Col>
                    <a style={{fontSize: 13, overflow: "hidden", textOverflow: "ellipsis", display: "-webkit-box", WebkitLineClamp: 2, WebkitBoxOrient: "vertical"}} href={props.originalLink || props.productOriginalLink}>{props.originalLink || props.productOriginalLink ? (props.originalLink || props.productOriginalLink||"") : "[in stock]"}</a>
                    <select value={props.status} style={{width: "100%", margin: "10px 0"}} onChange={(e)=>onChange(e)}>
                        {
                            Object.keys(statusObj).map(e => (
                                <option value={e} key={e}>{statusObj[e]}</option>
                            ))
                        }
                    </select>
                </Row>
            </CardBody>
        </Card>
    )
}