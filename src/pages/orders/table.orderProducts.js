    import React from "react";


import BootstrapTable from 'react-bootstrap-table-next';
import 'react-bootstrap-table-next/dist/react-bootstrap-table2.min.css';
import filterFactory, { textFilter, selectFilter } from 'react-bootstrap-table2-filter';
import cellEditFactory, { Type } from 'react-bootstrap-table2-editor';
import paginationFactory from 'react-bootstrap-table2-paginator';
import {
    Card,
    CardBody,
    CardHeader,
    Modal,
    ModalHeader,
    ModalBody,
    ModalFooter,
    Button,
    Input,
    FormText,
    FormGroup,
    Label,
    ButtonGroup,
    Row,
    Col
} from 'reactstrap';
import images from "../../constants/images";
import formatPrice from "../../constants/formatPrice";

export const statusObj = {
    // "-1": "Klient otmen etdi",
    "-1": {title: "Operator Ýatyrdy", edit: [0, 1]},
    "-2": {title: "Türkde Ýatyryldy", edit: [0, 1, 2]},
    "-3": {title: "Wearhouse Ýatyrdy", edit: [0, 1, 3]},
    "-4": {title: "Curier Ýatyrdy", edit: [0, 1, 4]},
    1: {title: "Garaşylýar", edit: [0, 1]}, // 
    2: {title: "Kabul edildi", edit: [0, 1, 2]}, // turkdaki
    3: {title: "Türke geldi", edit: [0, 1, 2, 3]}, // wearhouse
    4: {title: "Warehouse-da", edit: [0, 1, 3, 4]}, // kurier
    5: {title: "Habar berildi", edit: [0, 1, 4]}, // kurier
    6: {title: "Gowşuryldy", edit: [0, 1, 4, 5]},
    7: {title: "Tamamlandy", edit: [0, 1, 5]}
}

const statusColors = {
    // "-1": "Klient otmen etdi",
    "-1": "red",
    "-2": "red",
    "-3": "red",
    "-4": "red",
    1: "orange",
    2: "green",
    3: "blue",
    4: "purple",
    5: "cadetblue",  
    6: "brown",
    7: "black"  
}


class OrderProductTable extends React.Component {

    state = {
        updatingProduct: null
    }

    onUpdate(oldValue, newValue, row, column, done) {
        if (column.dataField === "status" && row.status != newValue ) {
            done(true);
            let reason = ""; 
            if (Number(newValue) < 0 )
                reason = prompt("Sebäbini giriziñ: ")
            this.props.updateOrderProduct(row.orderProductId, { status: newValue, cancelReason: reason })
        } else {
            done(false);
        }
        return { async: true };
    }

    updateOrderProduct(){
        let body = {}
        body[this.state.updatingProduct.key] = this.updatingValue.value
        if (this.state.updatingProduct.key === "originalPrice"){
            console.log(this.state.updatingProduct)
            body.weight = this.state.updatingProduct.weight
        }
        this.props.updateOrderProduct(this.state.updatingProduct.id, body)
        this.setState({updatingProduct: null})
    }

    openUpdateModal(id, key, value, weight){
        this.setState({updatingProduct: {id, key, value, weight}})
    }

    getOrderData(){
        // if(this.props.userId){
        //     return this.props.orders.data.filter( e => e.userId === this.props.userId )
        // }
        return this.props.orders || []
    }

    render() {
        return (
            <div>
                {/* <ButtonGroup style={{float: "right", marginBottom: 20}}>
                    <Button className="btn" color="danger" onClick={()=>this.deleteSelected()}>Delete</Button>
                    <Link to="/orders/new" className="btn btn-primary" >Create</Link>
                </ButtonGroup> */}
                <BootstrapTable
                    bootstrap4
                    keyField='id'
                    data={this.props.data}
                    columns={this.getColumns()}
                    expandRow={this.expandRow}
                    filter={filterFactory()}
                    noDataIndication={'No results found'}
                    defaultSorted = {[{
                        dataField: 'createdAt',
                        order: 'desc'
                      }]}
                    cellEdit={ cellEditFactory({ 
                        mode: 'click', 
                        blurToSave: true,
                        beforeSaveCell: this.onUpdate.bind(this)
                    }) }
                    rowStyle = {()=>({fontSize: 13})}
                />

                <Modal
                    isOpen={this.state.updatingProduct}
                    toggle={()=>this.setState({updatingProduct: null})}
                    className={this.props.className}
                    fade={false}
                >
                    <ModalHeader toggle={()=>this.setState({updatingProduct: null})}>
                        Update {this.state.updatingProduct && this.state.updatingProduct.key}
                    </ModalHeader>
                    <ModalBody>
                        <Input innerRef={(node) => this.updatingValue = node} defaultValue={this.state.updatingProduct && this.state.updatingProduct.value}/>
                    </ModalBody>
                    <ModalFooter>
                        <Button color="primary" onClick={()=>this.updateOrderProduct()}>
                            Save
                        </Button>
                        <Button color="secondary" onClick={()=>this.setState({updatingProduct: null})}>
                            Cancel
                        </Button>
                    </ModalFooter>
                </Modal>
            </div>
        )
    }

    formateDateTime(time){
        var date = new Date(time);
        
        var year = date.getFullYear();
        var month = (date.getMonth() +1);
        var day = date.getDate();
        
        var hour = date.getHours();
        var minute = date.getMinutes();
        
        return this.formateTime(year, month, day, hour, minute);
      }
      
      formateTime(year, month, day, hour, minute){
        return this.makeDoubleDigit(year) + "/" + 
               this.makeDoubleDigit(month) + "/" + 
               this.makeDoubleDigit(day) + " " + 
               this.makeDoubleDigit(hour) + ":" + 
               this.makeDoubleDigit(minute);
      }
      
      makeDoubleDigit(x){
        return (x < 10) ? "0" + x : x;
      }

    getColumns() {
        return [
            {
                dataField: 'orderProductId',
                text: 'ID',
                sort: true,
                filter: textFilter(),
                hidden: true
            },
            {
                dataField: "productTitle",
                text: "Title",
                sort: true,
                editable: false,
                filter: textFilter(),
                formatter: (cell, row) => <span> 
                    {row.productId} <hr/> {cell || <span style={{color: "red"}}>[no longer exists]</span>}
                    <hr/>
                    {(
                        row.originalLink || row.productOriginalLink
                        ?
                            <a target="_blank" href={row.originalLink || row.productOriginalLink} style={{fontSize: 13, overflow: "hidden", textOverflow: "ellipsis", display: "-webkit-box", WebkitLineClamp: 2, WebkitBoxOrient: "vertical"}}>
                                {(row.originalLink || row.productOriginalLink)}
                            </a>
                        :
                        null
                    )}
                </span>
            },
            {
                dataField: "image",
                text: "Image",
                editable: false,
                formatter: (cell, row) => {
                    if (cell)
                        return <img style={{maxHeight: 150, maxWidth: "80%"}} src={images.getThumbPath(cell)}></img>
                    return cell
                },
                style: {
                    textAlign: "center",
                    maxWidth: 110
                },
                headerStyle: (colum, colIndex) => ({ width: '150px', textAlign: 'center' })
            },
            {
                dataField: "price",
                text: "Filter & Price",
                editable: false,
                formatter: (cell, row) => {
                    return (
                        <div>
                            <span style={{cursor: "pointer"}} onDoubleClick={()=>this.openUpdateModal(row.orderProductId, "color", row.color)}>{row.color}</span>
                            <span>{row.color && row.size ? " / " : null}</span>
                            <span style={{cursor: "pointer"}} onDoubleClick={()=>this.openUpdateModal(row.orderProductId, "size", row.size)}>{row.size}</span>
                            <hr/>
                            {
                                row.originalPrice ?
                                <div style={{color: "#777"}}>
                                    <strong style={{cursor: "pointer"}} onDoubleClick={()=>this.openUpdateModal(row.orderProductId, "quantity", row.quantity)}>{row.quantity}</strong>
                                    {" x "}
                                    <span style={{cursor: "pointer"}} onDoubleClick={()=>this.openUpdateModal(row.orderProductId, "originalPrice", row.originalPrice, row.weight)}>{(row.originalPrice)}</span> TL
                                </div>
                                : null
                            }
                            <div>
                                <strong>{row.quantity}</strong>
                                {" x "} 
                                <span>{formatPrice(cell)}</span>
                                {
                                    row.discount
                                    ?
                                    <span><span style={{color: "red"}}> - {row.quantity} x {formatPrice(cell * row.discount / 100)}({row.discount}%)</span>  = {formatPrice((cell - cell * row.discount / 100)*row.quantity)}</span>
                                    :
                                        row.quantity === 1
                                        ?
                                        null
                                        :
                                        " = " + formatPrice(cell*row.quantity)
                                }
                                {" TMT"}
                            </div>

                        </div>
                    )
                }
            },
            {
                dataField: "status",
                text: "Status",
                sort: true,
                editor: {
                    type: Type.SELECT,
                    getOptions: (setOptions, { row, column }) => {
                        if (statusObj[row.status].edit.includes(Number(this.props.staffStatus)))
                            return(
                                Object.keys(statusObj)
                                    .filter(k => statusObj[k].edit.includes(Number(this.props.staffStatus)))
                                    .map( e => ({ value: e, label: statusObj[e].title, key: e }) )
                            )
                        return []
                    }
                },
                formatter: (cell, row) => (
                    <div>
                        <div style={{
                            border: statusObj[cell].edit.includes(Number(this.props.staffStatus)) ? "1px solid #ccc" : undefined, 
                            borderRadius: 2, padding: "7px 16px 5px 16px"
                        }}>
                            {this.getStatus(cell)}
                        </div>
                        <hr/>
                        {row.cancelReason}
                    </div>
                ),
                // filter: selectFilter({
                //     options: statusObj
                // })
            },
            // {
            //     text: "Action",
            //     dataField: "createChild",
            //     formatter: (cellContent, row) => (
            //         <ButtonGroup>
            //             <Link to={"/products/"+row.id}>
            //                 <Button 
            //                     color="primary" 
            //                     className="mdi mdi-eye" 
            //                     />
            //                 </Link>
            //             <Button color="danger" className="mdi mdi-delete" onClick={() => this.onClickDelete(row.id)}></Button>
            //         </ButtonGroup>
            //     ),
            //     style: { textAlign: "center", verticalAlign: "middle" }
            // }
        ];
    }

    getStatus(cell){
        return(<div style={{color: statusColors[cell]}}>{statusObj[cell].title}</div>)
    }
}

export default (OrderProductTable);
