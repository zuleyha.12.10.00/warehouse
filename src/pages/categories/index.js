import React from "react";
import {
    Card,
    CardBody,
    CardHeader,
    Modal,
    ModalHeader,
    ModalBody,
    ModalFooter,
    Button,
    Input,
    FormText,
    FormGroup,
    Label,
    ButtonGroup,
    Row,
    Col
} from 'reactstrap';
import MultiLangInput from '../../views/components/MultiLangInput'
import Table from './table.category'

import ImageUploadInput from "../../views/components/ImageUploadInput";

import { connect } from 'react-redux';
import { 
    getCategories, 
    createCategory, 
    deleteCategory, 
    updateCategory,
    updateCategoryTitle,
    updateCategoryImage 
} from '../../redux/categories/action';


const mapStateToProps = state => ({
    categories: state.categories
});
const mapDispatchToProps = dispatch => ({
    getCategories: () => getCategories(dispatch),  
    createCategory: (body) => createCategory(dispatch, body),
    deleteCategory: (id) => deleteCategory(dispatch, id),
    updateCategory: (id, body) => updateCategory(dispatch, id, body),
    updateCategoryImage: (body) => updateCategoryImage(dispatch, body),
    updateCategoryTitle: (body) => updateCategoryTitle(dispatch, body),
    
});


class Categories extends React.Component {
    state = {
        data: [],
        create: false
    }
    

    onClickCreate(row){
        if (row){
            this.parentCategoryForCreate = row.id
        }else {
            this.parentCategoryForCreate = null
        }
        this.setState({create: true})
    }

    onCancelCreate(){
        this.setState({create: false})
    }

    createCategory(){
        if (!Object.keys(this.refs.title.value).length){
            alert("Title can't be emty")
            return
        }

        let body = {
            image: this.refs.createImg.value,
            title: JSON.stringify(this.refs.title.value),
            priority: this.priority.value,
            isVisible: this.isVisible.checked ? 1: 0,
            weight: this.weight.value
        }

        console.log(body)
        if(this.parentCategoryForCreate){
            body["parentId"] = this.parentCategoryForCreate
        }

        this.props.createCategory(body)
        this.onCancelCreate()
    }

    componentWillMount(){
        if (!this.props.categories.data.length)
            this.props.getCategories();
    }

    render() {
        return (
            <div>
                <Card>
                    <CardHeader>
                        <h3 style={{ display: "inline-block" }}>Categories</h3>
                        <ButtonGroup style={{float: "right"}}>
                            <Button 
                                color="primary" 
                                onClick={()=>this.onClickCreate()}
                            >
                                Create new
                            </Button>
                        </ButtonGroup>
                    </CardHeader>
                    <CardBody>
                        <Table
                            data = {this.props.categories.data}
                            onClickCreate = {(row)=>this.onClickCreate(row)}
                            onDelete = {this.props.deleteCategory}
                            onUpdate = {this.props.updateCategory}
                            onUpdateImage = {this.props.updateCategoryImage}
                            onUpdateTitle = {this.props.updateCategoryTitle}
                        />
                    </CardBody>
                </Card>
              
                <Modal
                    isOpen={this.state.create}
                    toggle={()=>this.onCancelCreate()}
                    className={this.props.className}
                    fade={false}
                >
                    <ModalHeader toggle={()=>this.onCancelCreate()}>
                        Create child {this.parentCategoryForCreate && "for id="+this.parentCategoryForCreate}
                    </ModalHeader>
                    <ModalBody>
                        <FormGroup>

                            <MultiLangInput ref="title" inputTag={Input} name="title" />
                            <Label for="image" style={{fontWeight: "bold"}}>Category image: </Label>
                            <br/>
                            <ImageUploadInput 
                                ref = "createImg"
                                id = "createImg"
                                style={{width: 100}}
                            />
                            <FormText color="muted">
                                Make sure that size of an image is same with other categories
                            </FormText>
                            <Row>
                                <Col sm={6} style={{marginTop: 10}}>
                                    <Label for="priority">Priority</Label>
                                    <Input innerRef={(node) => this.priority = node} type="number" name="priority" defaultValue="0"/>
                                </Col>
                                <Col sm={6} style={{marginTop: 10}}>
                                    <Label for="priority">Weight</Label>
                                    <Input innerRef={(node) => this.weight = node} type="number" name="weight" defaultValue="0"/>
                                </Col>
                            </Row>
                            <Row>
                                <Col sm={6} style={{marginTop: 40}}>
                                    <FormGroup check>
                                        <Label check>
                                            <Input 
                                                type="checkbox" 
                                                defaultChecked = {true}
                                                innerRef={(node) => this.isVisible = node} 
                                            />
                                            Is visible
                                        </Label>
                                    </FormGroup>
                                </Col>
                            </Row>
                        </FormGroup>
                    </ModalBody>
                    <ModalFooter>
                        <Button color="primary" onClick={()=>this.createCategory()}>
                            Save
                        </Button>
                        <Button color="secondary" onClick={()=>this.onCancelCreate()}>
                            Cancel
                        </Button>
                    </ModalFooter>
                </Modal>
            </div>
        );
    }

}


export default connect(mapStateToProps, mapDispatchToProps)(Categories);
