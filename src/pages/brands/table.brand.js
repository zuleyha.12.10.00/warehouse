import React from "react";
import {
    Button,
    Input
} from 'reactstrap';

import BootstrapTable from 'react-bootstrap-table-next';
import 'react-bootstrap-table-next/dist/react-bootstrap-table2.min.css';
import cellEditFactory, { Type } from 'react-bootstrap-table2-editor';
import filterFactory, { textFilter, selectFilter } from 'react-bootstrap-table2-filter';

import images from "../../constants/images"

import add_image from "../../assets/images/upload_img.jpg"


class Table extends React.Component {

    expandRow = {
        showExpandColumn: true,
        expandByColumnOnly: true,
        renderer: row => (
            this.renderTable(this.columns, this.props.data, row.id)
        ),
        expandHeaderColumnRenderer: ({ isAnyExpands }) => {
            if (isAnyExpands) {
                return <span>-</span>;
            }
            return <span>+</span>;
        },
        expandColumnRenderer: ({ expanded }) => {
            if (expanded) {
                return (
                    <span>-</span>
                );
            }
            return (
                <span>+</span>
            );
        }
    };

    onClickDelete(id){
        if (window.confirm("Are you sure you want to delete this brand")){
            this.props.onDelete(id)
        }
    }

    onUpdate(oldValue, newValue, row, column, done) {
        console.log(oldValue, newValue, row, column, done)
        if (window.confirm('Do you want to accep this change?')) {
            done(true);
            if (column.dataField.split("_")[0]==="title" || column.dataField.split("_")[0]==="filter"){
                this.props.onUpdateTitle({
                    id: row[`id_${column.dataField.split("_")[1]}`], 
                    title: newValue.trim() || null,
                    field: column.dataField.split("_")[0]
                })
            } else {
                let body = {}
                body[column.dataField] = newValue
                this.props.onUpdate({title: row.title, scraperId: row.scraperId, ...body})
            }
        } else {
            done(false);
        }
        return { async: true };
    }

    renderTable(columns, data, parent) {
        let rows = data.filter((e) => e.parentId == parent)
        if (rows && rows.length)
            return <BootstrapTable
                bootstrap4
                keyField='title'
                data={rows || []}
                columns={columns}
                filter={ filterFactory() }
                cellEdit={cellEditFactory({
                    mode: 'dbclick',
                    beforeSaveCell: this.onUpdate.bind(this),
                    // afterSaveCell: (oldValue, newValue, row, column) => { console.log('After Saving Cell!!'); }
                })}
            />
        return (
            <div style={{ textAlign: "center" }}>
                No data have found!
            </div>
        );
    }

    render() {
        return this.renderTable(this.columns, this.props.data, null)
    }

    columns = [
        {
            dataField: 'image',
            text: 'Image',
            formatter: (cell, row) => (
                <div style={{position: "relative"}}>
                    <Button 
                        style={{
                            width: 18,
                            height: 18,
                            background: "red",
                            position: "absolute",
                            top: -10,
                            right: -5,
                            padding: 0,
                            borderRadius: "50%",
                            fontSize: 10,
                            display: cell || "none"
                        }}
                        onClick = {
                            ()=>{
                                this.props.onUpdateImage({title: row.brand})
                            }
                        }
                    >x</Button>
                    <ImageUpdate 
                        defaultValue = {cell} 
                        id = {row.brand+"_img"}
                        onUpdate = {(body)=>this.props.onUpdateImage({...body, title: row.title, scraperId: row.scraperId})}
                        />
                    </div>
            ),
            style: { textAlign: "center" },
            headerStyle: (colum, colIndex) => ({ width: '150px'}),
            editable: false
        },
        {
            dataField: 'brand',
            text: 'Brand',
            sort: true,
            filter: textFilter(),
        },
        {
            dataField: 'convert',
            text: 'Convert to',
            sort: true,
            filter: textFilter(),
        },
        {
            dataField: 'inBlackList',
            text: 'In Black List',
            sort: true,
            headerStyle: (colum, colIndex) => ({ width: '80px'}),
            formatter: (cell) => cell == 1 ? "True" : "False",
            editor: {
                type: Type.CHECKBOX,
                value: '1:0'
            },
            filter: selectFilter({
                options: {
                    0: "False",
                    1: "True"
                }
            })
        },
        {
            dataField: 'totalProducts',
            text: '# of products',
            headerStyle: (colum, colIndex) => ({ width: '80px'}),
            sort: true,
        },
        {
            dataField: 'totalOrders',
            text: '# of orders',
            headerStyle: (colum, colIndex) => ({ width: '80px'}),
            sort: true,
        },
        {
            dataField: 'delete',
            text: 'Delete',
            headerStyle: (colum, colIndex) => ({ width: '70px'}),
            formatter: (cellContent, row) => (
                <Button color="danger" className="mdi mdi-delete" onClick={() => this.onClickDelete(row.id)}>x</Button>
            )
        }
    ];
}

export default Table;

class ImageUpdate extends React.Component {
    
    onChange(element){

        let cnfirm = window.confirm("Do you want to change this image?")
        if (!cnfirm)
            return;

        this.value = element.target.files[0]
        
        this.props.onUpdate({image: this.value})

        var reader = new FileReader();
        reader.onload = (e) => {
            // get loaded data and render thumbnail.
            document.getElementById(this.props.id+"img").src = e.target.result;
        };
    
        // read the image file as a data URL.
        reader.readAsDataURL(element.target.files[0]);
    };
 
    render() {
        return (
            <div onDoubleClick={()=>{document.getElementById(this.props.id+"imgInput").click()}} >
                <img 
                    id = {this.props.id+"img"}
                    src={this.props.defaultValue ? images.getImagePath(this.props.defaultValue): add_image} 
                    style={{...this.props.style, maxWidth: "100%"}}
                    height="80px" 
                />
                
                <Input 
                    type="file" 
                    name="image" 
                    id={this.props.id+"imgInput"} 
                    accept="image/x-png,image/jpeg" 
                    onChange={(e) => this.onChange(e)}
                    style={{display: "none"}}
                />
            </div>                  
        );
    }
}
