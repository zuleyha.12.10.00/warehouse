import React from "react";
import {
    Card,
    CardBody,
    CardHeader,
    Modal,
    ModalHeader,
    ModalBody,
    ModalFooter,
    Button,
    Input,
    FormText,
    FormGroup,
    Label,
    ButtonGroup,
    Row,
    Col
} from 'reactstrap';
import MultiLangInput from '../../views/components/MultiLangInput'
import Table from './table.brand'

import ImageUploadInput from "../../views/components/ImageUploadInput";

import { connect } from 'react-redux';
import { 
    getBrands,
    updateBrandImage,
    update,
    deleteBrand
} from '../../redux/brands/action.js';
import { getScrapers } from "../../redux/scraper/action";


const mapStateToProps = state => ({
    brands: state.brands,
    scrapers: state.scrapers
});
const mapDispatchToProps = dispatch => ({
    getBrands: () => getBrands(dispatch),  
    updateBrandImage: (body) => updateBrandImage(dispatch, body),
    update: (body, callback) => update(dispatch, body, callback),
    delete: (id) => deleteBrand(dispatch, id),

    getScrapers: () => getScrapers(dispatch)
});

class Categories extends React.Component {
    state = {
        data: [],
        create: false
    }
    

    onClickCreate(bool){
        if (!this.props.scrapers.data.length){
            this.props.getScrapers()
        }
        if (!bool && bool !== false){
            bool = true
        }
        this.setState({create: bool})
    }

    createBrand(){
        let body = {
            title: this.brand.value,
            convert: this.convert.value,
            scraperId: this.scraper.value
        }

        this.props.update(body, (err, res)=>{
            if (err){
                return alert(err)
            }
            this.props.getBrands();
        })
        this.onClickCreate(false)
    }

    componentWillMount(){
        if (!this.props.brands.data.length)
            this.props.getBrands();
    }

    render() {
        return (
            <div>
                <Card>
                    <CardHeader>
                        <h3 style={{ display: "inline-block" }}>Brands</h3>
                        <ButtonGroup style={{float: "right"}}>
                            <Button 
                                color="primary" 
                                onClick={()=>this.onClickCreate()}
                            >
                                Create new
                            </Button>
                        </ButtonGroup>
                    </CardHeader>
                    <CardBody>
                        <Table
                            data = {this.props.brands.data}
                            onClickCreate = {()=>this.onClickCreate(true)}
                            onUpdate = {this.props.update}
                            onDelete = {this.props.delete}
                            onUpdateImage = {this.props.updateBrandImage}
                        />
                    </CardBody>
                </Card>
              
                <Modal
                    isOpen={this.state.create}
                    toggle={()=>this.onClickCreate(false)}
                    className={this.props.className}
                    fade={false}
                >
                    <ModalHeader toggle={()=>this.onClickCreate(false)}>
                        Add brand to whitelist
                    </ModalHeader>
                    <ModalBody>
                        <FormGroup>
                            <Label for="scraper">Select scraper</Label>
                            <Input innerRef={(node) => this.scraper = node} name="scraper" type="select">
                                {
                                    this.props.scrapers.data.map(e => (
                                        <option key={e.id} value={e.id}>{e.title}</option>
                                    ))
                                }
                            </Input>
                        </FormGroup>
                        <FormGroup>
                            <Label for="brand">Brand</Label>
                            <Input innerRef={(node) => this.brand = node} name="brand" />
                        </FormGroup>
                        <FormGroup>
                            <Label for="convert">Convert</Label>
                            <Input innerRef={(node) => this.convert = node} name="convert" />
                        </FormGroup>
                    </ModalBody>
                    <ModalFooter>
                        <Button color="primary" onClick={()=>this.createBrand()}>
                            Save
                        </Button>
                        <Button color="secondary" onClick={()=>this.onClickCreate(false)}>
                            Cancel
                        </Button>
                    </ModalFooter>
                </Modal>
            </div>
        );
    }

}


export default connect(mapStateToProps, mapDispatchToProps)(Categories);
