import React from "react";
import {
    Button,
    ButtonGroup,
    Input
} from 'reactstrap';
import { Link } from "react-router-dom";

import BootstrapTable from 'react-bootstrap-table-next';
import 'react-bootstrap-table-next/dist/react-bootstrap-table2.min.css';
import cellEditFactory from 'react-bootstrap-table2-editor';
import filterFactory, { textFilter } from 'react-bootstrap-table2-filter';

class Table extends React.Component {
    onClickDelete(id){
        if (window.confirm("Are you sure you want to delete this category")){
            this.props.onDelete(id)
        }
    }

    onUpdate(oldValue, newValue, row, column, done) {
        if (window.confirm('Do you want to accep this change?')) {
            done(true);
            
            let body = {}
            body[column.dataField] = newValue
            this.props.onUpdate(row.id, body)

        } else {
            done(false);
        }
        return { async: true };
    }

    getAsJson = (id) => {
        let myData = {}
        this.props.getScraperAsJson(id, (err, res)=>{
            if (err){
                return console.error(err)
            }
            myData = res
            console.log(myData)
            this.downloadFile(myData)
        })
    }

    downloadFile = async (obj) => {
        const fileName = "scraperConfig";
        const json = JSON.stringify(obj);
        const blob = new Blob([json],{type:'application/json'});
        const href = await URL.createObjectURL(blob);
        const link = document.createElement('a');
        link.href = href;
        link.download = fileName + ".json";
        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);
    }

    renderTable(columns, data, parent) {
        let rows = data.filter((e) => e.parentId == parent)
        if (rows && rows.length)
            return <BootstrapTable
                bootstrap4
                keyField='id'
                data={rows || []}
                columns={columns}
                filter={ filterFactory() }
                cellEdit={cellEditFactory({
                    mode: 'dbclick',
                    beforeSaveCell: this.onUpdate.bind(this),
                })}
            />
        return (
            <div style={{ textAlign: "center" }}>
                No data have found!
            </div>
        );
    }

    render() {
        return this.renderTable(this.columns, this.props.data, null)
    }

    columns = [
        {
            dataField: 'id',
            text: 'ID',
            sort: true,
            filter: textFilter(),
            hidden: true
        },
        {
            dataField: 'title',
            text: 'Title',
            sort: true,
            filter: textFilter(),
            formatter: (cell, row) => row.id + ". " +row.title
        },
        {
            text: "Action",
            dataField: "createChild",
            editable: false,
            formatter: (cellContent, row) => (
                <ButtonGroup>
                    <Link to={{pathname: "/scraper/category/"+row.id, state: row}}>
                        <Button 
                            color="primary" 
                            className="mdi mdi-link" 
                        >
                            ☇
                        </Button>
                    </Link>
                    <Button color="success" className="mdi mdi-download" onClick={() => this.getAsJson(row.id)}>⬇</Button>
                    <Button color="danger" onClick={() => this.onClickDelete(row.id)}>𝗫</Button>
                </ButtonGroup>
            ),
            style: { textAlign: "center", verticalAlign: "middle" }
        }
    ];
}

export default Table;
