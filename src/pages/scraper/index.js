import React from "react";
import {Calendar, momentLocalizer } from 'react-big-calendar';
import moment from 'moment'
import { Card, CardBody, CardText, Col, Row } from "reactstrap";

const localizer = momentLocalizer(moment)

class Products extends React.Component {
    constructor() {
        super();
        this.state = {
          events: [{
              id: 0,
              title: 'All Day Event very long title',
              allDay: true,
              start: new Date(2023, 3, 0),
              end: new Date(2023, 3, 1),
            },
            {
              id: 1,
              title: 'Long Event',
              start: new Date(2015, 3, 7),
              end: new Date(2015, 3, 10),
            },
    
            {
              id: 2,
              title: 'DTS STARTS',
              start: new Date(2016, 2, 13, 0, 0, 0),
              end: new Date(2016, 2, 20, 0, 0, 0),
            },
    
            {
              id: 3,
              title: 'DTS ENDS',
              start: new Date(2023, 10, 6, 0, 0, 0),
              end: new Date(2023, 10, 13, 0, 0, 0),
            },
    
            {
              id: 4,
              title: 'Some Event',
              start: new Date(2015, 3, 9, 0, 0, 0),
              end: new Date(2015, 3, 10, 0, 0, 0),
            }
          ]
        };  
      }
    
	render() {
		return (
        <div>
          <Card>
            <CardBody> 
              <Row>
                <Col xl="2" md="2">
                    <div className="product-all">Hemmesi</div>
                    <div className="product-send">Cykan harytlar</div>
                    <div className="product-get">Gelen harytlar</div>
                </Col>
                <Col xl="10" md="10">
                    <Calendar
                        localizer={localizer}
                        events={this.state.events}
                        startAccessor="start"
                        endAccessor="end"
                        style={{ height: 500 }}
                    />
                </Col>
              </Row> 
            </CardBody>
           </Card>
           </div>
		);
	}
}

export default Products;