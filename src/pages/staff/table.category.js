import React from "react";
import {
    Button,
    ButtonGroup,
    Input
} from 'reactstrap';

import BootstrapTable from 'react-bootstrap-table-next';
import 'react-bootstrap-table-next/dist/react-bootstrap-table2.min.css';
import cellEditFactory, { Type } from 'react-bootstrap-table2-editor';
import filterFactory, { textFilter } from 'react-bootstrap-table2-filter';

import languages from "../../languages";


class Table extends React.Component {

    onClickDelete(id){
        if (window.confirm("Are you sure you want to delete this staff")){
            this.props.onDelete(id)
        }
    }

    onUpdate(oldValue, newValue, row, column, done) {
        if (window.confirm('Do you want to accep this change?')) {
            done(true);
            let body = {}
            body[column.dataField] = newValue
            this.props.onUpdate(row.id, body)
        } else {
            done(false);
        }
        return { async: true };
    }

    renderTable(columns, data, parent) {
        let rows = data.filter((e) => e.parentId == parent)
        if (rows && rows.length)
            return <BootstrapTable
                bootstrap4
                keyField='id'
                data={rows || []}
                columns={columns}
                expandRow={this.expandRow}
                filter={ filterFactory() }
                cellEdit={cellEditFactory({
                    mode: 'dbclick',
                    beforeSaveCell: this.onUpdate.bind(this),
                    // afterSaveCell: (oldValue, newValue, row, column) => { console.log('After Saving Cell!!'); }
                })}
            />
        return (
            <div style={{ textAlign: "center" }}>
                No data have found!
            </div>
        );
    }

    render() {
        return (
            <BootstrapTable
                bootstrap4
                keyField='id'
                data={this.props.data || []}
                columns={this.columns}
                filter={ filterFactory() }
                noDataIndication={"No data have found!"}
                cellEdit={cellEditFactory({
                    mode: 'dbclick',
                    beforeSaveCell: this.onUpdate.bind(this),
                    // afterSaveCell: (oldValue, newValue, row, column) => { console.log('After Saving Cell!!'); }
                })}
            />
        )
    }

    columns = [
        {
            dataField: 'id',
            text: 'ID',
            sort: true,
            filter: textFilter()
        },
        {
            dataField: 'name',
            text: 'Name',
            sort: true,
            filter: textFilter()
        },
        {
            dataField: 'username',
            text: 'Username',
            sort: true,
            filter: textFilter()
        },
        {
            dataField: 'password',
            text: 'Password',
            sort: true,
            filter: textFilter()
        },
        {
            dataField: "status",
            text: "Status",
            sort: true,
            editor: {
                type: Type.SELECT,
                getOptions: (setOptions, { row, column }) => {
                    return(
                    Object.keys(this.props.staffOptions)
                        .map( e => ({ value: e, label: this.props.staffOptions[e], key: e }) )
                )}
            },
            formatter: (cell, row) => (
                <div>
                    <div style={{border: "1px solid #ccc", borderRadius: 2, padding: "7px 16px 5px 16px"}}>
                        {this.props.staffOptions[cell]}
                    </div>
                </div>
            ),
            // filter: selectFilter({
            //     options: statusObj
            // })
        },
        {
            text: "Action",
            dataField: "createChild",
            editable: false,
            formatter: (cellContent, row) => (
                <ButtonGroup>
                    <Button color="danger" className="mdi mdi-delete" onClick={() => this.onClickDelete(row.id)}>x</Button>
                </ButtonGroup>
            ),
            style: { textAlign: "center", verticalAlign: "middle" }
        }
    ];
}

export default Table;