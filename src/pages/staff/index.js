import React from "react";
import {
    Card,
    CardBody,
    CardHeader,
    Modal,
    ModalHeader,
    ModalBody,
    ModalFooter,
    Button,
    Input,
    FormGroup,
    Label,
    ButtonGroup
} from 'reactstrap';
import MultiLangInput from '../../views/components/MultiLangInput'
import Table from './table.category'

import { connect } from 'react-redux';
import { 
    getStaff, 
    createStaff, 
    deleteStaff, 
    updateStaff
} from '../../redux/staff/action';


const mapStateToProps = state => ({
    staff: state.staff
});
const mapDispatchToProps = dispatch => ({
    getStaff: () => getStaff(dispatch),  
    createStaff: (body) => createStaff(dispatch, body),
    deleteStaff: (id) => deleteStaff(dispatch, id),
    updateStaff: (id, body) => updateStaff(dispatch, id, body)
});

const staffOptions = {
    1: "Operator",
    2: "Türk",
    3: "Warehouse",
    4: "Curier",
    5: "Kassir"
}

class Staff extends React.Component {
    state = {
        data: [],
        create: false
    }
    

    onClickCreate(row){
        this.setState({create: true})
    }

    onCancelCreate(){
        this.setState({create: false})
    }

    createStaff(){
        if (!this.password.value.length || !this.username.value.length){
            alert("Username and password can't be emty")
            return
        }

        let body = {
            name: this.name.value,
            username: this.username.value,
            password: this.password.value,
            phonenumber: this.phonenumber.value,
            status: this.status.value
        }

        this.props.createStaff(body)
        this.onCancelCreate()
    }

    componentWillMount(){
        if (!this.props.staff.data.length)
            this.props.getStaff();
    }

    render() {
        return (
            <div>
                <Card>
                    <CardHeader>
                        <h3 style={{ display: "inline-block" }}>Staff</h3>
                        <ButtonGroup style={{float: "right"}}>
                            <Button 
                                color="primary" 
                                onClick={()=>this.onClickCreate()}
                            >
                                Create new
                            </Button>
                        </ButtonGroup>
                    </CardHeader>
                    <CardBody>
                        <Table
                            data = {this.props.staff.data}
                            onClickCreate = {(row)=>this.onClickCreate(row)}
                            onDelete = {this.props.deleteStaff}
                            onUpdate = {this.props.updateStaff}
                            staffOptions = {staffOptions}
                        />
                    </CardBody>
                </Card>
              
                <Modal
                    isOpen={this.state.create}
                    toggle={()=>this.onCancelCreate()}
                    className={this.props.className}
                    fade={false}
                >
                    <ModalHeader toggle={()=>this.onCancelCreate()}>
                        Create staff 
                    </ModalHeader>
                    <ModalBody>
                        <FormGroup>
                            <Label for="name">Name*</Label>
                            <Input innerRef={(node) => this.name = node} name="name"/>
                        </FormGroup>
                        <FormGroup>
                            <Label for="user">Username*</Label>
                            <Input innerRef={(node) => this.username = node} name="user"/>
                        </FormGroup>
                        <FormGroup>
                            <Label for="password">Password*</Label>
                            <Input innerRef={(node) => this.password = node} name="password"/>
                        </FormGroup>
                        <FormGroup>
                            <Label for="phone">Phone number (optional)</Label>
                            <Input innerRef={(node) => this.phonenumber = node} name="phone"/>
                        </FormGroup>
                        <FormGroup>
                            <Label for="Price">Status*</Label>
                            <Input type="select" innerRef={(node) => this.status = node}>
                                {
                                    Object.keys(staffOptions).map( key => (
                                        <option key={key} value={key}>{staffOptions[key]}</option>
                                    ))
                                }
                            </Input>
                        </FormGroup>
                    </ModalBody>
                    <ModalFooter>
                        <Button color="primary" onClick={()=>this.createStaff()}>
                            Save
                        </Button>
                        <Button color="secondary" onClick={()=>this.onCancelCreate()}>
                            Cancel
                        </Button>
                    </ModalFooter>
                </Modal>
            </div>
        );
    }

}


export default connect(mapStateToProps, mapDispatchToProps)(Staff);
