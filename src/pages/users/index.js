import React from "react";
import {
    Button,
	Card,
	CardBody,
    CardHeader,
    Col,
    Form,
    FormGroup,
    Input,
    Label,
    Modal,
    ModalBody,
    ModalFooter,
    ModalHeader,
    Row,
    Table,
} from 'reactstrap';


class Products extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
          modal: false
        };
    
        this.toggle = this.toggle.bind(this);
      }

      toggle() {
        this.setState({
          modal: !this.state.modal
        });
      }
	render() {
		return (
            <div>
                <Card>
                    <CardHeader style={{backgroundColor: '#fff'}}>
                    <div>
                        <h3 style={{display: "inline-block"}}>Jogapkar isgarler</h3>
                        <button className="btn btn-secondary add-user" onClick={this.toggle}><i className="fas fa-plus"></i></button>
                            <Modal isOpen={this.state.modal} toggle={this.toggle} className={this.props.className} size='lg'>
                            <ModalHeader style={{backgroundColor: '#00ACD1', color:'#fff'}} toggle={this.toggle}>Добавить</ModalHeader>
                            <ModalBody>
                                <Form className="mt-3" id="">
                                    <Row>
                                        <Col>
                                            <FormGroup className="mb-3">
                                                <Label for="username">
                                                    Ady
                                                </Label>

                                                <Input id="username" name="username" type="text" className='form-control' />
                                            </FormGroup>
                                        </Col>
                                        <Col>
                                            <FormGroup className="mb-3">
                                                <Label for="password">
                                                    Familiyasy
                                                </Label>

                                                <Input id="fullname" name="fullname" type="text" className='form-control' />
                                            </FormGroup>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col>
                                            <FormGroup className="mb-3">
                                                <Label for="job">
                                                    Wezipesi
                                                </Label>

                                                <Input id="job" name="job" type="text" className='form-control' />
                                            </FormGroup>
                                        </Col>
                                        <Col>
                                            <FormGroup className="mb-3">
                                                <Label for="email">
                                                    Email
                                                </Label>

                                                <Input id="email" name="email" type="email" className='form-control' />
                                            </FormGroup>
                                        </Col>
                                    </Row>
                                    
                                </Form>
                            </ModalBody>
                            <ModalFooter>
                                <Button 
                                    color="primary" 
                                    onClick={this.toggle}
                                    style={{color: '#fff', backgroundColor: '#00ACD1', 
                                            padding: '10px 20px', borderRadius: '4px',
                                            textTransform: 'uppercase', fontSize: '12px'}}
                                >
                                    Добавить
                                </Button>
                            </ModalFooter>
                            </Modal>
                        </div>
                    </CardHeader>
                    <CardBody>
                        <Table>
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Wagty</th>
                                    <th>Wezipesi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Jumayew Juma</td>
                                    <td>jumaarcalyk@gmail.com</td>
                                    <td>09.15-09.45</td>
                                    <td>Suw Gaplayjy</td>
                                </tr>
                                <tr>
                                    <td>Jumayew Juma</td>
                                    <td>jumaarcalyk@gmail.com</td>
                                    <td>09.15-09.45</td>
                                    <td>Suw Gaplayjy</td>
                                </tr>
                                <tr>
                                    <td>Jumayew Juma</td>
                                    <td>jumaarcalyk@gmail.com</td>
                                    <td>09.15-09.45</td>
                                    <td>Suw Gaplayjy</td>
                                </tr>
                            </tbody>
                        </Table>
                    </CardBody>
                </Card>
            </div>
		);
	}
}

export default Products;