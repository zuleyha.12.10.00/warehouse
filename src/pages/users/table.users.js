import React from "react";

import BootstrapTable from 'react-bootstrap-table-next';
import 'react-bootstrap-table-next/dist/react-bootstrap-table2.min.css';
import filterFactory, { textFilter, selectFilter } from 'react-bootstrap-table2-filter';
import cellEditFactory, { Type } from 'react-bootstrap-table2-editor';

import OrderTable from '../orders/table.orders'

import { connect } from 'react-redux';
import { 
    getUsers, 
    updateUser
} from '../../redux/users/action';


const mapStateToProps = state => ({
    users: state.users
});
const mapDispatchToProps = dispatch => ({
    getUsers: () => getUsers(dispatch),  
    updateUser: (id, body) => updateUser(dispatch, id, body),  
});

const gender = {
    2: "Female",
    1: "Male"
}

export const status = {
    0: "Adaty",
    1: "Bronze -3%",
    2: "Silver -5%",
    3: "Gold -7%",
    4: "SG -10%",
    5: "Ban",
}

class Table extends React.Component {
    componentWillMount(){
        if(!this.props.users.data.length){
            this.props.getUsers()
        }
    }

    onUpdate(oldValue, newValue, row, column, done) {
        if (window.confirm('Do you want to accept this change?')) {
            done(true);
            
            let body = {}
            body[column.dataField] = newValue
            this.props.updateUser(row.id, body)
        } else {
            done(false);
        }
        return { async: true };
    }

    render() {
        return(
            <div>
                {/* <ButtonGroup style={{float: "right", marginBottom: 20}}>
                    <Button className="btn" color="danger" onClick={()=>this.deleteSelected()}>Delete</Button>
                    <Link to="/products/new" className="btn btn-primary" >Create</Link>
                </ButtonGroup> */}

                <section>Total: {(this.props.users.data || []).length}</section>
                <br/>

                <BootstrapTable
                    bootstrap4
                    keyField='id'
                    data={(this.props.users.data || [])}
                    columns={this.getColumns()}
                    // expandRow={this.expandRow}
                    filter={ filterFactory() }
                    noDataIndication={ 'No results found' }
                    cellEdit={cellEditFactory({
                        mode: 'dbclick',
                        beforeSaveCell: this.onUpdate.bind(this),
                        // afterSaveCell: (oldValue, newValue, row, column) => { console.log('After Saving Cell!!'); }
                    })}
                />
            </div>
        )
    }


    formateDateTime(time){
        var date = new Date(time);
        
        var year = date.getFullYear();
        var month = (date.getMonth() +1);
        var day = date.getDate();
        
        var hour = date.getHours();
        var minute = date.getMinutes();
        
        return this.formateTime(year, month, day, hour, minute);
    }

    formateTime(year, month, day, hour, minute){
        return this.makeDoubleDigit(year) + "/" + 
               this.makeDoubleDigit(month) + "/" + 
               this.makeDoubleDigit(day) + " " + 
               this.makeDoubleDigit(hour) + ":" + 
               this.makeDoubleDigit(minute);
    }

    makeDoubleDigit(x){
        return (x < 10) ? "0" + x : x;
    }
    

    expandRow = {
        showExpandColumn: true,
        expandByColumnOnly: true,
        renderer: row => (
            <OrderTable userId={row.id}/>
        ),
        expandHeaderColumnRenderer: ({ isAnyExpands }) => {
            if (isAnyExpands) {
                return <span>-</span>;
            }
            return <span>+</span>;
        },
        expandColumnRenderer: ({ expanded }) => {
            if (expanded) {
                return (
                    <span>-</span>
                );
            }
            return (
                <span>+</span>
            );
        }
    };

    getColumns(){
        return [
            {
                dataField: 'id',
                text: 'ID',
                sort: true,
                filter: textFilter(),
                editable: false,
                hidden: false,
                headerStyle: (colum, colIndex) => ({ width: window.innerWidth < 500 ? "40px" : '70px'})
            },
            {
                dataField: "name",
                text: "Name",
                sort: true,
                editable: false,
                filter: textFilter(),
                formatter: (cell, row) => (
                    <div> 
                        {window.innerWidth < 500 ? (status[row.status] || status[0]) : null}
                        {row.name}
                        {row.orderCount ? <span style={{marginLeft: 5, color: "red"}}>({row.orderCount})</span> : null}
                        <div style={{display: window.innerWidth > 500 ? "none" : undefined }}>
                            {row.phoneNumber}
                        </div>
                    </div>
                )
            },
            {
                dataField: "phoneNumber",
                text: "Phone Number",
                hidden: window.innerWidth < 500,
                sort: false,
                editable: false,
                filter: textFilter(),
                headerStyle: (colum, colIndex) => ({ width: '150px'})
            },
            {
                dataField: "createdAt",
                text: "Registered At",
                sort: true,
                filter: textFilter(),
                editable: false,
                formatter: (cell, row) => (
                    <div>
                        {this.formateDateTime(cell)}
                        <div style={{display: window.innerWidth > 500 ? "none" : undefined }}>
                            <hr/>
                            {gender[row.gender] ? gender[row.gender]: row.gender}
                        </div>
                    </div>
                ),
                headerStyle: (colum, colIndex) => ({ width: '150px'})
            },
            {
                dataField: "gender",
                text: "Gender",
                hidden: window.innerWidth < 500,
                sort: true,
                filter: selectFilter({
                    options: gender
                }),
                editor: {
                    type: Type.SELECT,
                    getOptions: (setOptions, { row, column }) => (
                        Object.keys(gender).map( e => ({ value: e, label: gender[e] }) )
                    )
                },
                formatter: (cell) => gender[cell] ? gender[cell]: cell,
                headerStyle: (colum, colIndex) => ({ width: '150px'})

            },
            {
                dataField: "status",
                text: "Status",
                hidden: window.innerWidth < 500,
                sort: true,
                filter: selectFilter({
                    options: status
                }),
                editor: {
                    type: Type.SELECT,
                    getOptions: (setOptions, { row, column }) => (
                        Object.keys(status).map( e => ({ value: e, label: status[e] }) )
                    )
                },
                formatter: (cell) => status[cell] ? status[cell]: status[0],
                headerStyle: (colum, colIndex) => ({ width: '150px'})
            }
            // {
            //     text: "Action",
            //     dataField: "createChild",
            //     formatter: (cellContent, row) => (
            //         <ButtonGroup>
            //             <Link to={"/products/"+row.id}>
            //                 <Button 
            //                     color="primary" 
            //                     className="mdi mdi-eye" 
            //                     />
            //                 </Link>
            //             <Button color="danger" className="mdi mdi-delete" onClick={() => this.onClickDelete(row.id)}></Button>
            //         </ButtonGroup>
            //     ),
            //     style: { textAlign: "center", verticalAlign: "middle" }
            // }
        ];
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Table);
