import React from "react";


import BootstrapTable from 'react-bootstrap-table-next';
import 'react-bootstrap-table-next/dist/react-bootstrap-table2.min.css';
import filterFactory, { textFilter, selectFilter } from 'react-bootstrap-table2-filter';
import cellEditFactory, { Type } from 'react-bootstrap-table2-editor';

import { connect } from 'react-redux';
import { getOrders, updateOrderStatus } from '../../redux/orders/action';
import { getFilterValues } from "../../redux/filterValues/action";

import languages from '../../languages';

const statusObj = {
    0: "Canceled",
    1: "Pending",
    2: "Shipped",
    3: "Delivered"  
}

const mapStateToProps = state => ({
    orders: state.orders,
    filterValues: state.filterValues
});
const mapDispatchToProps = dispatch => ({
    getOrders: () => getOrders(dispatch),
    updateOrderStatus: (id, body) => updateOrderStatus(id, body, dispatch),
    getFilterValues: () => getFilterValues(dispatch)
});


class Table extends React.Component {
    componentWillMount() {
        if (!this.props.filterValues.array.length) {
            this.props.getFilterValues()
        }
        if (!this.props.orders.data.length) {
            this.props.getOrders()
        }
    }

    onUpdate(oldValue, newValue, row, column, done) {
        if (column.dataField === "status") {
            done(true);
            this.props.updateOrderStatus(row.id, { status: newValue })
        } else {
            done(false);
        }
        return { async: true };
    }

    getFilterValuesTitle(id) {
        let filterValues = this.props.filterValues.array
        for (let i = 0; i < filterValues.length; i++) {
            if (String(filterValues[i].id) === String(id)) {
                return filterValues[i][`title_${languages[0].code}`]
            }
        }
    }

    getUserOrder(){
        if (!this.props.orders.data) 
            return []
        console.log(this.props.orders.data, this.props.userId)
        return this.props.orders.data.filter( e => e.userId === this.props.userId )
    }

    render() {
        return (
            <div>
                {/* <ButtonGroup style={{float: "right", marginBottom: 20}}>
                    <Button className="btn" color="danger" onClick={()=>this.deleteSelected()}>Delete</Button>
                    <Link to="/orders/new" className="btn btn-primary" >Create</Link>
                </ButtonGroup> */}
                <BootstrapTable
                    bootstrap4
                    keyField='id'
                    data={this.getUserOrder()}
                    columns={this.getColumns()}
                    expandRow={this.expandRow}
                    filter={filterFactory()}
                    noDataIndication={'No results found'}
                    defaultSorted = {[{
                        dataField: 'createdAt',
                        order: 'desc'
                      }]}
                    cellEdit={ cellEditFactory({ 
                        mode: 'dbclick', 
                        blurToSave: true,
                        beforeSaveCell: this.onUpdate.bind(this)
                    })}
                />
            </div>
        )
    }

    formateDateTime(time){
        var date = new Date(time);
        
        var year = date.getFullYear();
        var month = (date.getMonth() +1);
        var day = date.getDate();
        
        var hour = date.getHours();
        var minute = date.getMinutes();
        
        return this.formateTime(year, month, day, hour, minute);
      }
      
      formateTime(year, month, day, hour, minute){
        return this.makeDoubleDigit(year) + "/" + 
               this.makeDoubleDigit(month) + "/" + 
               this.makeDoubleDigit(day) + " " + 
               this.makeDoubleDigit(hour) + ":" + 
               this.makeDoubleDigit(minute);
      }
      
      makeDoubleDigit(x){
        return (x < 10) ? "0" + x : x;
      }

    getColumns() {
        return [
            {
                dataField: 'id',
                text: 'ID',
                sort: true,
                filter: textFilter(),
                hidden: true
            },
            {
                dataField: 'createdAt',
                text: 'Date of order',
                sort: true,
                filter: textFilter(),
                formatter: (cell) => this.formateDateTime(cell)
            },
            {
                dataField: "title",
                text: "Title",
                sort: true,
                filter: textFilter()
            },
            {
                dataField: "image",
                text: "Image",
                formatter: (cell) => {
                    if (cell)
                        return <img width="50%" src={cell}></img>
                    return cell
                },
                style: {
                    textAlign: "center"
                }
            },
            {
                dataField: "filterIds",
                text: "Filters",
                formatter: (cell) => (
                    cell.map(e => this.getFilterValuesTitle(e)).join(" / ")
                )
            },
            {
                dataField: "price",
                text: "Price",
                sort: true,
                filter: textFilter(),
                formatter: (cell, row) => (<span><strong>{row.quantity}</strong> x {cell}</span>)
            },
            {
                dataField: "phoneNumber",
                text: "Buyer",
                filter: textFilter(),
                formatter: (cell, row) => (row.phoneNumber + " " + row.firstName)
            },
            {
                dataField: "status",
                text: "Status",
                sort: true,
                editor: {
                    type: Type.SELECT,
                    getOptions: (setOptions, { row, column }) => (
                        Object.keys(statusObj).map( e => ({ value: e, label: statusObj[e] }) )
                    )
                },
                formatter: (cell) => {
                    switch (Number(cell)) {
                        case 0:
                            return <div className='text-danger'>Canceled</div>
                        case 1:
                            return <div className='text-warning'>Pending</div>
                        case 2:
                            return <div className='text-info'>Shipped</div>
                        case 3:
                            return <div className='text-success'>Delivered</div>
                        default:
                            return cell
                    }
                },
                filter: selectFilter({
                    options: statusObj
                })
            },
            // {
            //     text: "Action",
            //     dataField: "createChild",
            //     formatter: (cellContent, row) => (
            //         <ButtonGroup>
            //             <Link to={"/products/"+row.id}>
            //                 <Button 
            //                     color="primary" 
            //                     className="mdi mdi-eye" 
            //                     />
            //                 </Link>
            //             <Button color="danger" className="mdi mdi-delete" onClick={() => this.onClickDelete(row.id)}></Button>
            //         </ButtonGroup>
            //     ),
            //     style: { textAlign: "center", verticalAlign: "middle" }
            // }
        ];
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Table);
