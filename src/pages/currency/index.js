import React from "react";
import {
    Card,
    CardBody,
    CardHeader,
    Button,
    Input,
    Row,
    Col } from 'reactstrap';
import { connect } from 'react-redux';
import { 
    getCurrency, 
    updateCurrency,
    getShowCurrency, 
    updateShowCurrency,
} from '../../redux/currency/action';

const mapStateToProps = state => ({
    scrapers: state.scrapers
});
const mapDispatchToProps = dispatch => ({
    updateCurrency: (body, callback) => updateCurrency(dispatch, body, callback),
    updateShowCurrency: (body, callback) => updateShowCurrency(dispatch, body, callback),
    getCurrency: (callback) => getCurrency(dispatch, callback),
    getShowCurrency: (callback) => getShowCurrency(dispatch, callback)
});


class Currency extends React.Component {
    state = {
        currency: {},
        show: {}
    }
    
    onUpdateValue(key, val){
        this.state.currency[key] = val
        this.setState(this.state)
    }

    onUpdateShowValue(key, val){
        this.state.show[key] = val
        this.setState(this.state)
    }

    updateCurrency(){
        let body = {
            TL: this.TL.value,
            KG: this.KG.value
        }

        this.props.createScraper(body)
        this.onCancelCreate()
    }

    onSubmit(){
        this.props.updateCurrency(this.state.currency, (err, res)=>{
            if (err)
                return console.error(err)
        })
    }

    onSubmitShow(){
        this.props.updateShowCurrency(this.state.show, (err, res)=>{
            if (err)
                return console.error(err)
        })
    }

    componentWillMount(){
        this.props.getCurrency((err, res)=>{
            if (err)
                return console.error(err)
            console.log(res)
            this.setState({currency: res})
        });

        this.props.getShowCurrency((err, res)=>{
            if (err)
                return console.error(err)
            console.log(res)
            this.setState({show: res})
        });
    }

    render() {
        return (
            <Row>
                <Col sm={6}>
                    <Card>
                        <CardHeader>
                            <h3 style={{ display: "inline-block" }}>Currency for pricing</h3>
                        </CardHeader>
                        <CardBody>
                            1 TL = <Input type="number" value={this.state.currency.TL} onChange={(e)=>this.onUpdateValue("TL", e.target.value)} placeholder="manat" style={{display: "inline-block", width: 120}}/> TMT
                            <br/>
                            <br/>
                            1 KG = <Input type="number" value={this.state.currency.KG} onChange={(e)=>this.onUpdateValue("KG", e.target.value)} placeholder="manat" style={{display: "inline-block", width: 120}}/> TMT
                            <br/>
                            <br/>
                            1 USD = <Input type="number" value={this.state.currency.USD} onChange={(e)=>this.onUpdateValue("USD", e.target.value)} placeholder="manat" style={{display: "inline-block", width: 120}}/> TMT
                            <br/>
                            <br/>
                            <Button onClick={()=>this.onSubmit()}>SUBMIT</Button>
                        </CardBody>
                    </Card>
                </Col>
                <Col sm={6}>
                    <Card>
                        <CardHeader>
                            <h3 style={{ display: "inline-block" }}>Currency for app</h3>
                        </CardHeader>
                        <CardBody>
                            1 TL = <Input type="number" value={this.state.show.beforeTL} onChange={(e)=>this.onUpdateShowValue("beforeTL", e.target.value)} placeholder="manat" style={{display: "inline-block", width: 120}}/> TMT
                            [100% öňünden töleg]
                            <br/>
                            <br/>
                            1 TL = <Input type="number" value={this.state.show.TL} onChange={(e)=>this.onUpdateShowValue("TL", e.target.value)} placeholder="manat" style={{display: "inline-block", width: 120}}/> TMT
                            [0% öňünden töleg]
                            <br/>
                            <br/>
                            1 KG = <Input type="number" value={this.state.show.KG} onChange={(e)=>this.onUpdateShowValue("KG", e.target.value)} placeholder="manat" style={{display: "inline-block", width: 120}}/> TMT
                            <br/>
                            <br/>
                            Eltip berme <Input type="text" value={this.state.show.delivery} onChange={(e)=>this.onUpdateShowValue("delivery", e.target.value)} placeholder="gün" style={{display: "inline-block", width: 120}}/> GÜN
                            <br/>
                            <br/>
                            <Button onClick={()=>this.onSubmitShow()}>SUBMIT</Button>
                        </CardBody>
                    </Card>
                </Col>
            </Row>
        );
    }

}

export default connect(mapStateToProps, mapDispatchToProps)(Currency);
