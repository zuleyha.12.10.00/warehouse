import React from "react";
import {
    Row,
    Col,
    Button,
    Card,
    CardBody,
    CardHeader,
    Input,
} from 'reactstrap';
import Carusel from './carousel'

import { connect } from 'react-redux';
import { 
    getBanners,
    updateImage,
    updateImageField,
    updateLink,
    addBanner,
    removeBanner
} from '../../redux/home/action';

import ImageUploadInput from "../../views/components/ImageUploadInput";
import images from "../../constants/images";
import languages from "../../languages";

const mapStateToProps = state => ({
    banners: state.home.data
});
const mapDispatchToProps = dispatch => ({
    getBanners: () => getBanners(dispatch),
    updateLink: (id, body) => updateLink(dispatch, id, body),
    updateImage: (id, body) => updateImage(dispatch, id, body),
    updateImageField: (id, body) => updateImageField(dispatch, id, body),
    addBanner: (tag) => addBanner(dispatch, tag),
    removeBanner: (id) => removeBanner(dispatch, id)
});


class Home extends React.Component {
    state = {
        imagePerRow: 1,
        links: {}
    }

    componentWillMount(){
        if (!this.props.banners.length){
            this.props.getBanners()
        }
    }

    onChangeImage(newImg, id, lang, field){
        if (lang === languages[0].code){
            this.props.updateImage(id, {image: newImg})
            return
        }
        this.props.updateImageField(id, {image: newImg, field})
    }

    onChangeImageField(newImg, id, field){
        console.log(id, {image: newImg, field})
        this.props.updateImageField(id, {image: newImg, field})
    }

    onChangeLink(id){
        this.props.updateLink(id, {link: this.state.links[id]})
    }

    addSliderImage(tag){
        this.props.addBanner(tag)
    }

    onRemove(id){
        this.props.removeBanner(id)
    }
    
    render() {
        let banners = this.props.banners;

        banners.forEach(e => {
            if (e && e.link){
                this.state.links[e.id] = e.link 
                delete e.link
            }

        });

        return (
            <div>
                {
                    languages.map( (lang, ind) => (
                        <Card key={lang.code}>
                            <CardHeader>
                                <h3 style={{ display: "inline-block" }}>Home Banners [{lang.code}] {ind === 0 ? "(MAIN)" : "" }</h3>
                            </CardHeader>
                            <CardBody>
                            <Row>
                                <Col lg={6}>
                                    <h3>Desktop Screen</h3>
                                    <Button onClick={()=>this.addSliderImage("dynamic")}>Add Image</Button>
                                    <Carusel
                                        lang={lang.code} 
                                        onChange={(e, id)=>this.onChangeImage(e, id, lang.code, `image_${lang.code}`)} 
                                        data={banners.filter(e => e.tag !== "static").map(e=>({id: e.id, image: e[`image_${lang.code}`], link: this.state.links[e.id]||""}))}
                                        onClose={(id)=>this.onRemove(id)}
                                        
                                        onChangeLink={(id, val)=>{
                                            this.state.links[id] = val
                                            this.setState(this.state)
                                        }}
                                        onSubmitLink={(id)=>this.onChangeLink(id)}
                                        />
                                    <Button onClick={()=>this.addSliderImage("static")}>Add Static Image</Button>
                                    {
                                        banners.filter(e => e.tag === "static").map( (img, ind) => (
                                            <div key={ind} style={{position: "relative"}}>
                                                <ImageUploadInput 
                                                    id={lang.code + ("sm"+img.id)}
                                                    banner
                                                    style={{width: "100%"}}
                                                    defaultValue={images.getImagePath(img[`image_${lang.code}`])} 
                                                    onChange={(e)=>this.onChangeImage(e, img.id, lang.code, `image_${lang.code}`)}
                                                    onClose={()=>this.onRemove(img.id)}
                                                    />
                                                <Input 
                                                    style={style.link} 
                                                    type="url" 
                                                    onChange={(e)=>{
                                                        this.state.links[img.id] = e.target.value
                                                        this.setState(this.state)
                                                    }}
                                                    value={this.state.links[img.id]||""}
                                                    />
                                                <Button onClick={()=>this.onChangeLink(img.id)}>Save</Button>
                                            </div>
                                        ))
                                    }
                                </Col>
                            </Row>
                            </CardBody>
                        </Card>
                    ))
                }
            </div>
        );
    }

}

const style = {
    col: {
        padding: "10px 10px",
    },
    link: {
        display: "inline-block",
        width: "calc(100% - 65px)",
        minWidth: "20px"
    },
    small_screen: {
        padding: "32px 80px",
        position: "relative",
    },
    mobile_screen: {
        overflowY: "scroll",
        userSelect: "none",
        padding: "10px 0",
        border: "5px solid #333",
        height: 500,
        borderRadius: "25px",
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Home);
