import React from 'react';
import {
  Carousel,
  CarouselItem,
  CarouselControl,
  CarouselIndicators,
  Input,
  Button
} from 'reactstrap';
import ImageUploadInput from "../../views/components/ImageUploadInput";
import images from '../../constants/images';

class CarouselComponent extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      'activeIndex': 0,
    };
    this.next = this.next.bind(this);
    this.previous = this.previous.bind(this);
    this.goToIndex = this.goToIndex.bind(this);
    this.onExiting = this.onExiting.bind(this);
    this.onExited = this.onExited.bind(this);
  }

  onExiting() {
    this.animating = true;
  }

  onExited() {
    this.animating = false;
  }

  next() {
    if (this.animating) {
      return;
    }
    const nextIndex = this.state.activeIndex === this.props.data.length - 1 ? 0 : this.state.activeIndex + 1;
    this.setState({ 'activeIndex': nextIndex });
  }

  previous() {
    if (this.animating) {
      return;
    }
    const nextIndex = this.state.activeIndex === 0 ? this.props.data.length - 1 : this.state.activeIndex - 1;
    this.setState({ 'activeIndex': nextIndex });
  }

  goToIndex(newIndex) {
    if (this.animating) {
      return;
    }
    this.setState({ 'activeIndex': newIndex });
  }

  render() {

    if (this.state.activeIndex >= this.props.data.length)
      this.state.activeIndex = 0;

    const activeIndex = this.state.activeIndex

    const slides = this.props.data.map(item => (
        <CarouselItem
          className="custom-tag"
          onExiting={this.onExiting}
          onExited={this.onExited}
          key={item.id}
        >
          {
            this.props.isPhone
            ||
            [
              <Input 
                style={style.link} 
                type="number" 
                onChange={(e)=>this.props.onChangeLink(item.id, e.target.value)}
                value={item.link}
                />,
              <Button onClick={()=>this.props.onSubmitLink(item.id)}>Save</Button>
            ]
          }
          <ImageUploadInput 
            banner 
            id={item.id + (this.props.isPhone ? "phone" : "") + this.props.lang}
            styleContainer={{width: "100%"}} 
            style={{width: "100%"}} 
            defaultValue={images.getImagePath(item.image)} 
            onChange={(newVal)=>this.props.onChange(newVal, item.id)}
            onClose={()=>this.props.onClose(item.id)}
            notClose={this.props.isPhone}/>
        </CarouselItem>
      ));

    return(
      <Carousel
        activeIndex={activeIndex}
        next={this.next}
        previous={this.previous}
      >
        <CarouselIndicators items={this.props.data} activeIndex={activeIndex} onClickHandler={this.goToIndex} />
        {slides}
        {
          this.props.isPhone && this.props.data.length>1
          ?
            [<CarouselControl key={1} direction="prev" directionText="Previous" onClickHandler={this.previous} />,
            <CarouselControl key={2} direction="next" directionText="Next" onClickHandler={this.next} /> ]

          :
            null
        }
      </Carousel>
    )
  }
}

export default CarouselComponent;


const style = {
  link: {
      display: "inline-block",
      width: "calc(100% - 65px)",
      minWidth: "20px"
  }
}