import React from "react";
import {
    Button,
    ButtonGroup,
    Input
} from 'reactstrap';

import BootstrapTable from 'react-bootstrap-table-next';
import 'react-bootstrap-table-next/dist/react-bootstrap-table2.min.css';
import cellEditFactory, { Type } from 'react-bootstrap-table2-editor';
import filterFactory, { textFilter, selectFilter } from 'react-bootstrap-table2-filter';

import languages from "../../languages";
import images from "../../constants/images"



class Table extends React.Component {

    expandRow = {
        showExpandColumn: true,
        expandByColumnOnly: true,
        renderer: row => (
            this.renderTable(this.columns, this.props.data, row.id)
        ),
        expandHeaderColumnRenderer: ({ isAnyExpands }) => {
            if (isAnyExpands) {
                return <span>-</span>;
            }
            return <span>+</span>;
        },
        expandColumnRenderer: ({ expanded }) => {
            if (expanded) {
                return (
                    <span>-</span>
                );
            }
            return (
                <span>+</span>
            );
        }
    };

    onClickDelete(id){
        if (window.confirm("Are you sure you want to delete this category")){
            this.props.onDelete(id)
        }
    }

    onUpdate(oldValue, newValue, row, column, done) {
        console.log(oldValue, newValue, row, column, done)
        if (window.confirm('Do you want to accep this change?')) {
            done(true);
            if (column.dataField.split("_")[0]==="title" || column.dataField.split("_")[0]==="filter"){
                this.props.onUpdateTitle({
                    id: row[`id_${column.dataField.split("_")[1]}`], 
                    title: newValue.trim() || null,
                    field: column.dataField.split("_")[0]
                })
            } else {
                let body = {}
                body[column.dataField] = newValue
                console.log(body)
                this.props.onUpdate(row.id, body)
            }
        } else {
            done(false);
        }
        return { async: true };
    }

    renderTable(columns, data, parent) {
        let rows = data.filter((e) => e.parentId == parent)
        if (rows && rows.length)
            return <BootstrapTable
                bootstrap4
                keyField='id'
                data={rows || []}
                columns={columns}
                expandRow={this.expandRow}
                filter={ filterFactory() }
                cellEdit={cellEditFactory({
                    mode: 'dbclick',
                    beforeSaveCell: this.onUpdate.bind(this),
                    // afterSaveCell: (oldValue, newValue, row, column) => { console.log('After Saving Cell!!'); }
                })}
            />
        return (
            <div style={{ textAlign: "center" }}>
                No data have found!
            </div>
        );
    }

    render() {
        console.log(this.props.categoryKeywords, this.props.keywords)
        return this.renderTable(this.columns, this.props.data, null)
    }

    columns = [
        {
            dataField: 'id',
            text: 'ID',
            sort: true,
            filter: textFilter(),
            headerStyle: (colum, colIndex) => ({ width: '60px'})
        },
        {
            dataField: "title_"+languages[0].code,
            text: "Title",
            sort: true,
            filter: textFilter(),
            headerStyle: (colum, colIndex) => ({ width: '160px'})
        },
        {
            text: "Keywords",
            dataField: "keywords",
            editable: false,
            formatter: (cell, row) => (
                <div>
                    {
                        (cell.map((kw, i) => (
                            <Keyword 
                                key={row.id+"_"+i}
                                onDelete={()=>this.props.onDelete(kw.id)} 
                                keyword={kw.keyword}/>
                        )))
                    }
                    <Button style={{marginLeft: 15}} color="success" className="mdi mdi-plus" onClick={() => this.props.onClickCreate(row)} >+</Button>
                </div>
            ),
            style: { verticalAlign: "middle" }
        }
    ];
}

export default Table;


const Keyword = (props) => (
    <span style={{
        padding: "2px 8px", 
        margin: "5px", 
        backgroundColor: "yellow", 
        border: "1px black solid", 
        borderRadius: 5, 
        position: "relative"
    }}>
        <Button style={{
                position: "absolute",
                backgroundColor: "red",
                borderRadius: "50%",
                fontSize: 10,
                padding: "2px 6px",
                top: -10,
                right: -10
            }}
            onClick={()=>{
                if (window.confirm("Do you relly want to delete?")){
                    props.onDelete()
                }
            }}
        >
            x
        </Button>
        {props.keyword}
    </span>
)
