import React from "react";
import {
    Card,
    CardBody,
    CardHeader,
    Modal,
    ModalHeader,
    ModalBody,
    ModalFooter,
    Button,
    Input,
    Label,
    ButtonGroup} from 'reactstrap';
import Table from './table.category'


import { connect } from 'react-redux';
import { 
    getCategories, 
} from '../../redux/categories/action';

import { 
    getCategoryKeywords,
    getKeywords,
    deleteKeyword,
    createKeyword 
} from '../../redux/keywords/action';

const mapStateToProps = state => ({
    categories: state.categories,
    keywords: state.keywords
});
const mapDispatchToProps = dispatch => ({
    getCategories: () => getCategories(dispatch),  
    
    getKeywords: (callback) => getKeywords(dispatch, callback),  
    getCategoryKeywords: (callback) => getCategoryKeywords(dispatch, callback),  
    createKeyword: (body) => createKeyword(dispatch, body),
    deleteKeyword: (id) => deleteKeyword(dispatch, id),    
});


class Keywords extends React.Component {
    state = {
        data: [],
        create: false,
        gotKeywords: false
    }
    
    onClickCreate(row){
        if (row){
            this.parentCategoryForCreate = row.id
            this.parentCategoryType = row.type
        }else {
            this.parentCategoryForCreate = null
            this.parentCategoryType = -1
        }
        this.setState({create: true})
    }

    onCancelCreate(){
        this.setState({create: false})
    }

    getChildCategories(parentId){
        let children = this.props.categories.data.filter(cat => cat.parentId === parentId).map(e => e.id)
        let newChildren = [...children]
        for (const category of children) {
            newChildren.push(...this.getChildCategories(category))
        }
        return newChildren
    }

    createKeyword(){
        if (!this.label.value.length){
            alert("Title can't be emty")
            return
        }

        let body = {
            keyword: this.label.value,
            categories: JSON.stringify([this.parentCategoryForCreate, ...this.getChildCategories(this.parentCategoryForCreate)])
        }

        this.props.createKeyword(body)
        this.onCancelCreate()
    }

    componentWillMount(){
        if (!this.props.categories.data.length){
            this.props.getCategories();
        }
        this.props.getKeywords((err, res)=>{
            if (err){
                return alert(err)
            }
            this.props.getCategoryKeywords((err, res)=>{
                if (err){
                    return alert(err)
                    
                }
                this.setState({gotKeywords: true})
            })
        })
        
    }

    getData(){
        return this.props.categories.data.map(category => ({
            ...category,
            keywords: (this.props.keywords.data[category.id]||[]).map( kwi => ({id: kwi, keyword: this.props.keywords.keywords[kwi]}))
        }))
    }

    render() {
        console.log("keywords", this.props.keywords)
        return (
            <div>
                <Card>
                    <CardHeader>
                        <h3 style={{ display: "inline-block" }}>Categories</h3>
                        <ButtonGroup style={{float: "right"}}>
                            <Button 
                                color="primary" 
                                onClick={()=>this.onClickCreate()}
                            >
                                Create new
                            </Button>
                        </ButtonGroup>
                    </CardHeader>
                    <CardBody>
                        {
                            this.state.gotKeywords
                            ?
                            <Table
                                data = {this.getData()}
                                onDelete = {this.props.deleteKeyword}
                                onClickCreate = {(row)=>this.onClickCreate(row)}
                            />
                            : null
                        }
                    </CardBody>
                </Card>
              
                <Modal
                    isOpen={this.state.create}
                    toggle={()=>this.onCancelCreate()}
                    className={this.props.className}
                    fade={false}
                >
                    <ModalHeader toggle={()=>this.onCancelCreate()}>
                        Add keyword {this.parentCategoryForCreate && "for id="+this.parentCategoryForCreate}
                    </ModalHeader>
                    <ModalBody>
                        <Label for="keyword">Label</Label>
                        <Input innerRef={(node) => this.label = node} type="text" list="keywords"/>
                        <datalist id="keywords">
                            {
                                Object.values(this.props.keywords.keywords).sort().map( elem => (
                                    <option key={elem} value={elem}/>
                                ))
                            }
                        </datalist>
                    </ModalBody>
                    <ModalFooter>
                        <Button color="primary" onClick={()=>this.createKeyword()}>
                            Save
                        </Button>
                        <Button color="secondary" onClick={()=>this.onCancelCreate()}>
                            Cancel
                        </Button>
                    </ModalFooter>
                </Modal>
            </div>
        );
    }

}


export default connect(mapStateToProps, mapDispatchToProps)(Keywords);
