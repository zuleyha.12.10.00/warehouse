import React from "react";
import { Link } from "react-router-dom";
import {
    Button,
    ButtonGroup,
    FormGroup,
    Collapse,
    Label,
    Card,
    CardBody,
    Input,
    Row,
    Col,
    Modal,
    ModalHeader,
    ModalFooter,
    ModalBody
} from 'reactstrap';

import BootstrapTable from 'react-bootstrap-table-next';
import 'react-bootstrap-table-next/dist/react-bootstrap-table2.min.css';
import filterFactory, { textFilter } from 'react-bootstrap-table2-filter';
import paginationFactory from 'react-bootstrap-table2-paginator';

import { connect } from 'react-redux';
import { 
    getProducts, 
    getProductsCount,
    getProductById, 
    deleteProduct, 
    deleteArrayProducts, 
    updateProduct,
    updateProductTitle,
    replaceAll,

    updateSelectedCategories,
    updateSelectedScrapers
    
} from '../../redux/products/action';
import images from "../../constants/images";
import { getCategories } from "../../redux/categories/action";
import { getScrapers } from "../../redux/scraper/action";
import languages from "../../languages";

const mapStateToProps = state => ({
    products: state.products,
    categories: state.categories,
    scrapers: state.scrapers
});


const mapDispatchToProps = dispatch => ({
    getProducts: (body) => getProducts(dispatch, body),  
    getProductsCount: (body, callback) => getProductsCount(dispatch, body, callback),  
    getProductById: (id, callback)=>getProductById(dispatch, id, callback),
    deleteProduct: (id, callback) => deleteProduct(dispatch, id, callback),
    deleteArrayProducts: (arr, callback) => deleteArrayProducts(dispatch, arr, callback),
    updateProduct: (id, body) => updateProduct(dispatch, id, body),
    updateProductTitle: (body) => updateProductTitle(dispatch, body),
    replaceAll: (body, callback) => replaceAll(dispatch, body, callback),
    
    getCategories: () => getCategories(dispatch),

    getScrapers: () => getScrapers(dispatch),

    updateSelectedCategories: (list) => updateSelectedCategories(dispatch, list),
    updateSelectedScrapers: (list) => updateSelectedScrapers(dispatch, list),

});


class Table extends React.Component {

    state = {
        collapsedCategories: [],
        replacing: false
    }

    componentWillMount(){
        if (!this.props.products.data.length)
            this.getFiltersAndProducts()
        if(!this.props.categories.data.length){
            this.props.getCategories()
        }
        if (!this.props.scrapers.data.length)
            this.props.getScrapers();
    }

    selected = []

    getFiltersAndProducts(){
        let body = {
            skip: this.props.products.sizePerPage * (this.props.products.page - 1),
            limit: this.props.products.sizePerPage,
            categoryIds: JSON.stringify(this.props.products.selectedCategories),
            scraperIds: JSON.stringify(this.props.products.selectedScrapers),
            search: this.search && this.search.value ? this.search.value : null
        }

        this.props.getProductsCount(body, (err, res)=> {
            if (err){
                alert(err)
                return
            }
            this.getProducts()
        })
    }

    getProducts(){
        let body = {
            skip: this.props.products.sizePerPage * (this.props.products.page - 1),
            limit: this.props.products.sizePerPage,
            categoryIds: JSON.stringify(this.props.products.selectedCategories),
            scraperIds: JSON.stringify(this.props.products.selectedScrapers),
            search: this.search && this.search.value ? this.search.value : null
        }

        this.props.getProducts(body)
    }

    onChangeScrapers(value, id){
        let newList;
        if (value){
            newList = [...this.props.products.selectedScrapers, id]
        }else{
            newList = this.props.products.selectedScrapers
                .filter((e)=> e !== id )
        }
        this.props.updateSelectedScrapers(newList)
    }

    onChangeCategories(value, id){
        let newList;
        if (value){
            newList = [...this.props.products.selectedCategories, id]
        }else{
            newList = this.props.products.selectedCategories
                .filter((e)=> e !== id )
        }
        this.props.updateSelectedCategories(newList)
    }

    collapseCategory(key){
        if (this.state.collapsedCategories.indexOf(key)<0){
            this.setState({collapsedCategories: [...this.state.collapsedCategories, key]})
        } else {
            this.setState({collapsedCategories: this.state.collapsedCategories.filter( e => e!==key) })
        }
    }
    
    onSubmit(){
        this.state.page = 1
        this.getFiltersAndProducts()
    }

    onClickDelete(id){
        if (window.confirm('Do you want to delete this product?')) {
            this.props.deleteProduct(id, (err, res)=>{
                if (err){
                    alert(err)
                    return
                }
                this.getFiltersAndProducts()
            })
        }
    }

    deleteSelected(){
        if (!this.selected.length){
            return alert("No product has been selected!")
        }
        if (window.confirm('Do you want to delete selected products?')) {
            this.props.deleteArrayProducts(this.selected, (err, succ)=>{
                if (err)
                    alert(err)
                if (succ){
                    this.selected = []
                    this.getFiltersAndProducts()
                }
            })
        }
    }

    toggleReplace(){
        this.setState({replacing: !this.state.replacing})
    }

    confirmReplace(){
        if (window.confirm('Do you want to replace the matchings?')) {
            this.props.replaceAll({from: this.from.value, to: this.to.value}, (err, succ)=>{
                if (err)
                    alert(err)
                if (succ){
                    this.state.replacing = false
                    this.getFiltersAndProducts()
                }
            })
        }
    }

    selectRow(isSelect, row){
        if ( isSelect )
            this.selected.push(row.id)
        else
            this.selected = this.selected.filter( e => e !== row.id )
        return true;
    }

    selectAllRow(isSelect, rows){
        if (isSelect)
            this.selected = rows.map( e => e.id );
        else
            this.selected = []
        return this.selected
    }

    render() {
        return(
            <div>
                <ButtonGroup style={{textAlign: "right", marginBottom: 20, display: "block"}}>
                    <Link to="/products/new" className="btn btn-primary">Create</Link>
                    <Button className="btn" color="danger" onClick={()=>this.deleteSelected()}>Delete</Button>
                </ButtonGroup>
                <div style={{margin: "10px 0"}}>
                    <span style={{margin: "0 10px"}}>
                        Visible: 
                        {" " + this.props.products.sizePerPage * (this.props.products.page-1)} 
                        -
                        {Math.min(this.props.products.sizePerPage * (this.props.products.page), this.props.products.total)} 
                        {"  From "}
                        {this.props.products.total}
                    </span>
                    <Input innerRef={(ref)=> this.search = ref} type="text" placeholder="Search..." style={{width: 180, display: "inline-block", marginRight: 10}}/>
                    <Button onClick={()=>this.onSubmit()} className="btn btn-success">Submit</Button>
                    <Button
                        color={"#fff"} 
                        onClick={()=>this.collapseCategory(-1)}
                        style={{margin: "0px 10px"}}
                    >
                        Select categories: {this.props.products.selectedCategories.length}
                    </Button>
                    <Collapse isOpen={ this.state.collapsedCategories.indexOf(-1) > -1 }>
                        <Card className="border">
                            <CardBody>
                                { this.renderCategories(null, 0) }
                            </CardBody>
                        </Card>
                    </Collapse>
                    <Button
                        color={"#fff"} 
                        style={{margin: "0px 10px"}}
                        onClick={()=>this.collapseCategory(-2)}
                    >
                        Select scrapers: {this.props.products.selectedScrapers.length}
                    </Button>
                    <Collapse isOpen={ this.state.collapsedCategories.indexOf(-2) > -1 }>
                        <Card className="border">
                            <CardBody>
                                { this.renderScrapers() }
                            </CardBody>
                        </Card>
                    </Collapse>
                </div>
                <BootstrapTable
                    bootstrap4
                    keyField='id'
                    rowStyle = {()=>({fontSize: 13, cursor: "pointer"})}
                    data={this.props.products.data || []}
                    columns={this.getColumns()}
                    expandRow={this.expandRow}
                    filter={ filterFactory() }
                    pagination={ paginationFactory({
                        sizePerPageList: [{
                            text: '30', value: 30
                          }, {
                            text: '200', value: 200
                          }, {
                            text: '1000', value: 1000
                          }, {
                            text: '5000', value: 5000
                          }, {
                            text: 'All', value: this.props.products.total
                          }],
                        page: this.props.products.page,
                        totalSize: this.props.products.total,
                        onPageChange: (page, sizePerPage)=>{
                            this.props.products.page = page
                            this.props.products.sizePerPage = sizePerPage
                            this.getProducts()
                        },
                        onSizePerPageChange: (sizePerPage, page) => {
                            this.props.products.page = page
                            this.props.products.sizePerPage = sizePerPage
                            this.getProducts()
                        }
                    }) }
                    selectRow = {{
                        mode: 'checkbox',
                        clickToSelect: false,
                        onSelect: (row, isSelect) => this.selectRow(isSelect, row),
                        onSelectAll: (isSelect, rows) => this.selectAllRow(isSelect, rows)
                    }}
                    noDataIndication={ 'No results found' }
                    remote={ {
                        filter: false,
                        pagination: true,
                        sort: false,
                        cellEdit: false
                    }}
                    hover
                    condensed
                />
                <Modal
                    isOpen={this.state.replacing}
                    toggle={()=>this.toggleReplace()}
                    className={this.props.className}
                    fade={false}
                >
                    <ModalHeader toggle={()=>this.toggleReplace()}>
                        Find and Replace text {this.parentCategoryForCreate && "for id="+this.parentCategoryForCreate}
                    </ModalHeader>
                    <ModalBody>
                        <FormGroup>

                            <Row>
                                <Col xs={6}>
                                    <Label for="from">From</Label>
                                    <Input innerRef={(node) => this.from = node} type="text" name="from"/>
                                </Col>
                                <Col xs={6}>
                                    <Label for="to">TO</Label>
                                    <Input innerRef={(node) => this.to = node} type="text" name="to"/>
                                </Col>
                            </Row>
                            
                        </FormGroup>
                    </ModalBody>
                    <ModalFooter>
                        <Button color="primary" onClick={()=>this.confirmReplace()}>
                            Save
                        </Button>
                        <Button color="secondary" onClick={()=>this.toggleReplace()}>
                            Cancel
                        </Button>
                    </ModalFooter>
                </Modal>
            </div>
        )
    }

    renderScrapers(){
        return this.props.scrapers.data.map(e => (
            <FormGroup check style={{margin: 5}} key={e.id}>
                <Label check>
                    <Input
                        type="checkbox" 
                        checked={this.props.products.selectedScrapers.indexOf(e.id)>-1}
                        onChange={(el)=>this.onChangeScrapers(el.target.checked, e.id)}
                    />{' '}
                    {e.id} - {e.title}
                </Label>
            </FormGroup>
        ))
    }
    
    renderCategories(parent, ind){
        let children = []
        this.props.categories.data.filter((e)=> e.parentId == parent )
            .forEach((e)=>{
                children.push(
                    this.hasChildren(e.id)
                    ?
                    <FormGroup check style={{margin: 5, marginLeft: ind*30}} key={e.id}>
                        <Input 
                            type="checkbox" 
                            checked={this.props.products.selectedCategories.indexOf(e.id)>-1}
                            onChange={(el)=>this.onChangeCategories(el.target.checked, e.id)}
                        />{' '}
                        <b style={{cursor: "pointer"}} onClick={()=>this.collapseCategory(e.id)}>{e.id} - {languages.map((elem, i)=> e[`title_${elem.code}`] ).join(" / ")}</b>
                    </FormGroup>
                    :
                    <FormGroup check style={{margin: 5, marginLeft: ind*30}} key={e.id}>
                        <Label check>
                            <Input 
                                type="checkbox" 
                                checked={this.props.products.selectedCategories.indexOf(e.id)>-1}
                                onChange={(el)=>this.onChangeCategories(el.target.checked, e.id)}
                            />{' '}
                            {e.id} - {languages.map((elem, i)=> e[`title_${elem.code}`] ).join(" / ")}
                        </Label>
                    </FormGroup>,
                    <Collapse key={"collapse_"+e.id} isOpen={ this.state.collapsedCategories.indexOf(e.id) > -1 }>
                        {this.renderCategories(e.id, ind+1)}
                    </Collapse>
                )
            })
        return children   
    }

    hasChildren(id){
        return this.props.categories.data.filter((e)=> e.parentId === id ).length
    }

    getColumns(){
        return [
            {
                dataField: 'id',
                text: 'Info',
                // sort: true,
                formatter: (cell, row) => (
                    <div>
                        {row.id + (row.originalId ? "  / " + row.originalId : "")}
                        <hr/>
                        {row.brand}
                        <hr/>
                        {row.title}
                        <hr/>
                        <del>{row.oldPrice}</del> <span>{row.price} TMT</span>
                        <span style={{color: "#aaa", marginLeft: 10}}><del>{row.originalOldPrice}</del> <span>{row.originalPrice}</span> TL</span>
                    </div>
                )
            },
            // {
            //     dataField: "price",
            //     text: "Price",
            //     // sort: true,
            //     formatter: (col, row) => (
            //         <div>
            //             <del>{row.salePrice}</del> <span>{row.price}</span> TMT
            //             <span style={{color: "#aaa"}}><del>{row.originalSalePrice}</del> <span>{row.originalPrice}</span> TL</span>
            //         </div>
            //     )
            // },
            {
                dataField: "image",
                text: "Image",
                headerStyle: (colum, colIndex) => ({ width: '110px' }),
                formatter: (cell, row) => {
                    if (cell)
                        return <div>
                            <img style={{width: "100%", height: "auto"}} src={images.getThumbPath(cell)}></img>
                            <div style={{display: window.innerWidth > 500 ? "none" : undefined }}>
                                <hr/>
                                {
                                    row.originalLink 
                                    ?
                                    <a target="_blank" href={row.originalLink}>
                                        <Button 
                                            color="info" 
                                            className="mdi mdi-link" 
                                        >
                                            ☇
                                        </Button>
                                    </a>
                                    :
                                    null
                                }
                                {/* <Link to={"/products/"+row.id}>
                                    <Button 
                                        color="primary" 
                                        className="mdi mdi-eye" 
                                    >
                                        ✎
                                    </Button>
                                </Link> */}
                                <Button color="danger" className="mdi mdi-delete" onClick={() => this.onClickDelete(row.id)}>x</Button>
                            </div>
                        </div>
                    return cell
                },
                style: {
                    textAlign: "center"
                }
            },
            {
                text: "Action",
                hidden: window.innerWidth < 500,
                dataField: "createChild",
                formatter: (cellContent, row) => (
                    <ButtonGroup vertical>
                        {
                            row.originalLink 
                            ?
                            <a target="_blank" href={row.originalLink}>
                                <Button 
                                    color="info" 
                                    className="mdi mdi-link" 
                                >
                                    ☇
                                </Button>
                            </a>
                            :
                            null
                        }
                        <Button color="danger" className="mdi mdi-delete" onClick={() => this.onClickDelete(row.id)}>x</Button>
                    </ButtonGroup>
                ),
                style: { textAlign: "center", verticalAlign: "middle" },
                headerStyle: (colum, colIndex) => ({ width: '70px', textAlign: 'center' })
            }
        ];
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Table);
