import React from "react";

import 'react-bootstrap-table-next/dist/react-bootstrap-table2.min.css';

import { connect } from 'react-redux';

import { getFilterKeys } from "../../redux/filterKeys/action";
import { getFilterValues } from "../../redux/filterValues/action";


const mapStateToProps = state => ({
    filterKeys: state.filterKeys,
    filterValues: state.filterValues
});

const mapDispatchToProps = dispatch => ({    
    getFilterKeys: () => getFilterKeys(dispatch),   
    getFilterValues: () => getFilterValues(dispatch), 
});


class Filters extends React.Component {
    state = {
        selectedFilters: [],
    }

    componentWillMount(){
        if(!this.props.filterKeys.data.length){
            this.props.getFilterKeys()
            this.props.getFilterValues()
        }
    }



    render() {
        renderFilters()
        return(
            <div>
                
            </div>
        )
    }

    filterData(){

        let value = this.props.filterValues.array.find(e => e.id === id);
        let key = this.props.filterValues.data.find(e => e.id === value.filterKeyId);

        this.props.productFilters.forEach((e)=>{
            if (obj[e.filterKeyId])
                obj[e.filterKeyId].push(e)
            else
                obj[e.filterKeyId] = [e]
        })
    }

    getValue(id){
        return 
    }


    renderCategories(parent, ind){
        let children = []
        this.props.categories.data.filter((e)=> e.parentId == parent )
            .forEach((e)=>{
                children.push(
                    this.hasChildren(e.id)
                    ?
                    <FormGroup check style={{margin: 5, marginLeft: ind*30}} key={e.id}>
                        <Input 
                            type="checkbox" 
                            checked={this.state.selectedCategories.indexOf(e.id)>-1}
                            onChange={(el)=>this.onChangeCategories(el.target.checked, e.id)}
                        />{' '}
                        <b style={{cursor: "pointer"}} onClick={()=>this.collapseCategory(e.id)}>{e.id} - {languages.map((elem, i)=> e[`title_${elem.code}`] ).join(" / ")}</b>
                    </FormGroup>
                    :
                    <FormGroup check style={{margin: 5, marginLeft: ind*30}} key={e.id}>
                        <Label check>
                            <Input 
                                type="checkbox" 
                                checked={this.state.selectedCategories.indexOf(e.id)>-1}
                                onChange={(el)=>this.onChangeCategories(el.target.checked, e.id)}
                            />{' '}
                            {e.id} - {languages.map((elem, i)=> e[`title_${elem.code}`] ).join(" / ")}
                        </Label>
                    </FormGroup>,
                    <Collapse key={"collapse_"+e.id} isOpen={ this.state.collapsedCategories.indexOf(e.id) > -1 }>
                        {this.renderCategories(e.id, ind+1)}
                    </Collapse>
                )
            })
        return children   
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Filters);
