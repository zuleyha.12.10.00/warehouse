import React from "react";
import {
	Card,
	CardBody,
    CardHeader
} from 'reactstrap';

import Table from './table.products';

class Products extends React.Component {
	render() {
		return (
            <div>
                <Card>
                    <CardHeader>
                        <h3 style={{display: "inline-block"}}>Products</h3>
                        
                    </CardHeader>
                    <CardBody>
                        
                        <Table history={this.props.history} />

                    </CardBody>
                </Card>
            </div>
		);
	}
}

export default Products;