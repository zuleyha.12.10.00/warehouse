import React from "react";
import {
    Card,
    CardBody,
    CardHeader,
    Collapse,
    FormGroup,
    Input,
    Row,
    Col,
    Button,
    Label
} from 'reactstrap';


import { connect } from 'react-redux';
import { getCategories } from '../../redux/categories/action';
import { updateProduct, getProductById, createProduct } from '../../redux/products/action';

import languages from '../../languages';

import FiltersForm from './FiltersForm';
import MultiLangInput from '../../views/components/MultiLangInput'
import { genders } from "../scraperCategories/table.scrapingLinks";

const mapStateToProps = state => ({
    categories: state.categories
});
const mapDispatchToProps = dispatch => ({
    getCategories: () => getCategories(dispatch), 

    createProduct: (body, callback) => createProduct(dispatch, body, callback),   
    updateProduct: (id, body, callback) => updateProduct(dispatch, id, body, callback),
    getProductById: (id, callback) => getProductById(dispatch, id, callback) 
});


class SingleProduct extends React.Component {
    state = {
        selectedCategories: [],
        product: null,
        selectedGender: null,
        collapsedCategories: []
    }

    componentDidMount(){
        if(!this.props.categories.data.length){
            this.props.getCategories()
        }
       
        let id = this.props.match.params.id
        if (id === "new"){
            this.setState({product: {}})
            return;
        }
        
        this.props.getProductById(id, (err, res) => {
            if (err)
                return alert(err);
            this.state.product = res;
            console.log("RES", res)
            this.state.selectedCategories = res.categories || [];
            this.state.selectedGender = res.gender || res.gender === 0 ? 0 : null;
            this.setState(this.state)
        })
    }

    onChangeGender(value){
        this.setState({selectedGender: value})
    }

    onChangeCategories(value, id){
        if (value)
            this.state.selectedCategories.push(id)
        else
            this.state.selectedCategories = this.state.selectedCategories
                .filter((e)=> e !== id )
        
        this.setState(this.state)
    }

    collapseCategory(key){
        if (this.state.collapsedCategories.indexOf(key)<0){
            this.setState({collapsedCategories: [...this.state.collapsedCategories, key]})
        } else {
            this.setState({collapsedCategories: this.state.collapsedCategories.filter( e => e!==key) })
        }
    }

    onClickSubmit(){
        let colors = this.filters.getColors().map( e => ({
            ...e,
            sizes: e.sizes.map( e => ({
                title: e, 
                price: this.price.value || 0,
                oldPrice: this.oldPrice.value === "" ? undefined : this.oldPrice.value,
                isInUSD: this.isInUSD.checked ? 1 : 0, 
            }))
        }))


        let body = {
            ...this.state.product,
            gender: this.state.selectedGender,
            title: this.title.value,
            brand: this.brand.value,
            categories: JSON.stringify(this.state.selectedCategories), // array of category id's. ex: [1, 2, 3]
            colors: JSON.stringify(colors), // array of color objects. ex: [{id: 1, title: "Red", images: ["abcde.jpg"]}]
            description: this.description.value,
            "image[]": this.filters.getImages(), //array of image files
            priority: Number(this.priority.value || 0) < 100000 ? (100000 + Number(this.priority.value || 0) ) : Number(this.priority.value), // integer value
            priceFormat: this.isInUSD.checked ? "USD" : "TMT",
            price: this.price.value || 0, // double value
            oldPrice: this.oldPrice.value === "" ? undefined : this.oldPrice.value, // if in sale double value, if not undefined
        }
       
        console.log(body)

        if (this.props.match.params.id === "new")
            this.props.createProduct(body, (err, succ)=>{
                if (err)
                    return alert(err)
                if (window.confirm("Product created SUCCESSFULLY!"))
                    if (this.props.match.params.stock === "stock")
                        return this.reset()
                    this.props.history.goBack()
            });
        else
            this.props.updateProduct(body.id, body, (err, succ)=>{
                if (err)
                    return alert(err)
                
                this.props.history.goBack()
            });
        
    }

    reset(){
        window.location.reload()
    }

    onCancel(){
        if (window.confirm("Cahnges will be discarded!"))
            this.props.history.goBack()
    }

    getDefaultTrans(key){
        if (!this.state.product.translation)
            return undefined;
        
        let obj = {}
        languages.forEach( e => {
            obj[e.code] = this.state.product.translation[e.code][key]
        })
        if (Object.keys(obj).length===0)
            return undefined;
        return obj
    }

    render() {
        console.log("render", this.state.selectedGender)
        let product = this.state.product;
        if (!product)
            return null;
        return (
            <div>
                <Card>
                    <CardHeader>
                        <h3 style={{ display: "inline-block" }}>
                            {
                                this.props.match.params.id === "new"
                                ?
                                    "Create a new Product"
                                :
                                    "Product id: "+this.props.match.params.id
                            }
                        </h3>
                    </CardHeader>
                    <CardBody>
                        {/******************************************/}
                        {/***   FORM STARTS HERE   *****************/}
                        {/******************************************/}
                        <div>
                            <Row form>
                                <Col md={6}>
                                    <FormGroup>
                                        <span>Title</span>
                                        <Input 
                                            defaultValue={product.title}
                                            innerRef={(node)=> this.title = node }
                                        />
                                    </FormGroup>
                                </Col>
                            </Row>
                            <Row form>
                                <Col md={6}>
                                    <FormGroup>
                                        <span>Brand</span>
                                        <Input 
                                            defaultValue={product.brand}
                                            innerRef={(node)=> this.brand = node }
                                        />
                                    </FormGroup>
                                </Col>
                            </Row>
                            <Row form>
                                {/* <Col md={2}>
                                    <FormGroup>
                                        <span>Price in TL</span>
                                        <Input 
                                            defaultValue={product.originalPrice}
                                            type="number" 
                                            placeholder="Ex: 120.50" 
                                            innerRef={(node)=> this.originalPrice = node }
                                        />
                                    </FormGroup>
                                </Col>
                                <Col md={2}>
                                    <FormGroup>
                                        <span>Old price in TL</span>
                                        <Input 
                                            defaultValue={product.originalOldPrice}
                                            type="number" 
                                            placeholder="Ex: 120.50" 
                                            innerRef={(node)=> this.originalOldPrice = node }
                                        />
                                    </FormGroup>
                                </Col> */}
                                <Col md={12}>
                                <FormGroup check>
                                    <Label check>
                                        <Input 
                                            type="checkbox" 
                                            defaultChecked = {product.isInUSD}
                                            innerRef={(node)=> this.isInUSD = node }
                                        />
                                        Is price in USD?
                                    </Label>
                                </FormGroup>
                                </Col>
                                <Col md={2}>
                                    <FormGroup>
                                        <span>Price</span>
                                        <Input 
                                            defaultValue={product.price}
                                            type="number" 
                                            placeholder="Ex: 120.50" 
                                            innerRef={(node)=> this.price = node }
                                        />
                                    </FormGroup>
                                </Col>
                                <Col md={2}>
                                    <FormGroup>
                                        <span>Old price</span>
                                        <Input 
                                            defaultValue={product.oldPrice}
                                            type="number" 
                                            placeholder="Ex: 120.50" 
                                            innerRef={(node)=> this.oldPrice = node }
                                        />
                                    </FormGroup>
                                </Col>
                                <Col md={2}>
                                    <FormGroup>
                                        <span>Priority</span>
                                        <Input 
                                            defaultValue={product.priority}
                                            type="number" 
                                            innerRef={(node)=> this.priority = node }
                                        />
                                    </FormGroup>
                                </Col>
                            </Row>
                            <hr/>
                            <FormGroup>
                                { this.renderGenders() }
                            </FormGroup>
                            <hr/>
                            <FormGroup>
                                <Button
                                    color={"#fff"} 
                                    onClick={()=>this.setState({categoryCollapse: !this.state.categoryCollapse})}
                                >
                                    Select categories: {this.state.selectedCategories.length}
                                </Button>
                                <Collapse isOpen={this.state.categoryCollapse}>
                                    <Card className="border">
                                        <CardBody>
                                            { this.renderCategories() }
                                        </CardBody>
                                    </Card>
                                </Collapse>
                            </FormGroup>

                            <hr/>

                            <FiltersForm 
                                innerRef={(node)=> this.filters = node }
                                defaultValue={product.colors}
                            />

                            <hr/>

                            <FormGroup>
                                <FormGroup>
                                    <span>Description</span>
                                    <Input
                                        type='textarea' 
                                        defaultValue={product.title}
                                        innerRef={(node)=> this.description = node }
                                    />
                                </FormGroup>
                            </FormGroup>
                        
                            <Button 
                                color="success"
                                onClick={()=>this.onClickSubmit()}
                            >
                                Save
                            </Button> {" "}
                            <Button color="secondary" onClick={()=>this.onCancel()}>Cancel</Button>
                        </div>
                        {/******************************************/}
                        {/***   FORM  ENDS  HERE   *****************/}
                        {/******************************************/}

                    </CardBody>
                </Card>
            </div>
        );
    }

    renderCategories(){
         return this.renderCategoriesRecursive(null, 0)
    }

    renderCategoriesRecursive(parent, ind){
        let children = []
        let categories = this.props.categories.data;
        if (this.props.match.params.stock === "stock" && parent === null){
            categories = categories.filter( e => e.parentId)
            return this.renderCategoriesRecursive(247, 0)
        }
        categories.filter((e)=> e.parentId == parent )
            .forEach((e)=>{
                children.push(
                    this.hasChildren(e.id)
                    ?
                    <FormGroup check style={{margin: 5, marginLeft: ind*30}} key={e.id}>
                        <b style={{cursor: "pointer"}} onClick={()=>this.collapseCategory(e.id)}>{e.id} - {languages.map((elem, i)=> e[`title_${elem.code}`] ).join(" / ")}</b>
                    </FormGroup>
                    :
                    <FormGroup check style={{margin: 5, marginLeft: ind*30}} key={e.id}>
                        <Input
                            id={`category_${e.id}`} 
                            name={`category_${e.id}`} 
                            style={{cursor: "pointer"}}
                            type="checkbox" 
                            checked={this.state.selectedCategories.indexOf(e.id)>-1}
                            onChange={(el)=>this.onChangeCategories(el.target.checked, e.id)}
                        />
                        <Label style={{cursor: "pointer"}} for={`category_${e.id}`}>{' '}{e.id} - {languages.map((elem, i)=> e[`title_${elem.code}`] ).join(" / ")}</Label>
                    </FormGroup>,
                    <Collapse key={"collapse_"+e.id} isOpen={ this.state.collapsedCategories.indexOf(e.id) > -1 }>
                        {this.renderCategoriesRecursive(e.id, ind+1)}
                    </Collapse>
                )
            })
        return children  
    }

    renderGenders(){
        return(
            <select value={this.state.selectedGender||(this.state.selectedGender === 0 ? 0 : -1)} onChange={(el)=>this.onChangeGender(el.target.value)}>
                <option disabled value={-1}>Select gender</option>
                {
                    Object.keys(genders).map((e)=> (
                        <option key={e} value={e}>{genders[e]}</option>
                    ))
                }
            </select>
        )
    }

    hasChildren(id){
        return this.props.categories.data.filter((e)=> e.parentId === id ).length
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(SingleProduct);
