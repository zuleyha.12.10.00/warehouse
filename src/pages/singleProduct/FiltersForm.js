import React from "react";
import {
    FormGroup,
    Label,
    Input,
    Row,
    Col,
    Button,
    ButtonGroup
} from 'reactstrap';

import ImageUploadInput from '../../views/components/ImageUploadInput'


const titleCase = (str) => {
    var result = ""
    var splitStr = str.toLowerCase().split(' ');
    for (var i = 0; i < splitStr.length; i++) {
        if (splitStr[i])
            result += splitStr[i].charAt(0).toUpperCase() + splitStr[i].substring(1);     
    }
    return result; 
}

class FilterForm extends React.Component {

    state = {
        colors: [],
        images: []
    }

    images = {}

    reset(){
        this.images = {}
        this.setState({colors: [], images: []})
    }

    getColors(){
        return this.state.colors.map( e => {
            if (!e.title)
                delete e.title
            else 
                e.title = titleCase(e.title)

            e.images = (e.images||[]).filter( e => e)
            e.sizes = (e.sizes||[]).filter( e => e).map(e => e.trim().toUpperCase() )
            return e
        })
    }

    getImages(){
        return Object.values(this.images)
    }

   
    // componentWillMount(){
    //     if(!this.props.filterKeys.data.length){
    //         this.props.getFilterKeys()
    //         this.props.getFilterValues()
    //     }
    // }

    componentDidMount(){
        this.props.innerRef(this)
    }

    renameFile(file, newName) {
        newName += "_"+Date.now()+"."+file.name.split('.').pop();
        return {
            file: new File([file], newName, {
                    type: file.type,
                    lastModified: file.lastModified,
                }),
            title: newName
        }
    }

    addNewColor(){
        this.setState({colors: [...this.state.colors, {images: [], sizes: []}]})
    }

    onRemoveColor(ind){
        this.state.colors = this.state.colors.filter( (e, i) => i != ind)
        this.setState(this.state)
    }

    onChangeColorTitle(ind, val){
        this.state.colors[ind].title = val;
        this.setState(this.state)
    }
    
    onAddColorSize(colorInd){
        this.state.colors[colorInd].sizes = [...(this.state.colors[colorInd].sizes||[]), null]
        this.setState(this.state)
    }

    onChangeSizeTitle(cInd, sInd, val){
        this.state.colors[cInd].sizes[sInd] = val
        this.setState(this.state)
    }

    onRemoveColorSize(cInd, sInd){
        this.state.colors[cInd].sizes = this.state.colors[cInd].sizes.filter( (e, i) => i !== sInd )
        this.setState(this.state)
    }

    onAddColorImage(colorInd){
        this.state.colors[colorInd].images = [...(this.state.colors[colorInd].images||[]), null]
        this.setState(this.state)
    }

    onChangeColorImages(colorInd, imgInd, newImg){
        newImg = this.renameFile(newImg, "")
        this.images[newImg.title] = newImg.file
        this.state.colors[colorInd].images[imgInd] = newImg.title
        this.setState(this.state)
    }

    onRemoveColorImages(cInd, imgInd){
        this.state.colors[cInd].images = this.state.colors[cInd].images.filter((e, i) => {
            if (i === imgInd) {
                delete this.images[e]
                return false
            }
            return true
        })
        this.setState(this.state)
    }

    getMyDefaultProps(){
        if (this.hasLoaded || !this.props.defaultValue){
            return
        }
        this.hasLoaded = true
        this.setState({colors: this.props.defaultValue})
    }

    render() {
        this.getMyDefaultProps()
        return (
            <FormGroup>
                <FormGroup>
                    <Label>Colors</Label>
                    {
                        this.state.colors.map((color, i) => (
                            this.colorRenderer(color, i)
                        ))
                    }
                    
                </FormGroup>
                <Button className="btn btn-info" onClick={()=>this.addNewColor()}>Add Color</Button>

            </FormGroup>
        );
    }

    colorRenderer = (color, i) => (
        <FormGroup key={i} style={{paddingTop: 10, paddingLeft: 10, backgroundColor: i%2 === 1 ? "rgba(0, 0, 0, .02)" : "rgba(0, 0, 0, .05)"}}>
            <Row>
                <Col sm={6}>
                    <FormGroup>
                        <Input 
                            key={i}
                            value={color.title||""}
                            placeholder="ex: Red"
                            onChange={(e)=>this.onChangeColorTitle(i, e.target.value)}
                            style={{width: "calc(100% - 90px)", display: "inline-block"}}
                        />
                        <ButtonGroup style={{float: "right"}}>
                            <Button className="mdi mdi-close btn-danger" onClick={()=>this.onRemoveColor(i)}>x</Button>
                            <Button className="mdi mdi-camera btn-primary" onClick={()=>this.onAddColorImage(i)}>📸</Button>
                        </ButtonGroup>
                    </FormGroup>
                </Col>
                <Col sm={12}>
                    {
                        (color.images||[]).map( (img, imgInd) => (
                            this.imagesRenderer(i, imgInd, img)
                        ))
                    }
                </Col>
                <Col sm={12}>
                <FormGroup>
                        <br/>
                        <Label>Sizes</Label><br/>
                        {
                            (color.sizes||[]).map( (s, sInd) => (
                                <span key={sInd} style={{marginRight: 10}}>
                                    <Input 
                                        key={i}
                                        value={s||""}
                                        placeholder="ex: SM"
                                        onChange={(e)=>this.onChangeSizeTitle(i, sInd, e.target.value)}
                                        style={{width: "125px", display: "inline-block", marginRight: 5}}
                                    />
                                    <Button className="mdi mdi-close btn-danger" onClick={()=>this.onRemoveColorSize(i, sInd)}>x</Button>
                                </span>
                            ))
                        }
                        
                        <Button className="mdi mdi-plus btn-primary" onClick={()=>this.onAddColorSize(i)}>+</Button>
                    </FormGroup>
                </Col>
            </Row>
        </FormGroup>
    )

    imagesRenderer = (colorInd, imgInd, img) => (
        <ImageUploadInput 
            id={`product_img_${colorInd}_${imgInd}`} 
            key={`product_img_${colorInd}_${imgInd}`} 
            style={{height: 75, margin: "0 5px"}}
            fileValue = {this.images[img] || img}
            onClose = {()=>this.onRemoveColorImages(colorInd, imgInd)}
            onChange = {(e)=>this.onChangeColorImages(colorInd, imgInd, e)}
        />
    )
}

export default (FilterForm);
