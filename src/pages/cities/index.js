import React from "react";
import {
    Card,
    CardBody,
    CardHeader,
    Modal,
    ModalHeader,
    ModalBody,
    ModalFooter,
    Button,
    Input,
    FormGroup,
    Label,
    ButtonGroup
} from 'reactstrap';
import MultiLangInput from '../../views/components/MultiLangInput'
import Table from './table.category'

import { connect } from 'react-redux';
import { 
    getCities, 
    createCity, 
    deleteCity, 
    updateCity,
    updateCityTitle
} from '../../redux/cities/action';


const mapStateToProps = state => ({
    cities: state.cities
});
const mapDispatchToProps = dispatch => ({
    getCities: () => getCities(dispatch),  
    createCity: (body) => createCity(dispatch, body),
    deleteCity: (id) => deleteCity(dispatch, id),
    updateCity: (id, body) => updateCity(dispatch, id, body),
    updateCityTitle: (body) => updateCityTitle(dispatch, body)
});


class Cities extends React.Component {
    state = {
        data: [],
        create: false
    }
    

    onClickCreate(row){
        if (row)
            this.parentCityForCreate = row.id
        else 
            this.parentCityForCreate = null
        
        this.setState({create: true})
    }

    onCancelCreate(){
        this.setState({create: false})
    }

    createCity(){
        if (!Object.keys(this.refs.title.value).length){
            alert("Title can't be emty")
            return
        }

        let body = {
            title: JSON.stringify(this.refs.title.value),
            priority: this.priority.value,
            price: this.price.value
        }

        if(this.parentCityForCreate){
            body["parentId"] = this.parentCityForCreate
        }

        this.props.createCity(body)
        this.onCancelCreate()
    }

    componentWillMount(){
        if (!this.props.cities.data.length)
            this.props.getCities();
    }

    render() {
        return (
            <div>
                <Card>
                    <CardHeader>
                        <h3 style={{ display: "inline-block" }}>Cities</h3>
                        <ButtonGroup style={{float: "right"}}>
                            <Button 
                                color="primary" 
                                onClick={()=>this.onClickCreate()}
                            >
                                Create new
                            </Button>
                        </ButtonGroup>
                    </CardHeader>
                    <CardBody>
                        <Table
                            data = {this.props.cities.data}
                            onClickCreate = {(row)=>this.onClickCreate(row)}
                            onDelete = {this.props.deleteCity}
                            onUpdate = {this.props.updateCity}
                            onUpdateTitle = {this.props.updateCityTitle}
                        />
                    </CardBody>
                </Card>
              
                <Modal
                    isOpen={this.state.create}
                    toggle={()=>this.onCancelCreate()}
                    className={this.props.className}
                    fade={false}
                >
                    <ModalHeader toggle={()=>this.onCancelCreate()}>
                        Create city {this.parentCityForCreate && "for id="+this.parentCityForCreate}
                    </ModalHeader>
                    <ModalBody>
                        <FormGroup>
                            <MultiLangInput ref="title" inputTag={Input} name="title" />
                            <Label for="priority">Priority</Label>
                            <Input innerRef={(node) => this.priority = node} type="number" name="priority" defaultValue="0"/>
                            <Label for="Price">Price</Label>
                            <Input innerRef={(node) => this.price = node} type="number" name="price" defaultValue="0"/>
                        </FormGroup>
                    </ModalBody>
                    <ModalFooter>
                        <Button color="primary" onClick={()=>this.createCity()}>
                            Save
                        </Button>
                        <Button color="secondary" onClick={()=>this.onCancelCreate()}>
                            Cancel
                        </Button>
                    </ModalFooter>
                </Modal>
            </div>
        );
    }

}


export default connect(mapStateToProps, mapDispatchToProps)(Cities);
