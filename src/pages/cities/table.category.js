import React from "react";
import {
    Button,
    ButtonGroup,
    Input
} from 'reactstrap';

import BootstrapTable from 'react-bootstrap-table-next';
import 'react-bootstrap-table-next/dist/react-bootstrap-table2.min.css';
import cellEditFactory from 'react-bootstrap-table2-editor';
import filterFactory, { textFilter } from 'react-bootstrap-table2-filter';

import languages from "../../languages";
import images from "../../constants/images"

import add_image from "../../assets/images/upload_img.jpg"


class Table extends React.Component {

    expandRow = {
        showExpandColumn: true,
        expandByColumnOnly: true,
        renderer: row => (
            this.renderTable(this.columns, this.props.data, row.id)
        ),
        expandHeaderColumnRenderer: ({ isAnyExpands }) => {
            if (isAnyExpands) {
                return <span>-</span>;
            }
            return <span>+</span>;
        },
        expandColumnRenderer: ({ expanded }) => {
            if (expanded) {
                return (
                    <span>-</span>
                );
            }
            return (
                <span>+</span>
            );
        }
    };

    onClickDelete(id){
        if (window.confirm("Are you sure you want to delete this category")){
            this.props.onDelete(id)
        }
    }

    onUpdate(oldValue, newValue, row, column, done) {
        if (window.confirm('Do you want to accep this change?')) {
            done(true);
            if (column.dataField.split("_")[0]==="title"){
                this.props.onUpdateTitle({
                    id: row[`id_${column.dataField.split("_")[1]}`], 
                    title: newValue
                })
            } else {
                let body = {}
                body[column.dataField] = newValue
                this.props.onUpdate(row.id, body)
            }
        } else {
            done(false);
        }
        return { async: true };
    }

    renderTable(columns, data, parent) {
        let rows = data.filter((e) => e.parentId == parent)
        if (rows && rows.length)
            return <BootstrapTable
                bootstrap4
                keyField='id'
                data={rows || []}
                columns={columns}
                expandRow={this.expandRow}
                filter={ filterFactory() }
                cellEdit={cellEditFactory({
                    mode: 'dbclick',
                    beforeSaveCell: this.onUpdate.bind(this),
                    // afterSaveCell: (oldValue, newValue, row, column) => { console.log('After Saving Cell!!'); }
                })}
            />
        return (
            <div style={{ textAlign: "center" }}>
                No data have found!
            </div>
        );
    }

    render() {
        return this.renderTable(this.columns, this.props.data, null)
    }

    columns = [
        {
            dataField: 'id',
            text: 'ID',
            sort: true,
            filter: textFilter()
        },
        ...languages.map((e) => (
            {
                dataField: "title_" + e.code,
                text: "Title: " + e.code,
                sort: true,
                filter: textFilter()
            }
        )),
        {
            dataField: 'priority',
            text: 'Priority',
            sort: true,
            filter: textFilter()
        },
        {
            dataField: 'price',
            text: 'Price',
            sort: true,
            filter: textFilter()
        },
        {
            text: "Action",
            dataField: "createChild",
            editable: false,
            formatter: (cellContent, row) => (
                <ButtonGroup>
                    <Button color="success" className="mdi mdi-plus" onClick={() => this.props.onClickCreate(row)} >+</Button>
                    <Button color="danger" className="mdi mdi-delete" onClick={() => this.onClickDelete(row.id)}>x</Button>
                </ButtonGroup>
            ),
            style: { textAlign: "center", verticalAlign: "middle" }
        }
    ];
}

export default Table;