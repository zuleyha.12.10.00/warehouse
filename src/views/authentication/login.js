import React from 'react';
import {
	InputGroup,
	InputGroupAddon,
	InputGroupText,
	Input,
	CustomInput,
	FormGroup,
	Row,
	Col,
	UncontrolledTooltip,
	Button,
	Label
} from 'reactstrap';
// import img1 from '../../assets/images/logo-icon.png';
import img2 from '../../assets/images/login-register.png';
import logo from '../../assets/images/arcalyk_logo.png';
import footer from '../../assets/images/payhas_logo.png';
import { Formik, Field, Form, ErrorMessage } from 'formik';
import * as Yup from 'yup';
import { authenticationService } from '../../request/_services/auth';

const sidebarBackground = {
	backgroundImage: "url(" + img2 + ")",
	backgroundRepeat: "no-repeat",
	backgroundPosition: "left center",
	backgroundSize: "50% 100%"
};

class Login extends React.Component {
	constructor(props) {
		super(props);
		this.handleClick = this.handleClick.bind(this);
	}

	handleClick() {
		var elem = document.getElementById('loginform');
		elem.style.transition = "all 2s ease-in-out";
		elem.style.display = "none";
		document.getElementById('recoverform').style.display = "block";
	}
	render() {
		return <div className="">
			{/*--------------------------------------------------------------------------------*/}
			{/*Login Cards*/}
			{/*--------------------------------------------------------------------------------*/}
			<div className="auth-wrapper d-flex no-block justify-content-center align-items-center" style={sidebarBackground}>
				<div className="auth-box on-sidebar">
					<div id="loginform">
						<div className="logo">
							{/* <span className="db"><img src={img1} alt="logo" width="80%" /></span> */}
							<div className="font-inter mb-1" style={{fontSize: '25px', fontWeight: '600', color: '#333333'}}>Welcome to Ammarhana</div>
							<div className="font-inter" style={{fontSize: '18px', color: '#525252'}}>Sign in your account</div>
						</div>
						<div className='auth-form'>
							<div style={{textAlign:'center'}}>
							<img src={logo}/>
							</div>
							<Row>
								<Col>
									<Formik
										initialValues={{
											username: '',
											password: ''
										}}
										validationSchema={Yup.object().shape({
											username: Yup.string().required('Username is required'),
											password: Yup.string().required('Password is required')
										})}
										onSubmit={({ username, password }, { setStatus, setSubmitting }) => {
											setStatus();
											authenticationService.login(username, password)
												.then(
													data => {
														const { from } = this.props.location.state || { from: { pathname: "/" } };
														this.props.history.push(from);
													},
													error => {
														setSubmitting(false);
														setStatus(error);
													}
												);
										}}
										render={({ errors, status, touched, isSubmitting }) => (
											<Form className="mt-3" id="loginform">
												<FormGroup className="mb-3">
													<Label for="username">
														Имя пользователя
													</Label>

													<Field id="username" name="username" type="text" className={'form-control' + (errors.username && touched.username ? ' is-invalid' : '')} />
													<ErrorMessage name="username" component="div" className="invalid-feedback" />
												</FormGroup>
												<FormGroup className="mb-3">
													<Label for="password">
														Пароль
													</Label>

													<Field id="password" name="password" type="password" className={'form-control' + (errors.password && touched.password ? ' is-invalid' : '')} />
													<ErrorMessage name="password" component="div" className="invalid-feedback" />
												</FormGroup>
												<Row>
													<Col>
														<FormGroup check className='mb-3' style={{float:'right'}}>
															<Input
															id="exampleCheckbox"
															name="checkbox"
															type="checkbox"
															/>
															<Label
															check
															for="exampleCheckbox"
															>
																Запомнить меня
															</Label>
														</FormGroup>
													</Col>
												</Row>
												<Row className="mb-3">
													<Col xs="12">
														<button type="submit" className="btn btn-block btn-primary btn-login" disabled={isSubmitting}>Вход</button>
													</Col>
												</Row>
												{
													// console.log(status)
													// status && <div className={'alert alert-danger'}>{status}</div>
													status && <div className={'alert alert-danger'}>Error!</div>
												}
											</Form>
										)}
									/>
								</Col>
							</Row>
						</div>
					</div>
					<div id="recoverform">
						<div className="logo">
							{/* <span className="db"><img src={img1} alt="logo" /></span> */}
							<h5 className="font-medium mb-3">Recover Password</h5>
							<span>Enter your Email and instructions will be sent to you!</span>
						</div>
						<Row className="mt-3">
							<Col xs="12">
								<Form action="/dashbaord">
									<FormGroup>
										<Input type="text" name="uname" bsSize="lg" id="Name" placeholder="Username" required />
									</FormGroup>
									<Row className="mt-3">
										<Col xs="12">
											<Button color="danger" size="lg" type="submit" block>Reset</Button>
										</Col>
									</Row>
								</Form>
							</Col>
						</Row>
					</div>
					<div className='footer'>
						<img src={footer} />
					</div>
				</div>
			</div>
		</div>
	}
}

export default Login;
