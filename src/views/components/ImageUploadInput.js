import React from "react";
import { Input, Button } from 'reactstrap';

import img_default from '../../assets/images/add-image.png';
import banner_default from "../../assets/images/banner_default.png"
import images from "../../constants/images";

class ImageUploadInput extends React.Component {
    state = {
        value: null
    }
    
    onChange(element){
        if (!element.target.files[0]){
            return;
        }
        this.value = element.target.files[0]

        var reader = new FileReader();
        reader.onload = (e) => {
            // get loaded data and render thumbnail.
            document.getElementById(this.props.id+"img").src = e.target.result;
        };
    
        // read the image file as a data URL.
        reader.readAsDataURL(this.value);
        


        if (this.props.onChange)
            this.props.onChange(this.value)
    };

    onClose(){
        if (this.props.onClose){
            this.props.onClose()
        } else {
            document.getElementById(this.props.id+"img").src = (this.props.banner ? banner_default : img_default);
        }
    }

    componentWillReceiveProps(props){
        if (props.fileValue!==this.props.fileValue){

            if (typeof(props.fileValue)==="string"){
                return;
            }

            var reader = new FileReader();
            reader.onload = (e) => {
                // get loaded data and render thumbnail.
                document.getElementById(props.id+"img").src = e.target.result;
            };
            reader.readAsDataURL(props.fileValue);
        }
    }
 
    render() {
        let dflt = this.props.defaultValue || (typeof(this.props.fileValue)==="string" && images.getImagePath(this.props.fileValue));
        return (
            <div style={{...styles.container, ...(this.props.styleContainer||{})}}>
                <img 
                    id = {this.props.id+"img"}
                    src={dflt || (this.props.banner ? banner_default : img_default)} 
                    style={this.props.style}
                    onClick={()=>{document.getElementById(this.props.id+"imgInput").click()}}
                    />
                
                <span 
                    style={styles.close} 
                    hidden={this.props.notClose}
                    className="mdi mdi-close-box"
                    onClick={()=>this.onClose()}
                >x</span>

                <Input 
                    type="file" 
                    name="image" 
                    id={this.props.id+"imgInput"} 
                    accept="image/x-png,image/jpeg,image/webp" 
                    onChange={(e) => this.onChange(e)}
                    style={{display: "none"}}
                />
            </div>                  
        );
    }
}

export default ImageUploadInput;

const styles = {
    container: {
        display: "inline-block",
        position: "relative"
    },
    close: {
        position: "absolute",
        right: 0,
        top: -5,
        padding: 0,
        width: 20,
        height: 20,
        textAlign: "center",
        color: "white",
        fontSize: 13,
        backgroundColor: "red",
        borderRadius: "50%",
        cursor: "pointer"
    }
}