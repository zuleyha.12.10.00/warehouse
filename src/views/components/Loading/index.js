import React from 'react';
import './style.css'

export default (props)=>{

    return(
        <div style={{...style, display: (props.isActive? "block" : "none")}}>
            <div className="lds-ellipsis"><div></div><div></div><div></div><div></div></div>        
        </div>
    )
}

const style = {
    width: "100%",
    height: "100%",
    top: 0,
    position: "fixed",
    zIndex: 99999,
    left: 0,
    background: "rgba(0, 0, 0, .6)",
    textAlign: "center",
    padding: "calc(50vh - 140px) calc(50vw + 40px)"
}