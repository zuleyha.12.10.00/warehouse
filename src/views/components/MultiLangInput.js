import React from "react";
import {
    CustomInput,
    FormGroup, 
    Label,
    Input
} from 'reactstrap';
import 'react-bootstrap-table-next/dist/react-bootstrap-table2.min.css';
import languages from '../../languages'

class MultiLangInput extends React.Component {
    
    state = {
        multiLang: this.props.defaultValue ? true : false
    }

    value = {}

    onChange(val, key){
        if (this.state.multiLang)
            this.value[key] = val
        else
            languages.forEach( (e)=>this.value[e.code] = val )
    }

    toggleMultiLang(val){
        this.value = {}
        this.setState({multiLang: val})
    }

    componentWillMount(){
        this.value = this.props.defaultValue || {}
    }

    render() {
        let Tag = this.props.inputTag || Input
        let name = this.props.name
        let type = this.props.type
        return (
            <FormGroup>
                <Label style={{textTransform: "capitalize", fontWeight: "bold"}}>{this.props.name}</Label>
                <FormGroup check>
                    <Label check>
                        <Input 
                            type="checkbox" 
                            defaultChecked = {this.props.defaultValue}
                            onChange={(e) => this.toggleMultiLang(e.target.checked)} 
                        />
                        Turn on if multilingual
                    </Label>
                </FormGroup>
                {
                    this.state.multiLang
                        ?
                        languages.map((elem, ind) => (
                            <Tag
                                key={ind}
                                onChange={(e)=>this.onChange(e.target.value, elem.code)}
                                defaultValue = {this.props.defaultValue && this.props.defaultValue[elem.code]}
                                type={type}
                                name={name + "_" + elem.code}
                                id={name + "_" + elem.code}
                                placeholder={name + ": " + elem.code} />
                        ))
                        :
                        <Tag
                            onChange={(e)=>this.onChange(e.target.value)}
                            type={type}
                            name={name}
                            id={name}
                            placeholder={name+ ": for all languages"} />
                }
            </FormGroup>
        );
    }
}

export default MultiLangInput;