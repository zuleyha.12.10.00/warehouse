import firebase from 'firebase/app';
import 'firebase/messaging';

const firebaseConfig = {
  apiKey: "AIzaSyCnzG-rCwK6nQ3kBGPtLyYWKTeqm7NaZds",
  authDomain: "sarga-gelsin.firebaseapp.com",
  projectId: "sarga-gelsin",
  storageBucket: "sarga-gelsin.appspot.com",
  messagingSenderId: "581465811376",
  appId: "1:581465811376:web:8c061c94749c8c56c98e75",
  measurementId: "G-G4V185MHSV"
};

export var messaging, getToken, onMessageListener;

try {

  firebase.initializeApp(firebaseConfig);

  messaging = firebase.messaging();

  getToken = (setTokenFound) => {
    return messaging.getToken({vapidKey: 'BFVk0_O5wuqhseoo7Y8gRYOoy5w3kJ3nj_IjL_G9fK1XwcIIVwvCOnqKxQz6hudSC28OfmxZMvUMn0xjcJjouxM'}).then((currentToken) => {
      if (currentToken) {
        console.log('current token for client: ', currentToken);
        setTokenFound(currentToken);
        // Track the token -> client mapping, by sending to backend server
        // show on the UI that permission is secured
      } else {
        console.log('No registration token available. Request permission to generate one.');
        setTokenFound(false);
        // shows on the UI that permission is required 
      }
    }).catch((err) => {
      console.log('An error occurred while retrieving token. ', err);
      // catch error while creating client token
    });
  }

  onMessageListener = () =>
    new Promise((resolve) => {
      messaging.onMessage((payload) => {
        console.log(payload)
        alert(payload)
        resolve(payload);
      });
  });
} catch {}
