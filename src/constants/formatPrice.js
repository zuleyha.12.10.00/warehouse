export default (price, quantity) => {
    if (!quantity)
        quantity = 1;
    
    return ((price/100) * quantity).toFixed(2)
}