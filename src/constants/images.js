import host from "./host";
const dir = "public/images/";

export default {
    getImagePath: ( image ) => {
        if (!image)
            return image
        if (image && image.startsWith("http"))
            return image
        return host.API_HOST + dir + image
    },
    getThumbPath: ( image ) => {
        if (!image)
            return image
        if (image && image.startsWith("http"))
            return image
        return host.API_HOST + dir + "thumb/" + image
    }
}