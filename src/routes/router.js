
//Ui-components Dropdown
import React from 'react'
import Products from '../pages/products';
import SingleProduct from '../pages/singleProduct';
import ScraperCategories from '../pages/scraperCategories';
import Categories from '../pages/categories';
import Brands from '../pages/brands';
import Cities from '../pages/cities';
import Users from '../pages/users';
import Orders from '../pages/orders';
import Home from '../pages/home';
import Scraper from '../pages/scraper';
import Currency from '../pages/currency';
import Staff from '../pages/staff';
import Statistics from '../pages/statistics';
import Calculation from '../pages/calculation';


var ThemeRoutes = [
	{
		path: '/stats',
		name: "Seljerme",
		icon: (<svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
		<path d="M2 22H22" stroke="#213067" stroke-width="1.5" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"/>
		<path d="M9.75 4V22H14.25V4C14.25 2.9 13.8 2 12.45 2H11.55C10.2 2 9.75 2.9 9.75 4Z" stroke="#213067" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
		<path d="M3 10V22H7V10C7 8.9 6.6 8 5.4 8H4.6C3.4 8 3 8.9 3 10Z" stroke="#213067" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
		<path d="M17 15V22H21V15C21 13.9 20.6 13 19.4 13H18.6C17.4 13 17 13.9 17 15Z" stroke="#213067" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
		</svg>),
		component: Statistics,
		isOnlyForAdmin: true
	},
	{
		path: '/orders',
		name: 'Galyndy',
		icon: (<svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
		<path d="M12.5 18.9584H7.49996C2.97496 18.9584 1.04163 17.025 1.04163 12.5V7.50002C1.04163 2.97502 2.97496 1.04169 7.49996 1.04169H12.5C17.025 1.04169 18.9583 2.97502 18.9583 7.50002V12.5C18.9583 17.025 17.025 18.9584 12.5 18.9584ZM7.49996 2.29169C3.65829 2.29169 2.29163 3.65835 2.29163 7.50002V12.5C2.29163 16.3417 3.65829 17.7084 7.49996 17.7084H12.5C16.3416 17.7084 17.7083 16.3417 17.7083 12.5V7.50002C17.7083 3.65835 16.3416 2.29169 12.5 2.29169H7.49996Z" fill="#213067"/>
		<path d="M11.4667 14.7916H8.52499C7.60832 14.7916 6.88332 14.3416 6.47499 13.525L5.73333 12.0333C5.55833 11.675 5.19999 11.4583 4.79999 11.4583H1.65833C1.31666 11.4583 1.03333 11.175 1.03333 10.8333C1.03333 10.4916 1.32499 10.2083 1.66666 10.2083H4.79999C5.67499 10.2083 6.45833 10.6916 6.84999 11.475L7.59166 12.9666C7.78332 13.35 8.09166 13.5416 8.52499 13.5416H11.4667C11.8667 13.5416 12.225 13.325 12.4 12.9666L13.1417 11.475C13.5333 10.6916 14.3167 10.2083 15.1917 10.2083H18.3083C18.65 10.2083 18.9333 10.4916 18.9333 10.8333C18.9333 11.175 18.65 11.4583 18.3083 11.4583H15.1917C14.7917 11.4583 14.4333 11.675 14.2583 12.0333L13.5167 13.525C13.125 14.3083 12.3417 14.7916 11.4667 14.7916Z" fill="#213067"/>
		<path d="M11.3916 6.45831H8.61661C8.26661 6.45831 7.98328 6.17498 7.98328 5.83331C7.98328 5.49165 8.26661 5.20831 8.60828 5.20831H11.3833C11.7249 5.20831 12.0083 5.49165 12.0083 5.83331C12.0083 6.17498 11.7333 6.45831 11.3916 6.45831Z" fill="#213067"/>
		<path d="M12.0833 8.95831H7.91663C7.57496 8.95831 7.29163 8.67498 7.29163 8.33331C7.29163 7.99165 7.57496 7.70831 7.91663 7.70831H12.0833C12.425 7.70831 12.7083 7.99165 12.7083 8.33331C12.7083 8.67498 12.425 8.95831 12.0833 8.95831Z" fill="#213067"/>
		</svg>
		),
		component: Orders
	},
	{
		path: '/users',
		name: "Jogapkar isgarler",
		icon: (<svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
		<path d="M15 5.96669C14.95 5.95836 14.8917 5.95836 14.8417 5.96669C13.6917 5.92502 12.775 4.98336 12.775 3.81669C12.775 2.62502 13.7334 1.66669 14.925 1.66669C16.1167 1.66669 17.075 2.63336 17.075 3.81669C17.0667 4.98336 16.15 5.92502 15 5.96669Z" stroke="#213067" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
		<path d="M14.1417 12.0334C15.2834 12.225 16.5417 12.025 17.425 11.4334C18.6 10.65 18.6 9.3667 17.425 8.58337C16.5334 7.9917 15.2584 7.79169 14.1167 7.99169" stroke="#213067" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
		<path d="M4.9749 5.96669C5.0249 5.95836 5.08324 5.95836 5.13324 5.96669C6.28324 5.92502 7.1999 4.98336 7.1999 3.81669C7.1999 2.62502 6.24157 1.66669 5.0499 1.66669C3.85824 1.66669 2.8999 2.63336 2.8999 3.81669C2.90824 4.98336 3.8249 5.92502 4.9749 5.96669Z" stroke="#213067" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
		<path d="M5.83328 12.0334C4.69162 12.225 3.43328 12.025 2.54995 11.4334C1.37495 10.65 1.37495 9.3667 2.54995 8.58337C3.44162 7.9917 4.71662 7.79169 5.85828 7.99169" stroke="#213067" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
		<path d="M10 12.1917C9.95002 12.1833 9.89169 12.1833 9.84169 12.1917C8.69169 12.15 7.77502 11.2083 7.77502 10.0417C7.77502 8.85 8.73336 7.89166 9.92502 7.89166C11.1167 7.89166 12.075 8.85833 12.075 10.0417C12.0667 11.2083 11.15 12.1583 10 12.1917Z" stroke="#213067" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
		<path d="M7.57498 14.8167C6.39998 15.6 6.39998 16.8833 7.57498 17.6667C8.90831 18.5583 11.0916 18.5583 12.425 17.6667C13.6 16.8833 13.6 15.6 12.425 14.8167C11.1 13.9333 8.90831 13.9333 7.57498 14.8167Z" stroke="#213067" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
		</svg>
		),
		component: Users,
		isOnlyForAdmin: true
	},
	{
		path: '/scraper',
		name: "Harydyn gelisi",
		icon: (<svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
		<path d="M10 8.12502C9.91667 8.12502 9.84167 8.10835 9.75833 8.07502C9.525 7.98335 9.375 7.75002 9.375 7.50002V1.66669C9.375 1.32502 9.65833 1.04169 10 1.04169C10.3417 1.04169 10.625 1.32502 10.625 1.66669V5.99169L11.225 5.39169C11.4667 5.15002 11.8667 5.15002 12.1083 5.39169C12.35 5.63335 12.35 6.03335 12.1083 6.27502L10.4417 7.94169C10.325 8.05835 10.1667 8.12502 10 8.12502Z" fill="#213067"/>
		<path d="M10 8.12497C9.8417 8.12497 9.68337 8.06664 9.55837 7.94164L7.8917 6.27497C7.65003 6.03331 7.65003 5.6333 7.8917 5.39164C8.13337 5.14997 8.53337 5.14997 8.77503 5.39164L10.4417 7.05831C10.6834 7.29997 10.6834 7.69997 10.4417 7.94164C10.3167 8.06664 10.1584 8.12497 10 8.12497Z" fill="#213067"/>
		<path d="M11.4667 14.7916H8.52502C7.65002 14.7916 6.86669 14.3083 6.47502 13.525L5.50002 11.575C5.46669 11.5 5.39169 11.4583 5.31669 11.4583H1.65002C1.30836 11.4583 1.02502 11.175 1.02502 10.8333C1.02502 10.4916 1.30836 10.2083 1.65002 10.2083H5.32502C5.88336 10.2083 6.38336 10.5166 6.63336 11.0166L7.60836 12.9666C7.78336 13.325 8.14169 13.5416 8.54169 13.5416H11.4834C11.8834 13.5416 12.2417 13.325 12.4167 12.9666L13.3917 11.0166C13.6417 10.5166 14.1417 10.2083 14.7 10.2083H18.3334C18.675 10.2083 18.9584 10.4916 18.9584 10.8333C18.9584 11.175 18.675 11.4583 18.3334 11.4583H14.7C14.6167 11.4583 14.55 11.5 14.5167 11.575L13.5417 13.525C13.125 14.3083 12.3417 14.7916 11.4667 14.7916Z" fill="#213067"/>
		<path d="M12.5 18.9583H7.49996C2.97496 18.9583 1.04163 17.025 1.04163 12.5V9.16665C1.04163 5.25831 2.49163 3.29998 5.74163 2.82498C6.09163 2.77498 6.39996 3.00831 6.44996 3.34998C6.49996 3.69165 6.26663 4.00831 5.92496 4.05831C3.30829 4.44165 2.29163 5.87498 2.29163 9.16665V12.5C2.29163 16.3416 3.65829 17.7083 7.49996 17.7083H12.5C16.3416 17.7083 17.7083 16.3416 17.7083 12.5V9.16665C17.7083 5.87498 16.6916 4.44165 14.075 4.05831C13.7333 4.00831 13.5 3.69165 13.55 3.34998C13.6 3.00831 13.9166 2.77498 14.2583 2.82498C17.5083 3.29998 18.9583 5.25831 18.9583 9.16665V12.5C18.9583 17.025 17.025 18.9583 12.5 18.9583Z" fill="#213067"/>
		</svg>
		),
		component: Scraper,
		isOnlyForAdmin: true
	},
	{
		path: '/calculation',
		name: "Calculation",
		icon: (<svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
		<path d="M8.33333 18.3334H11.6667C15.8333 18.3334 17.5 16.6667 17.5 12.5V7.50002C17.5 3.33335 15.8333 1.66669 11.6667 1.66669H8.33333C4.16667 1.66669 2.5 3.33335 2.5 7.50002V12.5C2.5 16.6667 4.16667 18.3334 8.33333 18.3334Z" stroke="#213067" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
		<path d="M13.75 6.31665V7.14998C13.75 7.83332 13.1917 8.39998 12.5 8.39998H7.5C6.81667 8.39998 6.25 7.84165 6.25 7.14998V6.31665C6.25 5.63332 6.80833 5.06665 7.5 5.06665H12.5C13.1917 5.06665 13.75 5.62498 13.75 6.31665Z" stroke="#213067" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
		<path d="M6.7801 11.6667H6.78973" stroke="#213067" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
		<path d="M9.99604 11.6667H10.0057" stroke="#213067" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
		<path d="M13.212 11.6667H13.2216" stroke="#213067" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
		<path d="M6.7801 14.5834H6.78973" stroke="#213067" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
		<path d="M9.99604 14.5834H10.0057" stroke="#213067" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
		<path d="M13.212 14.5834H13.2216" stroke="#213067" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
		</svg>
		),
		component: Calculation,
		isOnlyForAdmin: true
	},
	// {
	// 	path: '/products/:id/:stock',
	// 	name: 'Products',
	// 	notDisplay: true,
	// 	component: SingleProduct,
	// 	isOnlyForAdmin: true
	// },
	// {
	// 	path: '/products/:id',
	// 	name: 'Products',
	// 	notDisplay: true,
	// 	component: SingleProduct,
	// 	isOnlyForAdmin: true
	// },
	// {
	// 	path: '/products',
	// 	name: 'Products',
	// 	icon: '🏷',
	// 	component: Products,
	// 	isOnlyForAdmin: true
	// },
	// {
	// 	path: '/categories',
	// 	name: 'Categories',
	// 	icon: '𝌒',
	// 	component: Categories,
	// 	isOnlyForAdmin: true
	// },
	// {
	// 	path: '/brands',
	// 	name: 'Brands',
	// 	icon: '🇹🇷',
	// 	component: Brands,
	// 	isOnlyForAdmin: true
	// },
	// {
	// 	path: '/users',
	// 	name: 'Users',
	// 	icon: '👨‍👩‍👧‍👦',
	// 	component: Users,
	// 	isOnlyForAdmin: true
	// },
	// {
	// 	path: '/home',
	// 	name: 'Home page',
	// 	icon: '🖼',
	// 	component: Home,
	// 	isOnlyForAdmin: true
	// },	
	// {
	// 	path: '/scraper/category/:id',
	// 	name: 'Products',
	// 	notDisplay: true,
	// 	component: ScraperCategories,
	// 	isOnlyForAdmin: true
	// },
	// {
	// 	path: '/scraper',
	// 	name: "Scrapers",
	// 	icon: "📝",
	// 	component: Scraper,
	// 	isOnlyForAdmin: true
	// },
	// {
	// 	path: '/delivery',
	// 	name: "Cities",
	// 	icon: "🚛",
	// 	component: Cities,
	// 	isOnlyForAdmin: true
	// },
	// {
	// 	path: '/currency',
	// 	name: "Currency",
	// 	icon: "$",
	// 	component: Currency,
	// 	isOnlyForAdmin: true
	// },
	// {
	// 	path: '/staff',
	// 	name: "Staff",
	// 	icon: "👨🏻‍💻",
	// 	component: Staff,
	// 	isOnlyForAdmin: true
	// },
	// {
	// 	path: '/colors',
	// 	name: "Colors",
	// 	icon: "🎨",
	// 	component: Staff,
	// 	isOnlyForAdmin: true
	// },
	// // {
	// 	path: '/keywords',
	// 	name: "Keywords",
	// 	icon: "🔑",
	// 	component: Keywords
	// },
	// {
	// 	path: '/ui-components/badge',
	// 	name: 'Badges',
	// 	icon: 'mdi mdi-arrange-send-backward',
	// 	component: Badges
	// },
	// {
	// 	path: '/ui-components/breadcrumb',
	// 	name: 'Breadcrumbs',
	// 	icon: 'mdi mdi-equal',
	// 	component: Breadcrumbs
	// },
	// {
	// 	path: '/ui-components/button',
	// 	name: 'Buttons',
	// 	icon: 'mdi mdi-toggle-switch',
	// 	component: Buttons
	// },
	// {
	// 	path: '/ui-components/dropdown',
	// 	name: 'Button Dropdown',
	// 	icon: 'mdi mdi-cards-variant',
	// 	component: Dropdowns
	// },
	// {
	// 	path: '/ui-components/btn-group',
	// 	name: 'Button Group',
	// 	icon: 'mdi mdi-checkbox-multiple-blank',
	// 	component: BtnGroups
	// },
	// {
	// 	path: '/ui-components/card',
	// 	name: 'Cards',
	// 	icon: 'mdi mdi-credit-card-multiple',
	// 	component: Cards
	// },
	// {
	// 	path: '/ui-components/collapse',
	// 	name: 'Collapse',
	// 	icon: 'mdi mdi-elevator',
	// 	component: CollapseComponent
	// },
	// {
	// 	path: '/ui-components/carousel',
	// 	name: 'Carousel',
	// 	icon: 'mdi mdi-view-carousel',
	// 	component: CarouselComponent
	// },
	// {
	// 	path: '/ui-components/jumbotron',
	// 	name: 'Jumbotron',
	// 	icon: 'mdi mdi-hamburger',
	// 	component: JumbotronComponent
	// },
	// {
	// 	path: '/ui-components/listgroup',
	// 	name: 'List Group',
	// 	icon: 'mdi mdi-format-list-bulleted',
	// 	component: ListComponent
	// },
	// {
	// 	path: '/ui-components/media',
	// 	name: 'Media',
	// 	icon: 'mdi mdi-folder-multiple-image',
	// 	component: MediaComponent
	// },
	// {
	// 	path: '/ui-components/modal',
	// 	name: 'Modal',
	// 	icon: 'mdi mdi mdi-tablet',
	// 	component: ModalComponent
	// },
	// {
	// 	path: '/ui-components/navbar',
	// 	name: 'Navbar',
	// 	icon: 'mdi mdi-page-layout-header',
	// 	component: NavbarComponent
	// },
	// {
	// 	path: '/ui-components/navs',
	// 	name: 'Navs',
	// 	icon: 'mdi mdi-panorama-wide-angle',
	// 	component: NavsComponent
	// },
	// {
	// 	path: '/ui-components/pagination',
	// 	name: 'Pagination',
	// 	icon: 'mdi mdi-priority-high',
	// 	component: PaginationComponent
	// },
	// {
	// 	path: '/ui-components/popover',
	// 	name: 'Popover',
	// 	icon: 'mdi mdi-pencil-circle',
	// 	component: PopoverComponent
	// },
	// {
	// 	path: '/ui-components/progress',
	// 	name: 'Progress',
	// 	icon: 'mdi mdi-poll',
	// 	component: ProgressComponent
	// },
	// {
	// 	path: '/ui-components/table',
	// 	name: 'Tables',
	// 	icon: 'mdi mdi-border-none',
	// 	component: TableComponent
	// },
	// {
	// 	path: '/ui-components/tabs',
	// 	name: 'Tabs',
	// 	icon: 'mdi mdi-tab-unselected',
	// 	component: TabsComponent
	// },
	// {
	// 	path: '/ui-components/tooltip',
	// 	name: 'Tooltips',
	// 	icon: 'mdi mdi-image-filter-vintage',
	// 	component: TooltipComponent
	// },
	{ path: '/', pathTo: '/orders', name: 'Orders', redirect: true }
];
export default ThemeRoutes;
