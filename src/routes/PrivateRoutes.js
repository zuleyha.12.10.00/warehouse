import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import { authenticationService } from '../request/_services/auth';


export const PrivateRoute = ({ component: Component, ...rest }) => (
    <div>
        <Route {...rest} render={props => {
            const currentUser = authenticationService.currentUserValue;
            if (!currentUser || !currentUser.token) {
                // not logged in so redirect to login page with the return url
                return <Redirect to={{ pathname: '/authentication/login', state: { from: props.location } }} />
            }

            // authorised so return component
            return <Component {...props} isAdmin = {currentUser.isAdmin} currentUser = {currentUser}/>
        }} />
    </div>
)

export default PrivateRoute;