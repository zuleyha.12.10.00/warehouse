import { authHeader, handleResponse } from '../_helpers';
import config from './config'

export default request

function request(method, url, params, body) {
    let apiUrl = new URL(config.API_HOST + url)
    if (params) {
        Object.keys(params).forEach(key => {
            if (params[key]===undefined)
                delete params[key]
        })
        apiUrl.search = new URLSearchParams(params)
    }
    if (body){
        var formData = new FormData();
        Object.keys(body).forEach(key => {
            if (body[key]===null || body[key]===undefined || body[key === NaN])
                return;
            if (key.endsWith("[]") && body[key].length)
                body[key].forEach( e => formData.append(key, e) )
            else
                formData.append(key, body[key])
        })
    }

    const requestOptions = { 
        method, 
        headers: {
            ...authHeader(),
            'Access-Control-Allow-Origin': 'http://localhost:3000',
            
        },
        body: formData,
    };
    return fetch(apiUrl, requestOptions).then(handleResponse);
}