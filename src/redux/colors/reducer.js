import {
    GET_COLOR,
    CREATE_COLOR,
    UPDATE_COLOR,
    DELETE_COLOR
} from '../constants';

const INIT_STATE = {
    data: []
}

export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case GET_COLOR:
            return {
                ...state,
                data: action.payload,
            };
        case CREATE_COLOR:
            return {
                ...state,
                data: [...state.data, action.payload],
            };
        case DELETE_COLOR:
            return {
                ...state,
                data: state.data.filter((e)=> e.id !== action.payload ),
            };
        case UPDATE_COLOR:
            return {
                ...state,
                data: state.data.map((e)=> e.id == action.payload.id ? {...e, ...action.payload} : e ),
            };
        default:
            return state;
    }
};