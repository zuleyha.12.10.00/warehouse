import request from '../../request/_services/request.service'
import {
    GET_COLOR,
    CREATE_COLOR,
    UPDATE_COLOR,
    DELETE_COLOR
} from '../constants'
import {setError, setLoading} from '../loading/action'

export const getColors = (dispatch) => {
    dispatch(setLoading(true))
    request("GET", "filters/filter-colors").then(
        data => {
            dispatch(setLoading(false))
            dispatch({
                type: GET_COLOR,
                payload: data 
            })
        },
        error => {
            dispatch(setError(error))
        }
    )
}

export const createColors = (dispatch, body) => {
    dispatch(setLoading(true))
    request("POST", "filters/create-staff", null, body).then(
        data => {
            dispatch(setLoading(false))
            dispatch({
                type: CREATE_COLOR,
                payload: data 
            })
        },
        error => {
            dispatch(setError(error))
        }
    )
}

export const deleteColors = (dispatch, id) => {
    dispatch(setLoading(true))
    request("DELETE", "filters/staff/"+id).then(
        data =>{
            dispatch(setLoading(false))
            dispatch({
                type: DELETE_COLOR,
                payload: id 
            })
        },
        error => {
            dispatch(setError(error))
        }
    )  
}

export const updateColors = (dispatch, id, body) => {
    dispatch(setLoading(true))
    request("PUT", `filters/staff/${id}`, null, body).then(
        data => {
            dispatch(setLoading(false))
            dispatch({
                type: UPDATE_COLOR,
                payload: data
            })
        },
        error => {
            dispatch(setError(error))
        }
    )
}