import {
    GET_STAFF,
    CREATE_STAFF,
    UPDATE_STAFF,
    DELETE_STAFF
} from '../constants';

const INIT_STATE = {
    data: []
}

export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case GET_STAFF:
            return {
                ...state,
                data: action.payload,
            };
        case CREATE_STAFF:
            return {
                ...state,
                data: [...state.data, action.payload],
            };
        case DELETE_STAFF:
            return {
                ...state,
                data: state.data.filter((e)=> e.id !== action.payload ),
            };
        case UPDATE_STAFF:
            return {
                ...state,
                data: state.data.map((e)=> e.id == action.payload.id ? {...e, ...action.payload} : e ),
            };
        default:
            return state;
    }
};