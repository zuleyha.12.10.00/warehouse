import request from '../../request/_services/request.service'
import {
    GET_STAFF,
    CREATE_STAFF,
    UPDATE_STAFF,
    DELETE_STAFF
} from '../constants'
import {setError, setLoading} from '../loading/action'

export const getStaff = (dispatch) => {
    dispatch(setLoading(true))
    request("GET", "auth/staff").then(
        data => {
            dispatch(setLoading(false))
            dispatch({
                type: GET_STAFF,
                payload: data 
            })
        },
        error => {
            dispatch(setError(error))
        }
    )
}

export const createStaff = (dispatch, body) => {
    dispatch(setLoading(true))
    request("POST", "auth/create-staff", null, body).then(
        data => {
            dispatch(setLoading(false))
            dispatch({
                type: CREATE_STAFF,
                payload: data 
            })
        },
        error => {
            dispatch(setError(error))
        }
    )
}

export const deleteStaff = (dispatch, id) => {
    dispatch(setLoading(true))
    request("DELETE", "auth/staff/"+id).then(
        data =>{
            dispatch(setLoading(false))
            dispatch({
                type: DELETE_STAFF,
                payload: id 
            })
        },
        error => {
            dispatch(setError(error))
        }
    )  
}

export const updateStaff = (dispatch, id, body) => {
    dispatch(setLoading(true))
    request("PUT", `auth/staff/${id}`, null, body).then(
        data => {
            dispatch(setLoading(false))
            dispatch({
                type: UPDATE_STAFF,
                payload: data
            })
        },
        error => {
            dispatch(setError(error))
        }
    )
}