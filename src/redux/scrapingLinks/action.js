import request from '../../request/_services/request.service'
import {setError, setLoading} from '../loading/action'

export const getScrapingLinks = (dispatch, id, callback) => {
    dispatch(setLoading(true))
    request("GET", "scraping-links/" + id).then(
        data => {
            callback(null, data)
            dispatch(setLoading(false))
        },
        error => {
            dispatch(setError(error))
            callback(error, null)
        }
    )
}


export const resetScrapingLinks = (dispatch, id, callback) => {
    dispatch(setLoading(true))
    request("PUT", "scraping-links/reset-count/" + id).then(
        data => {
            callback(null, true)
            dispatch(setLoading(false))
        },
        error => {
            dispatch(setError(error))
            callback(error, null)
        }
    )
}

export const getScrapingLinksAsJson = (dispatch, id, callback) => {
    dispatch(setLoading(true))
    request("GET", "scraping-links/json/"+id).then(
        data => {
            callback(null, data)
            dispatch(setLoading(false))
        },
        error => {
            callback(error, null)
            dispatch(setError(error))
        }
    )
}

export const createScrapingLink = (dispatch, body, callback) => {
    console.log(body)
    dispatch(setLoading(true))
    request("POST", "scraping-links", null, body).then(
        data => {
            dispatch(setLoading(false))
            callback(null, data)
        },
        error => {
            dispatch(setError(error))
            callback(error, null)
        }
    )
}

export const deleteScrapingLink = (dispatch, id, callback) => {
    dispatch(setLoading(true))
    request("DELETE", "scraping-links/"+id).then(
        data =>{
            dispatch(setLoading(false))
            callback(null, data)
        },
        error => {
            dispatch(setError(error))
            callback(error, null)
        }
    )  
}

export const updateScrapingLink = (dispatch, id, body, callback) => {
    dispatch(setLoading(true))
    request("PUT", `scraping-links/${id}`, null, body).then(
        data => {
            dispatch(setLoading(false))
            callback(null, data)
        },
        error => {
            dispatch(setError(error))
            callback(error, null)
        }
    )
}
