import request from '../../request/_services/request.service'
import {
    GET_USERS,
    UPDATE_USER
} from '../constants'
import {setError, setLoading} from '../loading/action'

export const getUsers = (dispatch) => {
    dispatch(setLoading(true))
    request("GET", "auth").then(
        data => {
            dispatch(setLoading(false))
            dispatch({
                type: GET_USERS,
                payload: data.reverse() 
            })
        },
        error => {
            dispatch(setError(error))
        }
    )
}


export const updateUser = (dispatch, id, body) => {
    dispatch(setLoading(true))
    request("PUT", "auth/"+id, null, body).then(
        data => {
            dispatch(setLoading(false))
            dispatch({
                type: UPDATE_USER,
                payload: data 
            })
        },
        error => {
            dispatch(setError(error))
        }
    )
}
