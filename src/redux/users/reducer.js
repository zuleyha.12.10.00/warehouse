import {
    GET_USERS,
    UPDATE_USER
} from '../constants';

const INIT_STATE = {
    data: []
}

export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case GET_USERS:
            return {
                ...state,
                data: action.payload,
            };
        case UPDATE_USER:
            return {
                ...state,
                data: state.data.map((e)=> e.id === action.payload.id ? action.payload : e),
            };
        default:
            return state;
    }
};