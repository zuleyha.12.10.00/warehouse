import request from '../../request/_services/request.service'
import {
    GET_CITIES,
    CREATE_CITY,
    UPDATE_CITY,
    DELETE_CITY
} from '../constants'
import {setError, setLoading} from '../loading/action'

export const getCities = (dispatch) => {
    dispatch(setLoading(true))
    request("GET", "cities").then(
        data => {
            dispatch(setLoading(false))
            dispatch({
                type: GET_CITIES,
                payload: data 
            })
        },
        error => {
            dispatch(setError(error))
        }
    )
}

export const createCity = (dispatch, body) => {
    dispatch(setLoading(true))
    request("POST", "cities", null, body).then(
        data => {
            dispatch(setLoading(false))
            dispatch({
                type: CREATE_CITY,
                payload: data 
            })
        },
        error => {
            dispatch(setError(error))
        }
    )
}

export const deleteCity = (dispatch, id) => {
    dispatch(setLoading(true))
    request("DELETE", "cities/"+id).then(
        data =>{
            dispatch(setLoading(false))
            dispatch({
                type: DELETE_CITY,
                payload: id 
            })
        },
        error => {
            dispatch(setError(error))
        }
    )  
}

export const updateCity = (dispatch, id, body) => {
    dispatch(setLoading(true))
    request("PUT", `cities/${id}`, null, body).then(
        data => {
            dispatch(setLoading(false))
            dispatch({
                type: UPDATE_CITY,
                payload: data
            })
        },
        error => {
            dispatch(setError(error))
        }
    )
}


export const updateCityTitle = (dispatch, body) => {
    dispatch(setLoading(true))
    request("POST", "cities/update-title", null, body).then(
        data => {
            dispatch(setLoading(false))
        },
        error => {
            dispatch(setError(error))
        }
    )
}