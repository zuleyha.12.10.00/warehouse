import request from '../../request/_services/request.service'
import {setError, setLoading} from '../loading/action'

export const getStatisticsForDay = (dispatch, date, callback) => {
    dispatch(setLoading(true))
    request("GET", "orders/statistics-day/"+date).then(
        data => {
            callback(null, data)
            dispatch(setLoading(false))
        },
        error => {
            dispatch(setError(error))
            callback(error, null)
        }
    )
}

export const getStatisticsForMonth = (dispatch, date, callback) => {
    dispatch(setLoading(true))
    request("GET", "orders/statistics-month/"+date).then(
        data => {
            callback(null, data)
            dispatch(setLoading(false))
        },
        error => {
            dispatch(setError(error))
            callback(error, null)
        }
    )
}

