import {
    LOADING,
    ERROR,
} from '../constants'

export const setError = (error) => ({
    type: ERROR,
    payload: error
})

export const setLoading = (bool) => ({
    type: LOADING,
    payload: bool
})