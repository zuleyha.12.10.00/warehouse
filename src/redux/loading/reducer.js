import {
    LOADING,
    ERROR,
} from '../constants';

const INIT_STATE = {
    loading: false,
    error: null
}

export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case LOADING:
            return {
                ...state,
                loading: action.payload
            };
        case ERROR:
            return {
                ...state,
                error: action.payload,
                loading: false
            };
        default:
            return state;
    }
};