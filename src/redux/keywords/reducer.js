import {
    GET_CATEGORY_KEYWORDS,
    GET_KEYWORDS,
    DELETE_KEYWORD,
    CREATE_KEYWORD

} from '../constants';

const INIT_STATE = {
    data: {},
    keywords: {}
}

export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case GET_CATEGORY_KEYWORDS:
            return {
                ...state,
                data: action.payload,
            };
        case GET_KEYWORDS:
            let keywords = {}
            action.payload.forEach(e => {
                keywords[e.id] = e.keyword
            })
            return {
                ...state,
                keywords: keywords,
            };
        case CREATE_KEYWORD:
            let newData = {...state.data}

            for (const categoryId of JSON.parse(action.payload.categories)) {
                if (newData[categoryId])
                    newData[categoryId].push(Object.keys(action.payload.keyword)[0])
                else 
                    newData[categoryId] = [Object.keys(action.payload.keyword)[0]]
            }
            
            return {
                ...state,
                keywords: {...state.keywords, ...action.payload.keyword},
                data: newData
            };
        case DELETE_KEYWORD:
            Object.keys(state.data).forEach( key => {
                if (state.data[key].includes(action.payload)){
                    state.data[key] = state.data[key].filter(e => e !== action.payload)
                }
            })
            let newKeywords = {...state.keywords}
            delete newKeywords[action.payload]

            return {
                ...state,
                keywords: newKeywords,
                data: state.data
            };
        default:
            return state;
    }
};