import request from '../../request/_services/request.service'
import {
    GET_CATEGORY_KEYWORDS,
    GET_KEYWORDS,
    DELETE_KEYWORD,
    CREATE_KEYWORD
} from '../constants'
import {setError, setLoading} from '../loading/action'


export const getCategoryKeywords = (dispatch, callback) => {
    dispatch(setLoading(true))
    request("GET", "categories/category-keyword-ids").then(
        data => {
            dispatch(setLoading(false))
            dispatch({
                type: GET_CATEGORY_KEYWORDS,
                payload: data 
            })
            callback(null, data)
        },
        error => {
            dispatch(setError(error))
            callback(error, null)
        }
    )
}

export const getKeywords = (dispatch, callback) => {
    dispatch(setLoading(true))
    request("GET", "categories/keywords").then(
        data => {
            dispatch(setLoading(false))
            dispatch({
                type: GET_KEYWORDS,
                payload: data 
            })
            callback(null, true)
        },
        error => {
            dispatch(setError(error))
            callback(error, null)
        }
    )
}

export const createKeyword = (dispatch, body) => {
    dispatch(setLoading(true))
    console.log(body)
    request("POST", "categories/keyword", null, body).then(
        data => {
            console.log("data", data)
            dispatch(setLoading(false))
            dispatch({
                type: CREATE_KEYWORD,
                payload: {keyword: data, categories: body.categories}
            })
        },
        error => {
            dispatch(setError(error))
        }
    )
}


export const deleteKeyword = (dispatch, keyword) => {
    dispatch(setLoading(true))
    request("DELETE", "categories/keyword/"+keyword).then(
        data =>{
            dispatch(setLoading(false))
            dispatch({
                type: DELETE_KEYWORD,
                payload: keyword 
            })
        },
        error => {
            dispatch(setError(error))
        }
    )  
}