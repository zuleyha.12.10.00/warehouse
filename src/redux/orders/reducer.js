import {
    GET_ORDERS, 
    UPDATE_ORDER_PRODUCT,
    UPDATE_ORDER_FILTER,
    UPDATE_ORDER,
    DELETE_ORDER
} from '../constants';

const INIT_STATE = {
    data: [],
    page: 0,
    isEnd: false,
    filter: {}
}

export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case GET_ORDERS:
            return {
                ...state,
                data: [...state.data, ...action.payload],
                page: state.page + 1,
                isEnd: !action.payload.length
            };
        case UPDATE_ORDER_FILTER:
            return {
                ...state,
                data: [],
                page: 0,
                isEnd: false,
                filter: action.payload
            };  
        case UPDATE_ORDER_PRODUCT:
            let newData = [...state.data]
            newData.forEach(order => {
                order.products = order.products.map(op => {
                    if (op.orderProductId == action.payload.orderProductId)
                        return {...op, ...action.payload}
                    return op
                })
            });

            return {
                ...state,
                data: newData,
            };
        
        case UPDATE_ORDER:
            let newData1 = state.data.map(order => {
                if (order.id == action.payload.id)
                    return {...order, ...action.payload}
                return order
            });

            return {
                ...state,
                data: newData1,
            };

        case DELETE_ORDER:
            return {
                ...state,
                data: state.data.filter(e => !action.payload.ids.includes(e.id)),
            };
            
        default:
            return state;
    }
};