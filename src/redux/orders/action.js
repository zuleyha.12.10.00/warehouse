import request from '../../request/_services/request.service'
import {
    GET_ORDERS,
    UPDATE_ORDER_FILTER,
    UPDATE_ORDER_PRODUCT,
    UPDATE_ORDER,
    DELETE_ORDER
} from '../constants'
import {setError, setLoading} from '../loading/action'

export const getOrders = (dispatch, page, params, callback) => {
    dispatch(setLoading(true))
    request("GET", "orders/orders/"+page, params).then(
        data => {
            dispatch(setLoading(false))
            dispatch({
                type: GET_ORDERS,
                payload: data 
            })
            if (callback)
                callback(null, true)
        },
        error => {
            dispatch(setError(error))
            if (callback)
                callback(true, null)
        }
    )
}

export const updateOrderFilter = (dispatch, params, callback) => {
    dispatch({
        type: UPDATE_ORDER_FILTER,
        payload: params 
    })
    getOrders(dispatch, 0, params, callback)
}

export const updateOrderProduct = (id, body, dispatch) => {
    dispatch(setLoading(true))
    console.log(body)
    request("PUT", "orders/product/"+id, null, body).then(
        data => {
            dispatch(setLoading(false))
            dispatch({
                type: UPDATE_ORDER_PRODUCT,
                payload: {orderProductId: id, ...data} 
            })
        },
        error => {
            dispatch(setError(error))
        }
    )
}

export const updateOrder = (id, body, dispatch) => {
    dispatch(setLoading(true))
    console.log(body)
    request("PUT", "orders/"+id, null, body).then(
        data => {
            dispatch(setLoading(false))
            dispatch({
                type: UPDATE_ORDER,
                payload: {id, ...body} 
            })
        },
        error => {
            dispatch(setError(error))
        }
    )
}

export const deleteOrder = (body, dispatch) => {
    dispatch(setLoading(true))
    request("DELETE", "orders/array", null, body).then(
        data => {
            dispatch(setLoading(false))
            dispatch({
                type: DELETE_ORDER,
                payload: {...body} 
            })
        },
        error => {
            dispatch(setError(error))
        }
    )
}

export const registerAdmin = (id) => {
    const requestOptions = { 
        method: "POST", 
        headers: {
            'Content-Type':'application/json',
            "Authorization": "key=AAAAh2IQKbA:APA91bEywI3-y7bOTsV_HxyxgCguz-9yZkqx8kYoUAe9De6eDHc66Y2XCs5_rLb4aM4CgWuOPK_YeFchL2dwXeOTk2MQEOp23x4U6Aeya-9z3GkI9FxL8J5yIcFUM7EKaFoCcnE8inSE"
        }    
    };
    fetch(`https://iid.googleapis.com/iid/v1/${id}/rel/topics/operator`, requestOptions);
}