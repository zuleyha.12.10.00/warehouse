import {
    GET_BANNERS,
    UPDATE_BANNER,
    REMOVE_BANNER,
    ADD_BANNER
} from '../constants';

const INIT_STATE = {
    data: []
}

export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case GET_BANNERS:
            return {
                ...state,
                data: action.payload,
            };
        case UPDATE_BANNER:
            let newData = state.data.map(e => {
                if (e.id === action.payload.id)
                    return {...e, ...action.payload.body}
                return e;
            });
            return {
                ...state,
                data: newData,
            };
        case REMOVE_BANNER:
            return {
                ...state,
                data: state.data.filter( e => e.id !== action.payload),
            };
        case ADD_BANNER:
            return {
                ...state,
                data: [...state.data, action.payload],
            };
        
        default:
            return state;
    }
};