import request from '../../request/_services/request.service'
import {
    GET_BANNERS,
    REMOVE_BANNER,
    UPDATE_BANNER,
    ADD_BANNER
} from '../constants'
import {setError, setLoading} from '../loading/action'

export const getBanners = (dispatch) => {
    dispatch(setLoading(true))
    request("GET", "home/for-admin").then(
        data => {
            dispatch(setLoading(false))
            dispatch({
                type: GET_BANNERS,
                payload: data 
            })
        },
        error => {
            dispatch(setError(error))
        }
    )
}

export const updateImage = (dispatch, id, body) => {
    dispatch(setLoading(true))
    request("PUT", "home/"+id, null, body).then(
        data => {
            dispatch(setLoading(false))
            dispatch({
                type: UPDATE_BANNER,
                payload: {id, body: data} 
            })
        },
        error => {
            dispatch(setError(error))
        }
    )
}

export const updateImageField = (dispatch, id, body) => {
    dispatch(setLoading(true))
    request("PUT", "home/field/"+id, null, body).then(
        data => {
            dispatch(setLoading(false))
            dispatch({
                type: UPDATE_BANNER,
                payload: {id, body: data} 
            })
        },
        error => {
            dispatch(setError(error))
        }
    )
}

export const updateLink = (dispatch, id, body) => {
    dispatch(setLoading(true))
    request("PUT", "home/link/"+id, null, body).then(
        data => {
            dispatch(setLoading(false))
            dispatch({
                type: UPDATE_BANNER,
                payload: {id, body: data} 
            })
        },
        error => {
            dispatch(setError(error))
        }
    )
}

export const addBanner = (dispatch, tag) => {
    dispatch(setLoading(true))
    request("POST", "home?tag="+tag).then(
        data => {
            dispatch(setLoading(false))
            dispatch({
                type: ADD_BANNER,
                payload: data
            })
        },
        error => {
            dispatch(setError(error))
        }
    )
}

export const removeBanner = (dispatch, id,) => {
    dispatch(setLoading(true))
    request("DELETE", "home/"+id).then(
        data => {
            dispatch(setLoading(false))
            dispatch({
                type: REMOVE_BANNER,
                payload: id 
            })
        },
        error => {
            dispatch(setError(error))
        }
    )
}
