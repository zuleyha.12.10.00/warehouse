import {
    GET_BRANDS,
    UPDATE_BRAND,
    DELETE_BRAND,
    UPDATE_BRAND_IMAGE,
} from '../constants';

const INIT_STATE = {
    data: []
}

export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case GET_BRANDS:
            return {
                ...state,
                data: action.payload,
            };
        case UPDATE_BRAND_IMAGE:
            return {
                ...state,
                data: state.data.map((e)=> e.id == action.payload.id ? {...e, ...action.payload, image: action.payload.image} : e ),
            };
        case UPDATE_BRAND:
            return {
                ...state,
                data: state.data.map((e)=> e.title == action.payload.title ? {...e, ...action.payload} : e ),
            };
        case DELETE_BRAND:
            return {
                ...state,
                data: state.data.filter((e)=> e.id !== action.payload),
            };
        default:
            return state;
    }
};