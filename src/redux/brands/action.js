import request from '../../request/_services/request.service'
import {
    GET_BRANDS,
    UPDATE_BRAND_IMAGE,
    UPDATE_BRAND,
    DELETE_BRAND
} from '../constants'
import {setError, setLoading} from '../loading/action'


export const getBrands = (dispatch) => {
    dispatch(setLoading(true))
    request("GET", "products/brands-image-for-admin").then(
        data => {
            dispatch(setLoading(false))
            dispatch({
                type: GET_BRANDS,
                payload: data 
            })
        },
        error => {
            dispatch(setError(error))
        }
    )
}

export const updateBrandImage = (dispatch, body) => {
    dispatch(setLoading(true))
    request("POST", "products/update-brand-image", null, body).then(
        data => {
            console.log(data)
            dispatch(setLoading(false))
            dispatch({
                type: UPDATE_BRAND_IMAGE,
                payload: data 
            })
        },
        error => {
            dispatch(setError(error))
        }
    )
}


export const update = (dispatch, body, callback) => {
    dispatch(setLoading(true))
    request("POST", "products/update-brand", null, body).then(
        data => {
            console.log(data)
            dispatch(setLoading(false))
            dispatch({
                type: UPDATE_BRAND,
                payload: body 
            })
            if (callback){
                callback(null, data)
            }
        },
        error => {
            dispatch(setError(error))
            if (callback){
                callback(error)
            }
        }
    )
}

export const deleteBrand = (dispatch, id) => {
    dispatch(setLoading(true))
    request("DELETE", "products/brands/"+id).then(
        data => {
            dispatch(setLoading(false))
            dispatch({
                type: DELETE_BRAND,
                payload: id 
            })
        },
        error => {
            dispatch(setError(error))
        }
    )
}
