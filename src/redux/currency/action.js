import request from '../../request/_services/request.service'
import {setError, setLoading} from '../loading/action'

export const getCurrency = (dispatch, callback) => {
    dispatch(setLoading(true))
    request("GET", "currency/").then(
        data => {
            callback(null, data)
            dispatch(setLoading(false))
        },
        error => {
            dispatch(setError(error))
            callback(error, null)
        }
    )
}

export const updateCurrency = (dispatch, body, callback) => {
    dispatch(setLoading(true))
    request("POST", "currency", null, body).then(
        data => {
            dispatch(setLoading(false))
            callback(null, data)
        },
        error => {
            dispatch(setError(error))
            callback(error, null)
        }
    )
}


export const getShowCurrency = (dispatch, callback) => {
    dispatch(setLoading(true))
    request("GET", "currency/show").then(
        data => {
            callback(null, data)
            dispatch(setLoading(false))
        },
        error => {
            dispatch(setError(error))
            callback(error, null)
        }
    )
}

export const updateShowCurrency = (dispatch, body, callback) => {
    dispatch(setLoading(true))
    request("POST", "currency/show", null, body).then(
        data => {
            dispatch(setLoading(false))
            callback(null, data)
        },
        error => {
            dispatch(setError(error))
            callback(error, null)
        }
    )
}
