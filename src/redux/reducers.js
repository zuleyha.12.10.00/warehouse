import { combineReducers } from 'redux';
import settings from './settings/reducer';
import brands from './brands/reducer';
import categories from './categories/reducer';
import keywords from './keywords/reducer';
import loading from './loading/reducer';
import products from './products/reducer';
import users from './users/reducer';
import orders from './orders/reducer';
import home from './home/reducer';
import scrapers from './scraper/reducer';
import cities from './cities/reducer';
import colors from './colors/reducer';
import staff from './staff/reducer';

const reducers = combineReducers({
    settings,
    cities,
    categories,
    brands,
    loading,
    products,
    users,
    home,
    scrapers,
    orders,
    keywords,
    staff,
    colors
});

export default reducers;