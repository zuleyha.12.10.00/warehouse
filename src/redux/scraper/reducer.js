import {
    GET_SCRAPERS,
    CREATE_SCRAPER,
    UPDATE_SCRAPER,
    DELETE_SCRAPER
} from '../constants';

const INIT_STATE = {
    data: []
}

export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case GET_SCRAPERS:
            return {
                ...state,
                data: action.payload,
            };
        case CREATE_SCRAPER:
            return {
                ...state,
                data: [...state.data, action.payload],
            };
        case DELETE_SCRAPER:
            return {
                ...state,
                data: state.data.filter((e)=> e.id !== action.payload ),
            };
        case UPDATE_SCRAPER:
            return {
                ...state,
                data: state.data.map((e)=> e.id == action.payload.id ? action.payload : e ),
            };
        default:
            return state;
    }
};