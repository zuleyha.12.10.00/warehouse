import request from '../../request/_services/request.service'
import {
    GET_SCRAPERS,
    CREATE_SCRAPER,
    UPDATE_SCRAPER,
    DELETE_SCRAPER
} from '../constants'
import {setError, setLoading} from '../loading/action'

export const getScrapers = (dispatch) => {
    dispatch(setLoading(true))
    request("GET", "scrapers").then(
        data => {
            dispatch(setLoading(false))
            dispatch({
                type: GET_SCRAPERS,
                payload: data 
            })
        },
        error => {
            dispatch(setError(error))
        }
    )
}


export const getScraperAsJson = (dispatch, id, callback) => {
    dispatch(setLoading(true))
    request("GET", "scrapers/" + id).then(
        data => {
            callback(null, data)
            dispatch(setLoading(false))
        },
        error => {
            callback(error, null)
            dispatch(setError(error))
        }
    )
}

export const createScraper = (dispatch, body) => {
    dispatch(setLoading(true))
    request("POST", "scrapers", null, body).then(
        data => {
            dispatch(setLoading(false))
            dispatch({
                type: CREATE_SCRAPER,
                payload: data 
            })
        },
        error => {
            dispatch(setError(error))
        }
    )
}

export const deleteScraper = (dispatch, id) => {
    dispatch(setLoading(true))
    request("DELETE", "scrapers/"+id).then(
        data =>{
            dispatch(setLoading(false))
            dispatch({
                type: DELETE_SCRAPER,
                payload: id 
            })
        },
        error => {
            dispatch(setError(error))
        }
    )  
}

export const updateScraper = (dispatch, id, body) => {
    dispatch(setLoading(true))
    request("PUT", `scrapers/${id}`, null, body).then(
        data => {
            dispatch(setLoading(false))
            dispatch({
                type: UPDATE_SCRAPER,
                payload: data
            })
        },
        error => {
            dispatch(setError(error))
        }
    )
}
