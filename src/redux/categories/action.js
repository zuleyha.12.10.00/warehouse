import request from '../../request/_services/request.service'
import {
    GET_CATEGORIES,
    UPDATE_CATEGORY,
    DELETE_CATEGORY,
    CREATE_CATEGORY
} from '../constants'
import {setError, setLoading} from '../loading/action'


export const confCategoriesWithLinks = (dispatch, data) => {
    dispatch({
        type: GET_CATEGORIES,
        payload: data 
    })
}

export const getCategories = (dispatch) => {
    dispatch(setLoading(true))
    request("GET", "categories/for-admin").then(
        data => {
            dispatch(setLoading(false))
            dispatch({
                type: GET_CATEGORIES,
                payload: data 
            })
        },
        error => {
            dispatch(setError(error))
        }
    )
}

export const createCategory = (dispatch, body) => {
    dispatch(setLoading(true))
    request("POST", "categories", null, body).then(
        data => {
            dispatch(setLoading(false))
            dispatch({
                type: CREATE_CATEGORY,
                payload: data 
            })
        },
        error => {
            dispatch(setError(error))
        }
    )
}

export const deleteCategory = (dispatch, id) => {
    dispatch(setLoading(true))
    request("DELETE", "categories/"+id).then(
        data =>{
            dispatch(setLoading(false))
            dispatch({
                type: DELETE_CATEGORY,
                payload: id 
            })
        },
        error => {
            dispatch(setError(error))
        }
    )  
}

export const updateCategory = (dispatch, id, body) => {
    dispatch(setLoading(true))
    request("PUT", `categories/${id}`, null, body).then(
        data => {
            dispatch(setLoading(false))
            dispatch({
                type: UPDATE_CATEGORY,
                payload: data
            })
        },
        error => {
            dispatch(setError(error))
        }
    )
}

export const updateCategoryTitle = (dispatch, body) => {
    dispatch(setLoading(true))
    request("POST", "categories/update-title", null, body).then(
        data => {
            dispatch(setLoading(false))
        },
        error => {
            dispatch(setError(error))
        }
    )
}

export const updateCategoryImage = (dispatch, body) => {
    dispatch(setLoading(true))
    request("POST", "categories/update-image", null, body).then(
        data => {
            dispatch(setLoading(false))
            dispatch({
                type: UPDATE_CATEGORY,
                payload: {...data, image: data.image || null}   
            })
        },
        error => {
            dispatch(setError(error))
        }
    )  
}
