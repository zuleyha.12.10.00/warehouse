import {
    GET_CATEGORIES,
    UPDATE_CATEGORY,
    DELETE_CATEGORY,
    CREATE_CATEGORY
} from '../constants';

const INIT_STATE = {
    data: []
}

export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case GET_CATEGORIES:
            return {
                ...state,
                data: action.payload,
            };
        case CREATE_CATEGORY:
            return {
                ...state,
                data: [...state.data, action.payload],
            };
        case DELETE_CATEGORY:
            return {
                ...state,
                data: state.data.filter((e)=> e.id !== action.payload ),
            };
        case UPDATE_CATEGORY:
            return {
                ...state,
                data: state.data.map((e)=> e.id == action.payload.id ? {...e, ...action.payload} : e ),
            };
        default:
            return state;
    }
};