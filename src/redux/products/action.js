import request from '../../request/_services/request.service'
import {
    GET_PRODUCTS,
    UPDATE_PRODUCT,
    DELETE_PRODUCT,
    DELETE_ARRAY_PRODUCTS,
    SET_TOTAL,
    UPDATE_SELECTED_CATEGORIES,
    UPDATE_SELECTED_SCRAPERS
} from '../constants'
import {setError, setLoading} from '../loading/action'

export const getProducts = (dispatch, body) => {
    dispatch(setLoading(true))
    request("POST", "products/get-for-admin", null, body).then(
        data => {
            dispatch(setLoading(false))
            dispatch({
                type: GET_PRODUCTS,
                payload: data 
            })
        },
        error => {
            dispatch(setError(error))
        }
    )
}

export const getProductsCount = (dispatch, body, callback) => {
    dispatch(setLoading(true))
    request("POST", "products/for-admin/total-count", null, body).then(
        data => {
            dispatch(setLoading(false))
            dispatch({
                type: SET_TOTAL,
                payload: data.total
            })
            callback(null, data)
        },
        error => {
            dispatch(setError(error))
            callback(error, null)
        }
    )
}

export const getProductById = (dispatch, id, callback) => {
    dispatch(setLoading(true))
    request("GET", "products/"+id).then(
        data => {
            dispatch(setLoading(false))
            callback(null, data)
            // dispatch({
            //     type: GET_SINGLE_PRODUCT,
            //     payload: data 
            // })
        },
        error => {
            callback(error, null)
            dispatch(setError(error))
        }
    )
}


export const createProduct = (dispatch, body, callback) => {
    dispatch(setLoading(true))
    request("POST", "products", null, body).then(
        data => {
            dispatch(setLoading(false))
            getProducts(dispatch)
            callback(null, data)
        },
        error => {
            callback(error, null)
            dispatch(setError(error))
        }
    )
}

export const deleteProduct = (dispatch, id, callback) => {
    dispatch(setLoading(true))
    request("DELETE", "products/"+id).then(
        data =>{
            dispatch(setLoading(false))
            callback(null, data)
        },
        error => {
            dispatch(setError(error))
            callback(error, null)

        }
    )  
}

export const deleteArrayProducts = (dispatch, arr, callback) => {
    let body = {ids: JSON.stringify(arr)}
    dispatch(setLoading(true))
    request("POST", "products/delete-array", null, body).then(
        data =>{
            dispatch(setLoading(false))
            dispatch({
                type: DELETE_ARRAY_PRODUCTS,
                payload: arr 
            })
            callback(null, true)
        },
        error => {
            callback(error, null)
            dispatch(setError(error))
        }
    )  
}

export const replaceAll = (dispatch, body, callback) => {
    dispatch(setLoading(true))
    request("POST", `products/replace-all`, null, body).then(
        data => {
            dispatch(setLoading(false))
            getProducts(dispatch)
            callback(null, data)
        },
        error => {
            callback(error, null)
            dispatch(setError(error))
        }
    )
}

export const updateProduct = (dispatch, id, body, callback) => {
    dispatch(setLoading(true))
    console.log(id, body, callback)
    request("PUT", `products/${id}`, null, body).then(
        data => {
            dispatch(setLoading(false))
            getProducts(dispatch)
            callback(null, data)
        },
        error => {
            callback(error, null)
            dispatch(setError(error))
        }
    )
}

export const updateProductTitle = (dispatch, body) => {
    dispatch(setLoading(true))
    request("POST", "products/update-title", null, body).then(
        data => {
            dispatch(setLoading(false))
            dispatch({
                type: UPDATE_PRODUCT,
                payload: data
            })
        },
        error => {
            dispatch(setError(error))
        }
    )
}

export const updateProductImage = (dispatch, body) => {
    dispatch(setLoading(true))
    request("POST", "products/update-image", null, body).then(
        data => {
            dispatch(setLoading(false))
            dispatch({
                type: UPDATE_PRODUCT,
                payload: data
            })
        },
        error => {
            dispatch(setError(error))
        }
    )  
}


export const updateSelectedCategories = (dispatch, list) => {
    dispatch({
        type: UPDATE_SELECTED_CATEGORIES,
        payload: list
    })
}

export const updateSelectedScrapers = (dispatch, list) => {
    dispatch({
        type: UPDATE_SELECTED_SCRAPERS,
        payload: list
    })
}
