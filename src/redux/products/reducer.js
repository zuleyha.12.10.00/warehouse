import {
    GET_PRODUCTS,
    UPDATE_PRODUCT,
    DELETE_PRODUCT,
    DELETE_ARRAY_PRODUCTS,
    CREATE_PRODUCT,
    SET_TOTAL,
    UPDATE_SELECTED_SCRAPERS,
    UPDATE_SELECTED_CATEGORIES
} from '../constants';

const INIT_STATE = {
    data: [],
    page: 1, 
    sizePerPage: 30,
    selectedCategories: [],
    selectedFilters: [],
    selectedScrapers: [],
    collapsedCategories: [],
    total: 0
}

export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case GET_PRODUCTS:
            return {
                ...state,
                data: action.payload,
            };
        case SET_TOTAL:
            return {
                ...state,
                total: action.payload,
            };
        case CREATE_PRODUCT:
            return {
                ...state,
                data: [...state.data, action.payload],
            };
        case DELETE_PRODUCT:
            console.log(action.payload, state.data)
            return {
                ...state,
                data: state.data.filter((e)=> e.id !== action.payload ),
            };
        case DELETE_ARRAY_PRODUCTS:
            return {
                ...state,
                data: state.data.filter( e => action.payload.indexOf(e.id) === -1 ),
            };
        case UPDATE_SELECTED_CATEGORIES:
            return {
                ...state,
                selectedCategories: action.payload,
                data: []
            };
        case UPDATE_SELECTED_SCRAPERS:
            return {
                ...state,
                selectedScrapers: action.payload,
                data: []
            };
        // case UPDATE_PRODUCT:
        //     return {
        //         ...state,
        //         data: state.data.map((e)=> e.id == action.payload.id ? action.payload : e ),
        //     };
        default:
            return state;
    }
};